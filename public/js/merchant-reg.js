
$( document ).ready(function() {
    $('#mobile_number').on('keyup',function () {
        var mobile =$('#mobile_number').val();
        if (wallet_number_pattern.test(mobile))
        {
            $('.invalid-feedback-mobile').hide();
            $('.mobile_number').hide();
            $(this).removeClass('input-invalid');
        }
        else {
            $('.invalid-feedback-mobile').show();
            $('.mobile_number').hide();
            $(this).addClass('input-invalid');
        }
    });
    $('#wallet_number').on('keyup',function () {
        var mobile =$('#wallet_number').val();
        if (wallet_number_pattern.test(mobile))
        {
            $('.wallet_number').hide();
            $(this).removeClass('input-invalid');
        }
        else {
            $('.wallet_number').show();
            $(this).addClass('input-invalid');
        }
    });
    $('#email').on('keyup',function (){
        var email =$('#email').val();
        if (email_pattern.test(email))
        {
            $('.email').hide();
            $(this).removeClass('input-invalid');
        }
        else {
            $('.email').show();
            $(this).addClass('input-invalid');
        }
    });
    $('#first_name').on('keyup',function (){
        var first_name =$('#first_name').val();
        if(first_name==""){
            $('#first_name').addClass('input-invalid');
            $('.first-name').show();
        }
        else{
            $("#first_name").removeClass('input-invalid');
            $('.first_name').hide();
        }
    });
    $('#last_name').on('keyup',function (){
        var last_name =$('#last_name').val();
        if(last_name==""){
            $("#last_name").addClass('input-invalid');
            $('.last_name').show();
        }
        else{
            $("#last_name").removeClass('input-invalid');
            $('.last_name').hide();
        }
    });
    $('#shop_name').on('keyup',function (){
        var shop_name =$('#shop_name').val();
        if(shop_name==""){
            $("#shop_name").addClass('input-invalid');
            $('.shop_name').show();
        }
        else{
            $("#shop_name").removeClass('input-invalid');
            $('.shop_name').hide();
        }
    });
    $('#pickup_number').on('keyup',function () {
        var mobile =$('#pickup_number').val();
        if (wallet_number_pattern.test(mobile))
        {
            $('.pickup_number').hide();
            $(this).removeClass('input-invalid');
        }
        else {
            $('.pickup_number').show();
            $(this).addClass('input-invalid');
        }
    });
    $('#shop_link').on('keyup',function (){
        var shop_link =$('#shop_link').val();
        if(shop_link==""){
            $("#shop_link").addClass('input-invalid');
            $('.shop_link').show();
        }
        else{
            $("#shop_link").removeClass('input-invalid');
            $('.shop_link').hide();
        }
    });
    $('#verification_id_number').on('keyup',function (){
        var verification_id_number =$('#verification_id_number').val();
        if(verification_id_number==""){
            $("#verification_id_number").addClass('input-invalid');
            $('.verification_id_number').show();
        }
        else{
            $("#verification_id_number").removeClass('input-invalid');
            $('.verification_id_number').hide();
        }
    });
    $('#verification_id_pic').on('change',function (){
        var file = $('#verification_id_pic').val();
        var file1 = Validate(file);
        if(file1==1){
            $("#verification_id_pic").addClass('input-invalid');
            $('.verification_id_pic').show();
            $(this).val('');
        }
        else{
            $("#verification_id_pic").removeClass('input-invalid');
            $('.verification_id_pic').hide();
        }
    });
    $("input[name='payment_type']"). click(function(){
        var radioValue = $("input[name='payment_type']:checked"). val();
        if(radioValue =="Bank"){
            $('.bank').show();
            $('.mobile').hide();
        }
        else if(radioValue =="Mobile")
        {
            $('.bank').hide();
            $('.mobile').show();
        }
        else{
            $('.bank').hide();
            $('.mobile').hide();
        }
    });

    $("#bank_name").on('keyup',function () {
        var bank_name= $("#bank_name").val();

        if(!bank_name.replace(/\s/g, '').length){
            $('#bank_name').addClass('input-invalid');
            $('.bank_name').show();
        }
        else {
            $('#bank_name').removeClass('input-invalid');
            $('.bank_name').hide();
        }

    });
    $("#branch_name").on('keyup',function () {
        var branch_name= $("#branch_name").val();

        if(!branch_name.replace(/\s/g, '').length){
            $('#branch_name').addClass('input-invalid');
            $('.branch_name').show();
        }
        else {
            $('#branch_name').removeClass('input-invalid');
            $('.branch_name').hide();
        }
    });
    $("#account_type").on('keyup',function () {
        var account_type= $("#account_type").val();

        if(!account_type.replace(/\s/g, '').length){
            $('#account_type').addClass('input-invalid');
            $('.account_type').show();
        }
        else {
            $('#account_type').removeClass('input-invalid');
            $('.account_type').hide();
        }
    });
    $("#routing_number").on('keyup',function () {
        var routing_number= $("#routing_number").val();

        if(!routing_number.replace(/\s/g, '').length){
            $('#routing_number').addClass('input-invalid');
            $('.routing_number').show();
        }
        else {
            $('#routing_number').removeClass('input-invalid');
            $('.routing_number').hide();
        }
    });
    $("#account_holder_name").on('keyup',function () {
        var account_holder_name= $("#account_holder_name").val();

        if(!account_holder_name.replace(/\s/g, '').length){
            $('#account_holder_name').addClass('input-invalid');
            $('.account_holder_name').show();
        }
        else {
            $('#account_holder_name').removeClass('input-invalid');
            $('.account_holder_name').hide();
        }

    });
    $("#account_number").on('keyup',function () {
        var account_number= $("#account_number").val();
        if(!account_number.replace(/\s/g, '').length){
            $('#account_number').addClass('input-invalid');
            $('.account_number').show();
        }
        else {
            $('#account_number').removeClass('input-invalid');
            $('.account_number').hide();
        }

    });
    $('#agree').on('click', function () {
        if(!$("#agree").is(':checked')){
            // $("#agree").addClass('input-invalid');
            $('.agree').show();
        }
        else{
            $('.agree').hide();
        }
    })
});


$("input[name='verification_id_type']"). click(function(){
    var radioValue = $("input[name='verification_id_type']:checked"). val();
    if(radioValue =="Birth Certificate"){
        $('.passport').hide();
        $('.birth_certificate').show();
        $('.driving_license').hide();
        $('.nid').hide();
    }
    else if(radioValue =="Passport"){
        $('.passport').show();
        $('.birth_certificate').hide();
        $('.driving_license').hide();
        $('.nid').hide();
    }
    else if(radioValue =="Driving License"){
        $('.passport').hide();
        $('.birth_certificate').hide();
        $('.driving_license').show();
        $('.nid').hide();
    }
    else{
        $('.passport').hide();
        $('.birth_certificate').hide();
        $('.driving_license').hide();
        $('.nid').show();
    }
});

