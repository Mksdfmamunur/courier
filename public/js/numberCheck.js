(function($) {
    $.fn.inputFilter = function(inputFilter) {
        return this.on("input keydown keyup mousedown mouseup select contextmenu drop", function() {
            if (inputFilter(this.value)) {
                this.oldValue = this.value;
                this.oldSelectionStart = this.selectionStart;
                this.oldSelectionEnd = this.selectionEnd;
            } else if (this.hasOwnProperty("oldValue")) {
                this.value = this.oldValue;
                this.setSelectionRange(this.oldSelectionStart, this.oldSelectionEnd);
            } else {
                this.value = "";
            }
        });
    };
}(jQuery));

$(".number").inputFilter(function(value) {
    return /^-?\d*$/.test(value); });


var mobile_pattern = /\+?(88)?0?1[3456789][0-9]{8}$/;
var wallet_number_pattern = /^(01[346789])(\d{8})$/;
var email_pattern = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

function Validate(file) {
    var x;
    var le=file.length;
    var poin=file.lastIndexOf(".");
    var accu1=file.substring(poin,le);
    var accu = accu1.toLowerCase();
    console.log(accu);
//alert(accu) ;
    if ((accu !='.png') && (accu !='.jpg') && (accu !='.jpeg')){
        x=1;
        return x;
    }else{
        x=0;
        return x;
    }
}
if ( window.history.replaceState ) {
    window.history.replaceState( null, null, window.location.href );
}