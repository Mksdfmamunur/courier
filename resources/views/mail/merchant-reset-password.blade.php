<!DOCTYPE html>
<html lang="en">
<head>
    <title>Merchant Reset Password Mail</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
</head>
<body>


<div style="width: 600px; margin: 0 auto; border: 1px solid #ddd; overflow: hidden; font: 14px/20px 'Trebuchet MS', Arial, Helvetica, sans-serif; color: #333; background-color: #fcfcfc;">
    <div style="height: auto; padding: 24px 0 0 20px;">
        <a  href="http://www.metroexpress.com.bd" target="_blank">
            <img title="Metroexpress" src="{{asset('public/icon/logo.png')}}"  width="129" height="61" >
        </a>
        <br>
    </div>
    <div style="margin: 25px 20px 15px; padding: 0px;">
        <div>
            Dear Merchant,
            <br>
        </div>
        <div>
            <br>
        </div>
        <p style="text-align: justify; text-indent: 50px;">
            <span>
                We heard you need a password reset. Click the link below and you'll be redirected to a secure site from which you can set a new password.
            </span>
            <br>
        </p>
        <p style="text-align: justify">
            <a style="border-radius:3px;background:#3aa54c;color:#fff;display:block;font-weight:700;font-size:16px;line-height:1.25em;margin:24px auto 24px;padding:10px 18px;text-decoration:none;width:180px;text-align:center"   href="{{route('merchant.reset',['id'=>encrypt($data['token']),'email'=>encrypt($data['email'])])}}" target="_blank">
                Reset Password
            </a>
        </p>

        <p style="text-align: justify;">
            Need Help? Call 09639-103314 or Feel free to write to <a href = "mailto: info@metroexpress.com.bd">info@metroexpress.com.bd</a>, for any queries and suggession.&nbsp;
            <br>
        </p>
        <br>
        <div>
            Thanks!
            <br>
        </div>
        <div style="font-weight: bold;">
            <span>
                MetroExpress
            </span>
            Support Team
            <br>
        </div>
    </div>

</div>
<div>
    <br>
</div>
</body>
</html>








