<!DOCTYPE html>
<html lang="en">
<head>
    <title>Merchant Registration Mail</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
</head>
<body>


<div style="width: 600px; margin: 0 auto; border: 1px solid #ddd; overflow: hidden; font: 14px/20px 'Trebuchet MS', Arial, Helvetica, sans-serif; color: #333; background-color: #fcfcfc;">
    <div style="height: auto; padding: 24px 0 0 20px;">
        <a  href="http://www.metroexpress.com.bd" target="_blank">
            <img title="Metroexpress" src="{{asset('public/icon/logo.png')}}"  width="129" height="61" >
        </a>
        <br>
    </div>
    <div style="margin: 25px 20px 15px; padding: 0px;">
        <div>
            Dear Merchant,
            <br>
        </div>
        <div>
            <br>
        </div>
        <p style="text-align: justify; font-weight: bold; font-size: 20px">
            <span>
                Congratulations on register with MetroExpress Courier.
            </span>
            <br>
        </p>
        <p style="text-align: justify;">

            To start Creating Order you will have to wait for Verification Process. It will take maximum 2 business days to get the account verified. When the verification process is completed, an email will be sent to you. You will find the link to your merchant web platform and login credentials in that email.
        </p>
        <p style="text-align: justify;">
            Need Help? Call 09639-103314 or Feel free to write to <a href = "mailto: info@metroexpress.com.bd">info@metroexpress.com.bd</a>, for any queries and suggession.&nbsp;
            <br>
        </p>
        <br>
        <div>
            Thanks!
            <br>
        </div>
        <div style="font-weight: bold;">
            <span>
                MetroExpress
            </span>
            Support Team
            <br>
        </div>
    </div>
</div>
</body>
</html>








