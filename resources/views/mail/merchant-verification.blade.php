<!DOCTYPE html>
<html lang="en">
<head>
    <title>Merchant Verification Mail</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
</head>
<body>


<div style="width: 600px; margin: 0 auto; border: 1px solid #ddd; overflow: hidden; font: 14px/20px 'Trebuchet MS', Arial, Helvetica, sans-serif; color: #333; background-color: #fcfcfc;">
    <div style="height: auto; padding: 24px 0 0 20px;">
        <a  href="http://www.metroexpress.com.bd" target="_blank">
            <img title="Metroexpress" src="{{asset('public/icon/logo.png')}}"  width="129" height="61" >
        </a>
        <br>
    </div>
    <div style="margin: 25px 20px 15px; padding: 0px;">
        <div>
            Dear Merchant,
            <br>
        </div>
        <div>
            <br>
        </div>
        <p style="text-align: justify; text-indent: 50px;">
            <span>
                Welcome to MetroExpress!&nbsp;We have created your account in MetroExpress.
            </span>
            <br>
        </p>
        <div>
            <b>
                You can login through:
            </b>
            <br>
        </div>
        <div>
            <br>
        </div>
        <div align="center">
            <table style="margin: 5px 0 0 0; font-size: 13px;" border="0" cellspacing="0" cellpadding="5">
                <tbody>
                <tr>
                    <th style="text-align: left; width: 20%;">
                        URL
                        <br>
                    </th>
                    <td>
                        <a href="{{route('merchant.login')}}" target="_blank">
                            {{route('merchant.login')}}
                        </a>
                        <br>
                    </td>
                </tr>
                <tr>
                    <th style="text-align: left;">
                            <span>
                                Email
                            </span>
                        <br>
                    </th>
                    <td>
                        {{$data['email']}}
                        <br>
                    </td>
                </tr>
                <tr>
                    <th style="text-align: left;">
                            <span>
                                Password
                            </span>
                        <br>
                    </th>
                    <td>
                        {{$data['password']}}
                        <br>
                    </td>
                </tr>
                <tr>
                    <th style="text-align: left;">
                        &nbsp;
                        <br>
                    </th>
                    <td style="text-align: center;">
                        <a style="border-radius:3px;background:#3aa54c;color:#fff;display:block;font-weight:700;font-size:16px;line-height:1.25em;margin:24px auto 24px;padding:10px 18px;text-decoration:none;width:180px;text-align:center" title="Login merchant panel"  href="{{route('merchant.login')}}" target="_blank">
                            Login Now
                        </a>
                    </td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
    <div style="margin: 25px 20px 15px; padding: 0px;">
        <div>
            <span>
                After logging in, please do change your password!
            </span>
            <br>
        </div>
        <div>
            <br>
        </div>
        <p>
            To create a new delivery, once you are in your dashboard. Click on Create Order Button.
            <br>
        </p>
        <p>
            After a delivery is created, we will send someone to pick it up. Please make sure to create deliveries by 5PM so we can send someone in time!
            <br>
        </p>
        <p style="text-align: justify;">
            Need Help? Call 09639-103314 or Feel free to write to <a href = "mailto: info@metroexpress.com.bd">info@metroexpress.com.bd</a>, for any queries and suggession.&nbsp;
            <br>
        </p>
        <br>
        <div>
            Thanks!
            <br>
        </div>
        <div style="font-weight: bold;">
            <span>
                MetroExpress
            </span>
            Support Team
            <br>
        </div>
    </div>
</div>
<div>
    <br>
</div>
</body>
</html>








