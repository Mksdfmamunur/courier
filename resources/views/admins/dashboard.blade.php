@extends('admins.layouts.app')
@section('title')
     Dashboard
@stop
@section('dashboard','active')
@section('css')

@stop

@section('content')
    <?php
    $order = \App\Order::get();
    $order_deleted = \App\Order::where('delivery_status',7)->get();
    $order_rejected = \App\Order::where('delivery_status',11)->get();
    $total_order = (count($order)-(count($order_deleted)+count($order_rejected)));

    $order_paids = \App\Order::where('payment_status',"Paid")->get();
    $paid_amount = 0;
    foreach ($order_paids as $paid)
    {
        $paid_amount+=$paid->paid_out;
    }
    $order_dues = \App\Order::where('delivery_status',4)->where('payment_status',"Unpaid")->get();
    $due_amount = 0;
    foreach ($order_dues as $due)
    {
        $due_amount+=$due->paid_out;
    }

    $order_delivered = \App\Order::where('delivery_status',4)->get();

    $order_pickup_completed = \App\Order::where('delivery_status',3)->get();
    $order_pickup_approved = \App\Order::where('delivery_status',2)->get();
    $order_pickup_pending = \App\Order::where('delivery_status',1)->get();
    $order_order_confirmed = \App\Order::where('delivery_status',12)->get();
    $order_hold = \App\Order::where('delivery_status',6)->get();
    $order_shipped = \App\Order::where('delivery_status',5)->get();
    $order_returning = \App\Order::where('delivery_status',9)->get();
    $order_returned = \App\Order::where('delivery_status',10)->get();
    $on_processing = count($order_pickup_completed)+count($order_pickup_approved)+count($order_pickup_pending)+count($order_order_confirmed);


    //merchant
            $merchant = \App\Merchant::where('status','Approved')->get();
            $shop = \App\MerchantShop::get();
            $rider = \App\Rider::where('status','Active')->get();
            $customer = \App\Order::select('recipient_phone')->distinct()->get();
    //cash

            $cashCollection = 0;
            foreach ($order_delivered as $cash)
                {
                    $cashCollection +=$cash->amount_to_collect;
                }
            $totalCharge = 0;
            foreach ($order_delivered as $cash)
            {
                $totalCharge +=$cash->total_charge;
            }
            $totalCod = 0;
            foreach ($order_delivered as $cash)
            {
                $totalCod +=$cash->cod_charge;
            }

            $riderPayments= \App\RiderPayment::get();
            $riderPaidOut = 0;
            foreach ($riderPayments as $riderPayment)
            {
                $riderPaidOut +=$riderPayment->paid_amount;
            }

    ?>
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800">Dashboard</h1>

    </div>


    <div class="row">
        <div class="col-xxl-3 col-lg-6">
            <div class="card bg-primary text-white mb-4">
                <div class="container card-body justify-content-between align-items-center">


                    <div class="row">
                        <div class="col-5">
                            <div class="text-white-75 small">Total Delivered</div>
                            <div class="text-lg font-weight-bold">{{count($order_delivered)}}</div>
                            <div class="text-white-75 small">Total Processing</div>
                            <div class="text-lg font-weight-bold">{{$on_processing}}</div>

                        </div>
                        <div class="col-5">
                            <div class="text-white-75 small">Total On Way</div>
                            <div class="text-lg font-weight-bold">{{count($order_shipped)}}</div>
                            <div class="text-white-75 small">Total Returned</div>
                            <div class="text-lg font-weight-bold">{{count($order_returned)}}</div>

                        </div>


                        <div class="d-flex col-2" style="align-items: center">
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-calendar feather-xl text-white-50"><rect x="3" y="4" width="18" height="18" rx="2" ry="2"></rect><line x1="16" y1="2" x2="16" y2="6"></line><line x1="8" y1="2" x2="8" y2="6"></line><line x1="3" y1="10" x2="21" y2="10"></line></svg>
                        </div>
                    </div>




                </div>
                <div class="card-footer d-flex align-items-center justify-content-center">
                    <a class="small text-white" href="{{route('admin.order.view')}}"><i class="fas fa-angle-right"></i> All Orders </a>

                </div>
            </div>
        </div>
        <div class="col-xxl-3 col-lg-6">
            <div class="card bg-warning text-white mb-4">
                <div class="container card-body justify-content-between align-items-center">


                    <div class="row">
                        <div class="col-5">
                            <div class="text-white-75 small">Total Merchant</div>
                            <div class="text-lg font-weight-bold">{{count($merchant)}}</div>
                            <div class="text-white-75 small">Total Shop</div>
                            <div class="text-lg font-weight-bold">{{count($shop)}}</div>

                        </div>
                        <div class="col-5">
                            <div class="text-white-75 small">Total Rider</div>
                            <div class="text-lg font-weight-bold">{{count($rider)}}</div>
                            <div class="text-white-75 small">Total Customer</div>
                            <div class="text-lg font-weight-bold">{{count($customer)}}</div>

                        </div>


                        <div class="d-flex col-2" style="align-items: center">
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-calendar feather-xl text-white-50"><rect x="3" y="4" width="18" height="18" rx="2" ry="2"></rect><line x1="16" y1="2" x2="16" y2="6"></line><line x1="8" y1="2" x2="8" y2="6"></line><line x1="3" y1="10" x2="21" y2="10"></line></svg>
                        </div>
                    </div>




                </div>
                <div class="card-footer d-flex align-items-center justify-content-center">
                    <a class="small text-white" href="{{route('admin.merchant.approve')}}"><i class="fas fa-angle-right"></i> All Merchants</a>
                    <a class="small text-white pl-4" href="{{route('admin.rider.view')}}"><i class="fas fa-angle-right"></i> All Riders</a>
                </div>
            </div>
        </div>
        <div class="col-xxl-3 col-lg-6">
            <div class="card bg-success text-white mb-4">
                <div class="container card-body justify-content-between align-items-center">


                    <div class="row">
                        <div class="col-5">
                            <div class="text-white-75 small">Total Cash Collected</div>
                            <div class="text-lg font-weight-bold">BDT {{$cashCollection}}</div>
                            <div class="text-white-75 small">Total Merchant Paid</div>
                            <div class="text-lg font-weight-bold">Not Available</div>

                        </div>
                        <div class="col-5">
                            <div class="text-white-75 small">Merchant Due</div>
                            <div class="text-lg font-weight-bold">Not Available</div>
                            <div class="text-white-75 small">Total Fee Collected</div>
                            <div class="text-lg font-weight-bold">BDT {{$totalCharge}}</div>

                        </div>


                        <div class="d-flex col-2" style="align-items: center">
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-calendar feather-xl text-white-50"><rect x="3" y="4" width="18" height="18" rx="2" ry="2"></rect><line x1="16" y1="2" x2="16" y2="6"></line><line x1="8" y1="2" x2="8" y2="6"></line><line x1="3" y1="10" x2="21" y2="10"></line></svg>
                        </div>
                    </div>




                </div>
                <div class="card-footer d-flex align-items-center justify-content-between">
                    <a class="small text-white stretched-link" href="#">View All</a>
                    <div class="small text-white"><i class="fas fa-angle-right"></i></div>
                </div>
            </div>
        </div>
        <div class="col-xxl-3 col-lg-6">
            <div class="card bg-danger text-white mb-4">
                <div class="container card-body justify-content-between align-items-center">


                    <div class="row">
                        <div class="col-5">
                            <div class="text-white-75 small">Total Expense</div>
                            <div class="text-lg font-weight-bold">Not Available</div>
                            <div class="text-white-75 small">Total COD Collect</div>
                            <div class="text-lg font-weight-bold">BDT {{$totalCod}}</div>

                        </div>
                        <div class="col-5">
                            <div class="text-white-75 small">Total Rider Payment</div>
                            <div class="text-lg font-weight-bold">BDT {{$riderPaidOut}}</div>
                            <div class="text-white-75 small">Let Net Revenue</div>
                            <div class="text-lg font-weight-bold">Not Available</div>

                        </div>


                        <div class="d-flex col-2" style="align-items: center">
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-calendar feather-xl text-white-50"><rect x="3" y="4" width="18" height="18" rx="2" ry="2"></rect><line x1="16" y1="2" x2="16" y2="6"></line><line x1="8" y1="2" x2="8" y2="6"></line><line x1="3" y1="10" x2="21" y2="10"></line></svg>
                        </div>
                    </div>




                </div>
                <div class="card-footer d-flex align-items-center justify-content-between">
                    <a class="small text-white stretched-link" href="#">View All</a>
                    <div class="small text-white"><i class="fas fa-angle-right"></i></div>
                </div>
            </div>
        </div>
    </div>

    <!-- Content Row -->
    <div class="row">
        <div class="col-xl-3 mb-4">
            <div class="card card-header">Today's Delivery</div>
            <div class="card-body">
                <div class="chart-pie"><canvas id="myPieChart" width="100%" height="30"></canvas></div>
            </div>
            <div class="card-footer small text-muted">Shipped+delivered</div>
        </div>
        <div class="col-xl-3 mb-4">
            <div class="card card-header">Order confirmed VS to print</div>
            <div class="card-body">
                <div class="chart-pie"><canvas id="myPieChart2" width="100%" height="30"></canvas></div>
            </div>
            <div class="card-footer small text-muted">shipped+pp+pa+pc</div>
        </div>
        <div class="col-xl-6 mb-4">
            <div class="card card-header-actions">
                <div class="card-header">
                    Total orders by month
                    <div class="dropdown no-caret">
                        <button class="btn btn-transparent-dark btn-icon dropdown-toggle" id="areaChartDropdownExample" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="text-gray-500" data-feather="more-vertical"></i></button>
                        <div class="dropdown-menu dropdown-menu-right animated--fade-in-up" aria-labelledby="areaChartDropdownExample">
                            <a class="dropdown-item" href="#!">Last 12 Months</a>
                            <a class="dropdown-item" href="#!">Last 30 Days</a>
                            <a class="dropdown-item" href="#!">Last 7 Days</a>
                            <a class="dropdown-item" href="#!">This Month</a>
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item" href="#!">Custom Range</a>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <div class="chart-bar"><canvas id="myBarChart" width="100%" height="30"></canvas></div>
                </div>
            </div>
        </div>
    </div>



@stop
@section('script')
    <script src="{{asset('public/back-end/js/chart.min.js')}}"></script>
    <script src="{{asset('public/back-end/js/chart-bar-demo.js')}}"></script>
    <script src="{{asset('public/back-end/js/chart-pie-demo.js')}}"></script>
    @toastr_render

@stop