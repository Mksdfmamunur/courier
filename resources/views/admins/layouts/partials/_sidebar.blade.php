<!-- Sidebar -->
<ul class="navbar-nav bg-gradient-primary default-primary-color sidebar sidebar-dark accordion"  id="accordionSidebar">
    <!-- Sidebar - Brand -->
    <a class="sidebar-brand d-flex align-items-center justify-content-center" href="{{route('admin.dashboard')}}" style="text-decoration: none;font-size: 28px;color: black;">
        <div class="sidebar-brand-icon rotate-n-15">
            <img src="{{asset('public/icon/logo.png')}}" alt="Metro Express" style="width: 100px">
        </div>
        <div class="" style="">Metro Express</div>
    </a>
    <hr class="sidebar-divider my-0">
    <li class="nav-item @yield('dashboard')">
        <a class="nav-link" href="{{route('admin.dashboard')}}">
            <i class="fas fa-fw fa-tachometer-alt"></i>
            <span>Dashboard</span></a>
    </li>

    <li class="nav-item @yield('merchant')">
        <a class="nav-link @yield('collapsed-merchant', 'collapsed')" href="#" data-toggle="collapse" data-target="#merchant" aria-expanded="true" aria-controls="collapseTwo">
            <i class="fas fa-fw fa-cog"></i>
            <span>Merchants</span>
        </a>
        <div id="merchant" class="collapse @yield('merchant-c')" >
            <div class="bg-white py-2 collapse-inner rounded">
                <a class="collapse-item @yield('MR')" href="{{route('admin.merchant.request')}}">Merchant Request</a>
                <a class="collapse-item @yield('MA')" href="{{route('admin.merchant.approve')}}">Merchant Approve</a>
                <a class="collapse-item @yield('MRJ')" href="{{route('admin.merchant.reject')}}">Merchant Reject</a>
                <a class="collapse-item @yield('MP')" href="{{route('admin.merchant.plan')}}">Merchant Plan</a>
            </div>
        </div>
    </li>
    <li class="nav-item @yield('rider')">
        <a class="nav-link @yield('collapsed-rider', 'collapsed')" href="#" data-toggle="collapse" data-target="#riders" aria-expanded="true" aria-controls="collapseUtilities">
            <i class="fas fa-fw fa-wrench"></i>
            <span>Riders</span>
        </a>
        <div id="riders" class="collapse @yield('rider-c')" >
            <div class="bg-white py-2 collapse-inner rounded">
                <a class="collapse-item @yield('RL')" href="{{route('admin.rider.view')}}">Rider List</a>
                <a class="collapse-item @yield('RR')" href="{{route('admin.rider.register')}}">Rider Registration</a>
            </div>
        </div>
    </li>
    <li class="nav-item @yield('pickup')">
        <a class="nav-link @yield('collapsed-pickup', 'collapsed')" href="#" data-toggle="collapse" data-target="#pickups" aria-expanded="true" aria-controls="collapsePages">
            <i class="fas fa-fw fa-folder"></i>
            <span>Pickups</span>
        </a>
        <div id="pickups" class="collapse @yield('pickup-c')" >
            <div class="bg-white py-2 collapse-inner rounded">
                <a class="collapse-item @yield('PA')" href="{{route('admin.pickup.approve')}}">Pickup Approve</a>
                <a class="collapse-item @yield('PL')" href="{{route('admin.pickup.list')}}">Pickup List</a>
            </div>
        </div>
    </li>
    <li class="nav-item @yield('order')">
        <a class="nav-link @yield('collapsed-order', 'collapsed')" href="#" data-toggle="collapse" data-target="#orders" aria-expanded="true" aria-controls="collapsePages">
            <i class="fas fa-fw fa-folder"></i>
            <span>Orders</span>
        </a>
        <div id="orders" class="collapse @yield('order-c')" >
            <div class="bg-white py-2 collapse-inner rounded">
                <a class="collapse-item @yield('VO')" href="{{route('admin.order.view')}}">View Orders</a>
                <a class="collapse-item @yield('PO')" href="{{route('admin.order.pending')}}">অর্ডার প্রিন্ট ও কনফার্ম</a>
                <a class="collapse-item @yield('CO')" href="{{route('admin.order.confirm')}}">Agent Assign</a>
                <a class="collapse-item @yield('SO')" href="{{route('admin.order.shipped')}}">ডেলিভারি হিসাব</a>
                <a class="collapse-item @yield('DO')" href="{{route('admin.order.delivered')}}">Delivered Orders</a>
            </div>
        </div>
    </li>
    <li class="nav-item @yield('payment')">
        <a class="nav-link @yield('collapsed-payment', 'collapsed')" href="#" data-toggle="collapse" data-target="#payment" aria-expanded="true" aria-controls="collapseTwo">
            <i class="fas fa-fw fa-cog"></i>
            <span>Payments</span>
        </a>
        <div id="payment" class="collapse @yield('payment-c')" >
            <div class="bg-white py-2 collapse-inner rounded">
                <a class="collapse-item @yield('RP')" href="{{route('admin.payment.rider-payment')}}">Rider Payment</a>
                <a class="collapse-item @yield('RPL')" href="{{route('admin.payment.rider-payment-list')}}">Rider Payment List</a>
                <a class="collapse-item @yield('CMPI')" href="{{route('admin.payment.merchant-payment-invoice')}}">Create Merchant Invoice</a>
                <a class="collapse-item @yield('CMPIL')" href="{{route('admin.payment.merchant-invoice-list')}}">Merchant Invoice List</a>
                <a class="collapse-item @yield('RPL')" href="{{route('admin.payment.rider-payment-list')}}">Rider Payment List</a>
            </div>
        </div>
    </li>
    <li class="nav-item @yield('business')">
        <a class="nav-link @yield('collapsed-business', 'collapsed')" href="#" data-toggle="collapse" data-target="#business" aria-expanded="true" aria-controls="collapseTwo">
            <i class="fas fa-fw fa-cog"></i>
            <span>Business Settings</span>
        </a>
        <div id="business" class="collapse @yield('business-c')" >
            <div class="bg-white py-2 collapse-inner rounded">
                <a class="collapse-item @yield('PL')" href="{{route('admin.business.plan')}}">Plan List</a>
                <a class="collapse-item @yield('CAL')" href="{{route('admin.business.coverage-area')}}">Coverage Area List</a>
            </div>
        </div>
    </li>
    <!-- Divider -->
    <hr class="sidebar-divider d-none d-md-block">
    <!-- Sidebar Toggler (Sidebar) -->
    <div class="text-center d-none d-md-inline">
        <button class="rounded-circle border-0" id="sidebarToggle"></button>
    </div>
</ul>
<!-- End of Sidebar -->