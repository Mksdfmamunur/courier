<!DOCTYPE html>
<html lang="en">

<head>
    <title>MetroExpress || @yield('title')</title>
    <meta charset="utf-8" />
    <meta name="_token" content="{{ csrf_token() }}">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="description" content="#">
    <meta name="keywords" content="#">
    <meta name="author" content="MetroExpress">
    <link rel="icon" href="{{asset('public/icon/logo.png')}}" type="image/x-icon">
    <link rel="stylesheet" href="{{ asset('public/css/bootstrap.min.css')}}">
    <link href="{{asset('public/back-end/css/jm-lead.min.css')}}" rel="stylesheet">
    <link href="{{asset('public/back-end/css/jm-lead.css')}}" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('public/assets/fontawesome-5.14.0/css/all.css')}}">
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/css/select2.min.css" rel="stylesheet" />
    <link href='https://fonts.googleapis.com/css?family=Titillium Web' rel='stylesheet'>
    <link href="https://fonts.maateen.me/solaiman-lipi/font.css" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.css">
    <link rel="stylesheet" type="text/css" href="http://cdn.datatables.net/1.10.22/css/jquery.dataTables.min.css"/>
    <!-- Custom styles for this template-->
    <link href="{{asset('public/back-end/css/jm-default.css')}}" rel="stylesheet">
    <style>
        #dataTables_Filter{
            float: right;
        }
        #content-wrapper {
            position: relative;
            min-height: 100vh;
        }

        .content-wrap {
            padding-bottom: 5.5rem;    /* Footer height */
        }

        footer {
            position: absolute;
            bottom: 0;
            width: 100%;
            height: 2.5rem;            /* Footer height */
        }
    </style>
@yield('css')
</head>

<body id="page-top">
<div id="wrapper">
    @include('admins.layouts.partials._sidebar')
    <div id="content-wrapper" class="d-flex flex-column">
        <div id="content" class="content-wrap">
            @include('admins.layouts.partials._topbar')
            <div class="container-fluid " style="margin-top: 90px;">
            @yield('content')
            </div>
        </div>
        <footer class="sticky-footer bg-white">
            <div class="container my-auto">
                <div class="copyright text-center my-auto">
                    <span>Copyright &copy; MetroExpress 2020</span>
                </div>
            </div>
        </footer>
        <a class="scroll-to-top rounded" href="#page-top">
            <i class="fas fa-angle-up"></i>
        </a>

        <!-- Logout Modal-->
        <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
                        <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                    </div>
                    <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
                    <div class="modal-footer">
                        <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
                        <a class="btn btn-primary" href="login.html">Logout</a>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>
        <script src="{{ asset('public/js/jquery.min.js')}}"></script>
        <script src="{{ asset('public/js/popper.min.js')}}"></script>
<script src="{{asset('public/back-end/js/jm-lead.min.js')}}"></script>
        <script type="text/javascript" src="//cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script><script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js"></script>
        <script src="{{ asset('public/js/numberCheck.js')}}"></script>
        <script type="text/javascript" src="https://cdn.datatables.net/1.10.22/js/jquery.dataTables.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>
        <script type="text/javascript" src="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.js"></script>
        <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.css"/>
        <script src="{{asset('public/back-end/asset/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
        <!-- Core plugin JavaScript-->
        <script src="{{asset('public/back-end/asset/jquery-easing/jquery.easing.min.js')}}"></script>
        <!-- Custom scripts for all pages-->

@yield('script')
</body>

</html>
