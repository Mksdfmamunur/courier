@extends('admins.layouts.app')
@section('title')
    Pending Order
@stop
@section('order','active')
@section('collapsed-order','')
@section('order-c','show')
@section('PO','active')
@section('css')

@stop

@section('content')
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <div class="row p-0">
                        <div class="col text-left">
                            <h3>Pending Order</h3>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-striped" id="dataTable" width="100%" >
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Shop</th>
                                <th>Customer Details</th>
                                <th>Customer Area</th>
                                <th>Amount</th>
                                <th>Weight</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            $orders = \App\Order::where('delivery_status',1)
                                ->orWhere('delivery_status', 2)
                                ->orWhere('delivery_status', 3)
                                ->orderBy('id','desc')->get();
                            ?>
                            @foreach($orders as $key=> $order)
                                <tr>
                                    <td>{{$key+1}}</td>
                                    <td>{{$order->merchantShop->shop_name}}</td>
                                    <td>
                                        <span class="txt_bottom_all"><i class="fas fa-user" aria-hidden="true"></i></span> {{$order->recipient_name}} <br>
                                        <span class="txt_bottom_all"><i class="fas fa-phone-square" aria-hidden="true"></i></span> {{$order->recipient_phone}} <br>
                                        <span class="txt_bottom_all"><i class="fas fa-location-arrow" aria-hidden="true"></i></span> {{$order->recipient_address}}
                                    </td>
                                    <td>{{$order->coverageArea->area}}</td>
                                    <td>{{$order->amount_to_collect}}</td>
                                    <td>{{$order->order_weight}} Kg</td>
                                    <td>
                                        <a href="{{route('admin.order.edit', encrypt($order->id))}}" class="btn btn-warning phone1"><i class='fas fa-wrench'></i></a>
                                        <a href="{{route('admin.order.print',encrypt($order->id))}} }}" class="btn btn-admin-secondary btnprn phone1"><i class="fa fa-print" aria-hidden="true"></i></a>
                                        <form action="{{route('admin.order.single-status')}}" method="post">
                                            @csrf
                                            <input type="hidden" name="id" value="{{$order->id}}">
                                            <input type="hidden" name="delivery_status" value="12">
                                            <button type="submit" class="btn btn-admin-primary">Confirm</button>
                                        </form>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
@section('script')
    @toastr_render
    <script src="{{ asset('public/js/jquery.printPage.js') }}"></script>
    <script>
        $("#dataTable").dataTable();

        $(document).ready(function(){
            $('.btnprn').printPage();
            $('.btnprn1').printPage();
        });
    </script>
@stop