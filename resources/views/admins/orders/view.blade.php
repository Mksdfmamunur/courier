@extends('admins.layouts.app')
@section('title')
    View Orders
@stop
@section('order','active')
@section('collapsed-order','')
@section('order-c','show')
@section('VO','active')
@section('css')

@stop

@section('content')
    <div class="container-fluid p-0 mt-1">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col text-left">
                        <h2>View All Orders</h2>
                    </div>
                </div>
                <form action="#" method="post" novalidate="novalidate">
                    <div class="row">
                        <div class="col-lg-2 col-md-2 col-sm-12 pl-1 pr-1">
                            <input type="text" class="form-control search-slt" id="tracking_id" placeholder="Tracking ID">
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-12 pl-1 pr-1">
                            <input type="text" class="form-control search-slt" id="receiver" placeholder="Receiver Name/Phone">
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-12 pl-1 pr-1">
                            <input type="text" class="form-control" name="date" id="date" readonly/>
                        </div>
                        <div class="col-lg-2 col-md-2 col-sm-12 pl-1 pr-1">
                            <select class="form-control search-slt" id="shop">
                                <option value="0" selected>Select shop</option>
                                @foreach (\App\MerchantShop::get() as $shop)
                                    <option value="{{$shop->id}}">{{$shop->shop_name}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-lg-2 col-md-2 col-sm-12 pl-1 pr-1">
                            <select class="form-control search-slt" id="delivery_status">
                                <option value="0" selected>Delivery Status</option>
                                @foreach(\App\Status::get() as $status)
                                    <option value="{{$status->id}}">{{$status->name}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </form>
            </div>
        </div>

    </div>

    <hr>
    <div class="container-fluid">
        <div class="row border-bottom">
            <div class="col-lg-1 col-md-1 col-sm-12 pl-1 pr-1 ap_table">
                Creation Date
            </div>
            <div class="col-lg-2 col-md-2 col-sm-12 pl-1 pr-1 ap_table">
                Tracking ID
            </div>
            <div class="col-lg-1 col-md-1 col-sm-12 pl-1 pr-1 ap_table">
                Shop
            </div>
            <div class="col-lg-3 col-md-3 col-sm-12 pl-1 pr-1 ap_table">
                Customer Details
            </div>
            <div class="col-lg-2 col-md-2 col-sm-12 pl-1 pr-1 ap_table">
                Delivery Status
            </div>
            <div class="col-lg-1 col-md-1 col-sm-12 pl-1 pr-1 ap_table">
                Payment Info
            </div>
            <div class="col-lg-1 col-md-1 col-sm-12 pl-1 pr-1 ap_table">
                Payment Status
            </div>
            <div class="col-lg-1 col-md-1 col-sm-12 pl-1 pr-1 ap_table">
                Action
            </div>
        </div>
        <div id="orderData">

        </div>
    </div>
@stop
@section('script')
    @toastr_render
    <script type="text/javascript" src="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.js"></script>    @toastr_render
    <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.css"/>
    @toastr_render
    <script>
        $(function () {
            let dateInterval = getQueryParameter('date');
            let start = moment().subtract(29, 'days').startOf('month');
            let end = moment().endOf('isoWeek');
            if (dateInterval) {
                dateInterval = dateInterval.split(' - ');
                start = dateInterval[0];
                end = dateInterval[1];
            }
            $('#date').daterangepicker({
                'autoApply':true,
                "showDropdowns": true,
                "showWeekNumbers": true,
                "alwaysShowCalendars": true,
                startDate: start,
                endDate: end,
                locale: {
                    format: 'DD/MM/YYYY',
                    firstDay: 1,
                },
                ranges: {
                    'Today': [moment(), moment()],
                    'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                    'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                    'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                    'This Month': [moment().startOf('month'), moment().endOf('month')],
                    'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')],
                    'This Year': [moment().startOf('year'), moment().endOf('year')],
                    'Last Year': [moment().subtract(1, 'year').startOf('year'), moment().subtract(1, 'year').endOf('year')],
                    'All time': [moment().subtract(30, 'year').startOf('month'), moment().endOf('month')],
                }
            });
            // $('#date').val('');
        });
        function getQueryParameter(name) {
            const url = window.location.href;
            name = name.replace(/[\[\]]/g, "\\$&");
            const regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
                results = regex.exec(url);
            if (!results) return null;
            if (!results[2]) return '';
            return decodeURIComponent(results[2].replace(/\+/g, " "));
        }

        $('#date').on('change',function () {
            order_data();
        });
        $('#shop').on('change',function () {
            order_data();
        });
        $('#delivery_status').on('change',function () {
            order_data();
        });
        $('#tracking_id').on('keyup',function () {
            order_data();
        });
        $('#receiver').on('keyup',function () {
            order_data();
        });
        function order_data() {
            var tracking_id = $('#tracking_id').val();
            var receiver = $('#receiver').val();
            var date = $('#date').val();
            var shop = $('#shop').val();
            var delivery_status = $('#delivery_status').val();
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                }
            });
            $.ajax({
                url: "{{route('admin.order.search-data')}}",
                method: 'POST',
                data: {
                    tracking_id:tracking_id,
                    receiver:receiver,
                    date:date,
                    shop:shop,
                    delivery_status:delivery_status,
                },
                success: function (data) {
                    $('#orderData').html('');
                    $('#orderData').html(data.success);
                }
            });
        }
        $(document).on('click', '.pagination a', function(event){
            event.preventDefault();
            var page = $(this).attr('href').split('page=')[1];
            $('#hidden_page').val(page);
            $('#page .pagination li').removeClass('active');
            $(this).parent().addClass('active');
            var tracking_id = $('#tracking_id').val();
            var receiver = $('#receiver').val();
            var date = $('#date').val();
            var shop = $('#shop').val();
            var delivery_status = $('#delivery_status').val();
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                }
            });
            $.ajax({
                url: "{{route('admin.order.search-data')}}",
                method: 'POST',
                data: {
                    page:page,
                    tracking_id:tracking_id,
                    receiver:receiver,
                    date:date,
                    shop:shop,
                    delivery_status:delivery_status,
                },
                success:function(data)
                {
                    $('#orderData').html('');
                    $('#orderData').html(data.success);

                }
            });
        });
    </script>
@stop