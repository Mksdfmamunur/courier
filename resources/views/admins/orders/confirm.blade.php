@extends('admins.layouts.app')
@section('title')
    Confirm Order
@stop
@section('order','active')
@section('collapsed-order','')
@section('order-c','show')
@section('CO','active')
@section('css')

@stop

@section('content')

    <div class="row">
        <div class="col">
            <div class="card">
                <div class="card-header">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="input-group">
                                <input type="text" class="form-control" id="search" placeholder="Customer Number /Tracking Id">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <form action="{{route('admin.order.status')}}" method="post">
                        @csrf
                        <div class="table-responsive">
                            <table class="table table-striped" id="dataTable" width="100%" >
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Track ID</th>
                                    <th>Shop Name</th>
                                    <th>Customer Name</th>
                                    <th>Customer Phone</th>
                                    <th>Customer Area</th>
                                    <th>Customer Address</th>
                                    <th>Collection Amount</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>

                                </tbody>
                            </table>
                        </div>
                        <div class="row">
                            <div class="offset-md-8 col-md-4 mb-2">
                                <input type="hidden" value="5" name="delivery_status">
                                <label for="rider">ডেলিভারি করার জন্য এজেন্ট সিলেক্ট করুন</label>
                                <select class="form-control" id="rider" name="rider" required>

                                    <option value="" selected disabled>Select Delivery Agent</option>
                                    @foreach(\App\Rider::get() as $rider)
                                        <option value="{{$rider->id}}">{{$rider->nick_name}}</option>
                                    @endforeach
                                </select>
                            </div>

                        </div>
                        <div class="row mt-2">
                            <div class="offset-md-8 col-md-4">
                                <button type="submit" class="btn btn-admin-primary float-right">Shipped</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@stop
@section('script')
    @toastr_render
    <script>
        // $("#dataTable").dataTable();
        $(document).ready(function () {
            function addRow(search) {
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                    }
                });
                $.ajax({
                    url: "{{route('admin.order.fetch-data-assign')}}",
                    method: 'POST',
                    data: {
                        search:search,
                        delivery_status:12,
                        delivery_status1:6,

                    },
                    success:function(data)
                    {
                        console.log(data);
                        if (Object.keys(data).length === 0 && data.constructor === Object) {

                            $("#search").val("");
                            toastr.error('Sorry  phone number/tracking id not found.');
                        }
                        else {
                            var newRow = $("<tr>");
                            var cols = "";
                            cols += '<td><input type="hidden" name="id[]" value="'+data.order.id+'">'+ (counter+1) +'</td>';
                            cols += '<td>'+data.order.tracking_id+'</td>';
                            cols += '<td>'+data.shop.shop_name+'</td>';
                            cols += '<td>'+data.order.recipient_name+'</td>';
                            cols += '<td>'+data.order.recipient_phone+'</td>';
                            cols += '<td><b>'+data.coverageArea.area+'</b></td>';
                            cols += '<td>'+data.order.recipient_address+'</td>';
                            cols += '<td>'+data.order.amount_to_collect+'</td>';
                            cols += '<td><a class="ibtnDel btn btn-admin-danger text-white"><i class="far fa-trash-alt"></i></a></td>';
                            newRow.append(cols);
                            $("#dataTable").append(newRow);
                            counter++;
                            $("#search").val("");
                        }
                    }
                });
            }
            var counter = 0;

            $("#search").on("change", function () {
                var search = $("#search").val();
                addRow(search);
            });
            $("#dataTable").on("click", ".ibtnDel", function (event) {
                $(this).closest("tr").remove();
                counter -= 1
            });


        });
    </script>
@stop