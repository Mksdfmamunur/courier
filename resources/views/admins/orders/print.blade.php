
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Custom fonts for this template-->
    {{--    <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">--}}

    <link href="https://fonts.maateen.me/solaiman-lipi/font.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Libre+Barcode+128&display=swap" rel="stylesheet">
    <style>

        #invoice-POS {
            box-shadow: 0 0 1in -0.25in rgba(0, 0, 0, 0.5);
            margin: 0 auto;
            width: 80mm;
            background: #FFF;
            min-height: 200mm;
        }
        #top,#mid,#bot {
            border-bottom: 1px solid #EEE;
        }

        #top {
            padding-top: 20px;
            margin-bottom: 20px;
        }

        #mid {
            min-height: 80px;
        }

        #bot {
            min-height: 40px;
            top: 600px;
            bottom: 0;
            position: absolute;
            margin-left: 10px;
            margin-right: 10px;

        }

        .logo{
            height: 105px;
            width: 260px;
            background: url({{asset('public/icon/invoice.svg')}}) no-repeat;
            background-size: 260px 104px;
        }
        .invHeader {
            font-family: 'SolaimanLipi', sans-serif !important;
            font-size: 18px;
            margin-top: 2px;
            margin-bottom: 5px;
        }

        p.bameSpace {
            padding-left: 10px;
            margin-top: 0px;
            margin-bottom: 0px;
        }
        .info {
            display: block;
            margin-left: 0;
            margin-bottom: 10px;
        }
        #legalcopy {
            margin-top: 5mm;
        }

        .legal {
            font-family: 'Libre Barcode 128', cursive;
            font-size: 60px;
            text-align: center;
            height: 40px;

        }

        .pos-inv-space-hunter {
            margin-top: 2px;
            margin-bottom: 2px;
        }

        .space-hunter {
            margin-top: 0px;
            margin-bottom: 0px;
        }


        .invBangla {
            font-family: 'SolaimanLipi', sans-serif !important;
            font-size: 13px;
            line-height: 1.1;


        }

        .invBangla2 {
            font-family: 'SolaimanLipi', sans-serif !important;
            font-size: 16px;
        }

        .invCustPhn {
            font-family: 'SolaimanLipi', sans-serif !important;
            font-size: 22px;
            letter-spacing: 2px;
            padding-left: 28px;
            margin-top: 0px;
            margin-bottom: 0px;
        }
    </style>
</head>

<body class="bg-gradient-primary">
<div id="invoice-POS">

    <p>Date: {{Carbon\Carbon::now()->format('M d Y')}} <span>Time: {{Carbon\Carbon::now()->format('g:i A')}}</span></p>
    <center id="top">
        <div class="logo">


        </div>
    </center>

    <div id="mid">
        <div class="info">

            <h4 class="pos-inv-space-hunter invHeader">#SENDER </h4>
            <p class="bameSpace">{{$order->merchantShop->shop_name}}</p>
            <p class="bameSpace">{{$order->merchantShop->pickup_number}}</p>
        </div>
        <div class="info">
            <h4 class="pos-inv-space-hunter invHeader">#RECEIVER</h4>
            <p class="bameSpace invBangla2">{{$order->recipient_name}}</p>
            <p class="bameSpace invCustPhn">{{$order->recipient_phone}}</p>
            <p class="bameSpace invBangla2 invBangla">{{$order->recipient_address}}</p>
        </div>
        <div class="info">

            <p class="bameSpace invBangla"> <span class="pos-inv-space-hunter invHeader"><?php if($order->merchant_instruction!="") echo "NOTE:"?></span>{{$order->merchant_instruction}}</p>
        </div>
        <div class="info">
            <p class="invCustPhn"><span class="pos-inv-space-hunter invHeader">AMOUNT: BDT </span>{{$order->amount_to_collect}}</p>
        </div>
    </div>

    <div id="bot" >
        <div>
            <p class="pos-inv-space-hunter">_______________</p>
            <p class="pos-inv-space-hunter">Signature</p>
        </div>
        <div id="legalcopy">
        <!--<p style="text-align: center">{{$order->coverageArea->area}}</p>-->

            <?php
            use CodeItNow\BarcodeBundle\Utils\BarcodeGenerator;

            $barcode = new BarcodeGenerator();
            $barcode->setText($order->tracking_id);
            $barcode->setType(BarcodeGenerator::Code128);
            $barcode->setScale(2);
            $barcode->setThickness(30);
            $barcode->setFontSize(10);
            $code = $barcode->generate();

            echo '<img src="data:image/png;base64,'.$code.'" width="280" />';
            ?>
        </div>
    </div>
</div>
<!--End Invoice-->

<!-- Bootstrap core JavaScript-->
<script src="{{ asset('public/js/jquery.min.js')}}"></script>
</body>

</html>