@extends('admins.layouts.app')
@section('title')
    Merchant Request
@stop
@section('business','active')
@section('collapsed-business','')
@section('business-c','show')
@section('PL','active')
@section('css')

@stop

@section('content')
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <div class="row p-0">
                        <div class="col text-left">
                            <h3>Plan List</h3>
                        </div>
                        <div class="col text-right">
                            <button type="button" class="btn btn-success" data-toggle="modal" data-target="#plan-create-Modal"><i class='fas fa-plus'></i>Create Plan</button>
                            <!--plan-create Modal -->
                            <div class="modal fade" id="plan-create-Modal" role="dialog">
                                <div class="modal-dialog modal-md">
                                    <!-- Modal content-->
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h4 class="modal-title">Plan Details</h4>
                                            <button type="button" class="close btn btn-warning" data-dismiss="modal">&times;</button>
                                        </div>
                                        <form action="{{route('admin.business.store-plan')}}" method="post">
                                            @csrf
                                            <div class="modal-body">
                                                <div class="container">
                                                    <div class="row">
                                                        <div class="col-xl-12">
                                                            <!-- Account details card-->
                                                            <div class="card mb-4">
                                                                <div class="card-body text-left">
                                                                    <div class="form-row">
                                                                        <!-- Form Group (first name)-->
                                                                        <div class="col-sm-6">
                                                                            <label>Plan Name</label>
                                                                        </div>
                                                                        <!-- Form Group (last name)-->
                                                                        <div class="form-group col-sm-6">
                                                                            <input class="form-control" id="plan_name" type="text" placeholder="Plan name" name="plan_name">
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-row">
                                                                        <!-- Form Group (first name)-->
                                                                        <div class=" col-sm-6">
                                                                            <label>Inside Dhaka Regular</label>
                                                                        </div>
                                                                        <!-- Form Group (last name)-->
                                                                        <div class="form-group col-sm-3">

                                                                            <input class="form-control number" id="idr_charge" type="text" placeholder="rate" value="" name="idr_charge" required>

                                                                        </div>
                                                                        <div class="form-group col-sm-3">
                                                                            <div class="custom-control custom-switch">
                                                                                <input type="checkbox" class="custom-control-input" id="idr_cod" name="idr_cod" >
                                                                                <label class="custom-control-label" for="idr_cod">COD</label>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-row">
                                                                        <!-- Form Group (first name)-->
                                                                        <div class=" col-sm-6">
                                                                            <label>Inside Dhaka Urgent</label>
                                                                        </div>
                                                                        <!-- Form Group (last name)-->
                                                                        <div class="form-group col-sm-3">

                                                                            <input class="form-control number" id="idu_charge" type="text" placeholder="rate" value="" name="idu_charge" required>
                                                                        </div>
                                                                        <div class="form-group col-sm-3">
                                                                            <div class="custom-control custom-switch">
                                                                                <input type="checkbox" class="custom-control-input" id="idu_cod" value="yes" name="idu_cod">
                                                                                <label class="custom-control-label" for="idu_cod">COD</label>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-row">
                                                                        <div class=" col-sm-6">
                                                                            <label>Dhaka Sub Charge</label>
                                                                        </div>
                                                                        <div class="form-group col-sm-3">
                                                                            <input class="form-control number" id="ds_charge" type="text" placeholder="rate" value="" name="ds_charge" required>
                                                                        </div>
                                                                        <div class="form-group col-sm-3">
                                                                            <div class="custom-control custom-switch">
                                                                                <input type="checkbox" class="custom-control-input" id="ds_cod" value="0" name="ds_cod">
                                                                                <label class="custom-control-label" for="ds_cod">COD</label>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-row">
                                                                        <div class=" col-sm-6">
                                                                            <label>Outside Dhaka Charge</label>
                                                                        </div>
                                                                        <div class="form-group col-sm-3">
                                                                            <input class="form-control number" id="od_charge" type="text" placeholder="rate" value="" name="od_charge" required>
                                                                        </div>
                                                                        <div class="form-group col-sm-3">
                                                                            <div class="custom-control custom-switch">
                                                                                <input type="checkbox" class="custom-control-input" id="od_cod" name="od_cod">
                                                                                <label class="custom-control-label" for="od_cod">COD</label>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-row">
                                                                        <!-- Form Group (first name)-->
                                                                        <div class="col-sm-6">
                                                                            <label>Charge Per Weight</label>
                                                                        </div>
                                                                        <!-- Form Group (last name)-->
                                                                        <div class="form-group col-sm-6">
                                                                            <input class="form-control number" id="cpw" type="text" placeholder="weight Charge" name="cpw" required>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>

                                                    </div>

                                                </div>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="submit" class="btn btn-admin-primary" >Create</button>
                                                <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-striped" id="dataTable" width="100%" cellspacing="0">
                            <thead>
                            <tr>
                                <th>Plan Name</th>
                                <th>InDhk Regular</th>
                                <th>InDhkR COD</th>
                                <th>InDhk Urgent</th>
                                <th>InDhkU COD</th>
                                <th>DhkSub Charge</th>
                                <th>DhkSub COD</th>
                                <th>OutDhk Charge</th>
                                <th>OutDhk COD</th>
                                <th>Charge Per Weight</th>
                                <th>Edit</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach(\App\Plan::orderBy('id','desc')->get() as $plan)
                            <tr>
                                <td>{{$plan->plan_name}}</td>
                                <td>{{$plan->idr_charge}}</td>
                                <td>{{$plan->idr_cod}}</td>
                                <td>{{$plan->idu_charge}}</td>
                                <td>{{$plan->idu_cod}}</td>
                                <td>{{$plan->ds_charge}}</td>
                                <td>{{$plan->ds_cod}}</td>
                                <td>{{$plan->od_charge}}</td>
                                <td>{{$plan->od_cod}}</td>
                                <td>{{$plan->cpw}}</td>
                                <td>
                                    <div class="btn-group">
                                        <button type="button" class="btn btn-success" data-toggle="modal" data-target="#plan-edit-Modal{{$plan->id}}"><i class='fas fa-wrench'></i></button>
                                    </div>
                                    <!--plan-edit Modal -->
                                    <div class="modal fade" id="plan-edit-Modal{{$plan->id}}" role="dialog">
                                        <div class="modal-dialog modal-md">
                                            <!-- Modal content-->
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h4 class="modal-title">Plan Details</h4>
                                                    <button type="button" class="close btn btn-warning" data-dismiss="modal">&times;</button>
                                                </div>
                                                <form action="{{route('admin.business.update-plan')}}" method="post">
                                                    @csrf
                                                    <input type="hidden" name="id" value="{{$plan->id}}">
                                                    <div class="modal-body">
                                                        <div class="container">
                                                            <div class="row">
                                                                <div class="col-xl-12">
                                                                    <!-- Account details card-->
                                                                    <div class="card mb-4">
                                                                        <div class="card-body text-left">
                                                                            <div class="form-row">
                                                                                <!-- Form Group (first name)-->
                                                                                <div class="col-sm-6">
                                                                                    <label>Plan Name</label>
                                                                                </div>
                                                                                <!-- Form Group (last name)-->
                                                                                <div class="form-group col-sm-6">
                                                                                    <input class="form-control" id="plan_name" type="text" placeholder="Plan name" name="plan_name" value="{{$plan->plan_name}}">
                                                                                </div>
                                                                            </div>
                                                                            <div class="form-row">
                                                                                <!-- Form Group (first name)-->
                                                                                <div class=" col-sm-6">
                                                                                    <label>Inside Dhaka Regular</label>
                                                                                </div>
                                                                                <!-- Form Group (last name)-->
                                                                                <div class="form-group col-sm-3">

                                                                                    <input class="form-control number" id="idr_charge" type="text" placeholder="rate" value="{{$plan->idr_charge}}" name="idr_charge" required>

                                                                                </div>
                                                                                <div class="form-group col-sm-3">
                                                                                    <div class="custom-control custom-switch">
                                                                                        <input type="checkbox" class="custom-control-input" id="idr_cod{{$plan->id}}" name="idr_cod" <?php if ($plan->idr_cod ==1) echo "checked" ?> >
                                                                                        <label class="custom-control-label" for="idr_cod{{$plan->id}}">COD</label>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="form-row">
                                                                                <!-- Form Group (first name)-->
                                                                                <div class=" col-sm-6">
                                                                                    <label>Inside Dhaka Urgent</label>
                                                                                </div>
                                                                                <!-- Form Group (last name)-->
                                                                                <div class="form-group col-sm-3">

                                                                                    <input class="form-control number" id="idu_charge" type="text" placeholder="rate" value="{{$plan->idu_charge}}" name="idu_charge" required>
                                                                                </div>
                                                                                <div class="form-group col-sm-3">
                                                                                    <div class="custom-control custom-switch">
                                                                                        <input type="checkbox" class="custom-control-input" id="idu_cod{{$plan->id}}"  name="idu_cod" <?php if ($plan->idu_cod ==1) echo "checked" ?>>
                                                                                        <label class="custom-control-label" for="idu_cod{{$plan->id}}">COD</label>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="form-row">
                                                                                <div class=" col-sm-6">
                                                                                    <label>Dhaka Sub Charge</label>
                                                                                </div>
                                                                                <div class="form-group col-sm-3">
                                                                                    <input class="form-control number" id="ds_charge" type="text" placeholder="rate" value="{{$plan->ds_charge}}" name="ds_charge" required>
                                                                                </div>
                                                                                <div class="form-group col-sm-3">
                                                                                    <div class="custom-control custom-switch">
                                                                                        <input type="checkbox" class="custom-control-input" id="ds_cod{{$plan->id}}"  name="ds_cod" <?php if ($plan->ds_cod ==1) echo "checked" ?>>
                                                                                        <label class="custom-control-label" for="ds_cod{{$plan->id}}">COD</label>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="form-row">
                                                                                <div class=" col-sm-6">
                                                                                    <label>Outside Dhaka Charge</label>
                                                                                </div>
                                                                                <div class="form-group col-sm-3">
                                                                                    <input class="form-control number" id="od_charge" type="text" placeholder="rate" value="{{$plan->od_charge}}" name="od_charge"  required>
                                                                                </div>
                                                                                <div class="form-group col-sm-3">
                                                                                    <div class="custom-control custom-switch">
                                                                                        <input type="checkbox" class="custom-control-input" id="od_cod{{$plan->id}}" name="od_cod" <?php if ($plan->od_cod ==1) echo "checked" ?>>
                                                                                        <label class="custom-control-label" for="od_cod{{$plan->id}}">COD</label>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="form-row">
                                                                                <!-- Form Group (first name)-->
                                                                                <div class="col-sm-6">
                                                                                    <label>Charge Per Weight</label>
                                                                                </div>
                                                                                <!-- Form Group (last name)-->
                                                                                <div class="form-group col-sm-6">
                                                                                    <input class="form-control number" id="cpw" type="text" placeholder="weight charge" name="cpw" value="{{$plan->cpw}}" required>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                            </div>

                                                        </div>
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="submit" class="btn btn-admin-primary" >Update</button>
                                                        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>


@stop
@section('script')
    @toastr_render
    <script>
        $("#dataTable").dataTable();
    </script>
@stop