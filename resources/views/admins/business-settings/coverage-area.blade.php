@extends('admins.layouts.app')
@section('title')
    Merchant Request
@stop
@section('business','active')
@section('collapsed-business','')
@section('business-c','show')
@section('CAL','active')
@section('css')
    <style>
        .switch {
            position: relative;
            display: inline-block;
            width: 60px;
            height: 34px;
        }

        .switch input {
            opacity: 0;
            width: 0;
            height: 0;
        }

        .slider {
            position: absolute;
            cursor: pointer;
            top: 0;
            left: 0;
            right: 0;
            bottom: 0;
            background-color: #ccc;
            -webkit-transition: .4s;
            transition: .4s;
        }

        .slider:before {
            position: absolute;
            content: "";
            height: 26px;
            width: 26px;
            left: 4px;
            bottom: 4px;
            background-color: white;
            -webkit-transition: .4s;
            transition: .4s;
        }

        input:checked + .slider {
            background-color: #2196F3;
        }

        input:focus + .slider {
            box-shadow: 0 0 1px #2196F3;
        }

        input:checked + .slider:before {
            -webkit-transform: translateX(26px);
            -ms-transform: translateX(26px);
            transform: translateX(26px);
        }

        /* Rounded sliders */
        .slider.round {
            border-radius: 34px;
        }

        .slider.round:before {
            border-radius: 50%;
        }
        .select2-container--default .select2-selection--single .select2-selection__rendered {
            color: #444;
            line-height: 28px;
            width: 180px;
        }
    </style>
@stop

@section('content')
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <div class="row p-0">
                        <div class="col text-left">
                            <h3>Coverage Area List</h3>
                        </div>
                        <div class="col text-right">
                            <button type="button" class="btn btn-success" data-toggle="modal" data-target="#plan-create-Modal"><i class='fas fa-plus'></i>Create Coverage Area</button>
                            <!--plan-create Modal -->
                            <div class="modal fade" id="plan-create-Modal" role="dialog">
                                <div class="modal-dialog modal-md">
                                    <!-- Modal content-->
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h4 class="modal-title">Coverage Area Details</h4>
                                            <button type="button" class="close btn btn-warning" data-dismiss="modal">&times;</button>
                                        </div>
                                        <form action="{{route('admin.business.store-coverage-area')}}" method="post">
                                            @csrf
                                            <div class="modal-body">
                                                <div class="container">
                                                    <div class="row">
                                                        <div class="col-xl-12">
                                                            <!-- Account details card-->
                                                            <div class="card mb-4">
                                                                <div class="card-body text-left">
                                                                    <div class="form-row">
                                                                        <!-- Form Group (first name)-->
                                                                        <div class="col-sm-6">
                                                                            <label>Area Name</label>
                                                                        </div>
                                                                        <!-- Form Group (last name)-->
                                                                        <div class="form-group col-sm-6">
                                                                            <select class="form-control pickup_area_select" id="area" name="area" >
                                                                                <option value="" selected disabled>Select area</option>
                                                                                @foreach (App\City::orderBy('city_name','asc')->get() as $city)
                                                                                    <optgroup label="{{$city->city_name}}">
                                                                                        @foreach(App\Area::where('city_id',$city->id)->orderBy('area_name','asc')->get() as $area)
                                                                                            <option value="{{$area->id}}">{{$area->area_name}}</option>
                                                                                        @endforeach
                                                                                    </optgroup>
                                                                                @endforeach
                                                                            </select>
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-row">
                                                                        <!-- Form Group (first name)-->
                                                                        <div class="col-sm-6">
                                                                            <label>Sector Name</label>
                                                                        </div>
                                                                        <!-- Form Group (last name)-->
                                                                        <div class="form-group col-sm-6">
                                                                            <select class="form-control pickup_area_select" id="division" name="division" >
                                                                                <option value="Inside Dhaka" selected>Inside Dhaka</option>
                                                                                <option value="Outside Dhaka">Outside Dhaka</option>
                                                                                <option value="Dhaka Sub">Dhaka Sub</option>
                                                                            </select>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>

                                                    </div>

                                                </div>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="submit" class="btn btn-admin-primary" >Create</button>
                                                <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-striped" id="dataTable" width="100%" cellspacing="0">
                            <thead>
                            <tr>
                                <th>Sector</th>
                                <th>City</th>
                                <th>Area</th>
                                <th>Status</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach(\App\CoverageArea::orderBy('id','desc')->get() as $coverageArea)
                                <tr>
                                    <td>{{$coverageArea->division}}</td>
                                    <td>{{$coverageArea->city}}</td>
                                    <td>{{$coverageArea->area}}</td>
                                    <td><label class="switch status"><input type="checkbox" class="status" id="{{$coverageArea->id}}" <?php if($coverageArea->status==1)echo "checked" ?>><span class="slider round"></span></label></td>

                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>


@stop
@section('script')
    @toastr_render
    <script>
        $("#area").select2({
            placeholder: "Select area",
            allowClear: false
        });
        $("#dataTable").dataTable();

        $(document).on('change','.status',function () {
            var id = $(this).attr("id");
            if($(this).prop("checked") == true){
                var status = 1;
            }
            else if($(this).prop("checked") == false){
                var status = 0;
            }

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                }
            });
            $.ajax({
                url:"{{route('admin.business.status')}}",
                method: 'POST',
                data: {
                    id:id,
                    status:status
                },
                success:function () {
                    toastr.success('Coverage area status updated successfully');
                }
            });
        });
    </script>
@stop