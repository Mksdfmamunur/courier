@extends('admins.layouts.app')
@section('title')
    Merchant Request
@stop
@section('merchant','active')
@section('collapsed-merchant','')
@section('merchant-c','show')
@section('MR','active')
@section('css')

@stop

@section('content')
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <div class="row p-0">
                        <div class="col text-left">
                            <h3>Requested Merchant</h3>
                        </div>
{{--                        <div class="col text-right">--}}
{{--                            <a href="{{route('admin.rider.register')}}" class="btn btn-admin-primary">Add Rider</a>--}}
{{--                        </div>--}}
                    </div>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-striped" id="dataTable" width="100%" >
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Name</th>
                                <th>Email</th>
                                <th>Phone Number</th>
                                <th>Status</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach(\App\Merchant::where('status',"Pending")->get() as $key=> $merchant)
                                <tr>
                                    <td>{{$key+1}}</td>
                                    <td>{{$merchant->first_name}} {{$merchant->last_name}}</td>
                                    <td>{{$merchant->email}}</td>
                                    <td>{{$merchant->mobile_number}}</td>
                                    <td>{{$merchant->status}}</td>
                                    <td>
                                        <a href="" class="btn btn-admin-primary" data-toggle="modal" data-target="#riderModal{{$merchant->id}}"><i class='fas fa-eye'></i></a>
                                    </td>
                                </tr>
                                <!-- Modal -->
                                <div class="modal fade" id="riderModal{{$merchant->id}}" role="dialog">
                                    <div class="modal-dialog modal-xl">
                                        <!-- Modal content-->
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h4 class="modal-title">Merchant Info</h4>
                                                <button type="button" class="close btn btn-admin-danger" data-dismiss="modal">&times;</button>
                                            </div>
                                            <div class="modal-body">
                                                <div class="container">
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <!-- Account details card-->
                                                            <div class="card mb-4">
                                                                <div class="card-header">Personal Information</div>
                                                                <div class="card-body">
                                                                    <div class="row">
                                                                        <div class="form-group col-md-6">
                                                                            <span class="heading"><u>First Name :</u></span>
                                                                            <h6><b>{{$merchant->first_name}}</b></h6>
                                                                        </div>
                                                                        <div class="form-group col-md-6">
                                                                            <span class="heading"><u>Last Name :</u></span>
                                                                            <h6><b>{{$merchant->last_name}}</b></h6>
                                                                        </div>
                                                                    </div>
                                                                    <div class="row">
                                                                        <div class="form-group col-md-6">
                                                                            <span class="heading"><u>Email :</u></span>
                                                                            <h6><b>{{$merchant->email}}</b></h6>
                                                                        </div>
                                                                        <div class="form-group col-md-6">
                                                                            <span class="heading"><u>Mobile Number :</u></span>
                                                                            <h6><b>{{$merchant->mobile_number}}</b></h6>
                                                                        </div>
                                                                    </div>
                                                                    <div class="row pl-0 pr-0">
                                                                        <div class="col border" style="background-color: #f8f9fc">
                                                                            <h6>Shop Information</h6>
                                                                        </div>
                                                                    </div>
                                                                    @foreach ($merchant->shops as $shop)
                                                                        <div class="row">
                                                                            <div class="form-group col-md-6">
                                                                                <span class="heading"><u>Shop Name :</u></span>
                                                                                <h6><b>{{$shop->shop_name}}</b></h6>
                                                                            </div>
                                                                            <div class="form-group col-md-6">
                                                                                <span class="heading"><u>Shop Link :</u></span>
                                                                                <h6><b>{{$shop->shop_link}}</b></h6>
                                                                            </div>
                                                                        </div>
                                                                        <div class="row">
                                                                            <div class="form-group col-md-6">
                                                                                <span class="heading"><u>Pickup Number :</u></span>
                                                                                <h6><b>{{$shop->pickup_number}}</b></h6>
                                                                            </div>
                                                                            <div class="form-group col-md-6">
                                                                                <span class="heading"><u>Pickup Address :</u></span>
                                                                                <h6><b>{{$shop->pickup_address}}</b></h6>
                                                                            </div>
                                                                        </div>
                                                                    @endforeach

                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <!-- Profile picture card-->
                                                            <div class="card">
                                                                <div class="card-header">Verification details </div>
                                                                <div class="card-body">
                                                                    <div class="row">
                                                                        <div class="form-group col-md-6">
                                                                            <span class="heading"><u>Verification Type</u></span>
                                                                            <h6><b>{{$merchant->verification_id_type}}</b></h6>
                                                                        </div>
                                                                        <div class="form-group col-md-6">
                                                                            <span class="heading"><u>{{$merchant->verification_id_type}} Number</u></span>
                                                                            <h6><b>{{$merchant->verification_id_number}}</b></h6>
                                                                        </div>
                                                                    </div>
                                                                    <div class="row">
                                                                        <div class="col-md-12">
                                                                            <span><u>{{$merchant->verification_id_type}} Pic :</u></span><br>
                                                                            <img class="img-account-profile mb-2" src="{{$merchant->verification_id_pic}}" alt=""  height="220" style="width: 280px;">
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                            <div class="modal-footer mx-auto">
                                                <form action="{{route('admin.merchant.status')}}" method="post">
                                                    @csrf
                                                    <input type="hidden" value="{{$merchant->id}}" name="id">
                                                    <input type="hidden" value="Approved" name="status">
                                                    <button type="submit"  class="btn btn-admin-primary">Approved</button>
                                                </form>
                                                <form action="{{route('admin.merchant.status')}}" method="post">
                                                    @csrf
                                                    <input type="hidden" value="{{$merchant->id}}" name="id">
                                                    <input type="hidden" value="Rejected" name="status">
                                                    <button type="submit"  class="btn btn-admin-danger">Rejected</button>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- End Modal -->

                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>


@stop
@section('script')
    @toastr_render
    <script>
        $("#dataTable").dataTable();
    </script>
@stop