@extends('admins.layouts.app')
@section('title')
    Merchant Plan Assign
@stop
@section('merchant','active')
@section('collapsed-merchant','')
@section('merchant-c','show')
@section('MP','active')
@section('css')

@stop

@section('content')
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <div class="row p-0">
                        <div class="col text-left">
                            <h3>Merchant Plan List</h3>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-striped" id="dataTable" width="100%" cellspacing="0">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Merchant Name</th>
                                <th>Merchant Phone</th>
                                <th>Merchant Email</th>
                                <th>Shop Name</th>
                                <th>Plan Name</th>
                                <th>Edit</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach(\App\MerchantPlan::orderBy('id','desc')->get() as $key=>$merchantPlan)
                                <tr>
                                    <td>{{$key+1}}</td>
                                    <td>{{$merchantPlan->merchant->first_name}} {{$merchantPlan->merchant->last_name}}</td>
                                    <td>{{$merchantPlan->merchant->mobile_number}}</td>
                                    <td>{{$merchantPlan->merchant->email}}</td>
                                    <td>{{$merchantPlan->merchantShop->shop_name}}</td>
                                    <td>{{$merchantPlan->plan->plan_name}}</td>
                                    <td>
                                        <div class="btn-group">
                                            <button type="button" class="btn btn-success" data-toggle="modal" data-target="#plan-edit-Modal{{$merchantPlan->id}}"><i class='fas fa-wrench'></i></button>
                                        </div>
                                        <!--plan-edit Modal -->
                                        <div class="modal fade" id="plan-edit-Modal{{$merchantPlan->id}}" role="dialog">
                                            <div class="modal-dialog modal-md">
                                                <!-- Modal content-->
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h4 class="modal-title">Plan Details</h4>
                                                        <button type="button" class="close btn btn-warning" data-dismiss="modal">&times;</button>
                                                    </div>
                                                    <form action="{{route('admin.merchant.update-plan')}}" method="post">
                                                        @csrf
                                                        <input type="hidden" name="id" value="{{$merchantPlan->id}}">
                                                        <div class="modal-body">
                                                            <div class="container">
                                                                <div class="row">
                                                                    <div class="col-xl-12">
                                                                        <!-- Account details card-->
                                                                        <div class="card mb-4">
                                                                            <div class="card-body text-left">
                                                                                <div class="form-row">
                                                                                    <div class="col-md-6">
                                                                                        <label>Merchant Name</label>
                                                                                    </div>
                                                                                    <div class="form-group col-md-6">
                                                                                        <input class="form-control"  placeholder="rate" value="{{$merchantPlan->merchant->first_name}} {{$merchantPlan->merchant->last_name}}" readonly>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="form-row">
                                                                                    <div class="col-md-6">
                                                                                        <label>Merchant Email</label>
                                                                                    </div>
                                                                                    <div class="form-group col-md-6">
                                                                                        <input class="form-control"  type="text" placeholder="rate" value="{{$merchantPlan->merchant->email}}" readonly>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="form-row">
                                                                                    <div class="col-md-6">
                                                                                        <label>Shop Name</label>
                                                                                    </div>
                                                                                    <div class="form-group col-md-6">
                                                                                        <input class="form-control"  type="text" placeholder="rate" value="{{$merchantPlan->merchantShop->shop_name}}" readonly>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="form-row">
                                                                                    <div class=" col-md-6">
                                                                                        <label>Plan Name</label>
                                                                                    </div>
                                                                                    <div class="form-group col-md-6">
                                                                                        <select class="form-control plan_select" id="plan_id" name="plan_id" >
                                                                                            @foreach(App\Plan::orderBy('plan_name','asc')->get() as $plan)
                                                                                                <option value="{{$plan->id}}" <?php if ($plan->id==$merchantPlan->plan_id) echo "selected"?>>{{$plan->plan_name}}</option>
                                                                                            @endforeach
                                                                                        </select>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>

                                                                </div>

                                                            </div>
                                                        </div>
                                                        <div class="modal-footer">
                                                            <button type="submit" class="btn btn-admin-primary" >Update</button>
                                                            <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>


@stop
@section('script')
    @toastr_render
    <script>
        $("#dataTable").dataTable();
    </script>
@stop