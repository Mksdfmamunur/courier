@extends('admins.layouts.app')
@section('title')
    Pickup List
@stop
@section('pickup','active')
@section('collapsed-pickup','')
@section('pickup-c','show')
@section('PL','active')
@section('css')

@stop

@section('content')

    <div class="row">
        <div class="col">
            <div class="card">
                <div class="card-header">
                    <h3>Pickup List</h3>
                </div>
                <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-striped" id="dataTable" width="100%" >
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Shop Name</th>
                                    <th>Shop Number</th>
                                    <th>Shop Area</th>
                                    <th>Shop Address</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach(\App\Order::select('shop_id')->where('delivery_status',2)->whereDate('updated_at',\Carbon\Carbon::today())->groupBy('shop_id')->get() as $key=> $shop_id)
{{--                                    {{$shop_id}}--}}
                                    <?php $shop=\App\MerchantShop::where('id',$shop_id->shop_id)->first() ?>
                                        <tr>
                                            <td> {{$key+1}}</td>
                                            <td>{{$shop->shop_name}}</td>
                                            <td>{{$shop->pickup_number}}</td>
                                            <td>{{$shop->pickUpArea->area}}</td>
                                            <td>{{$shop->pickup_address}}</td>
                                        </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                </div>
            </div>
        </div>
    </div>
@stop
@section('script')
    @toastr_render
    <script>
        $("#dataTable").dataTable();
    </script>
@stop