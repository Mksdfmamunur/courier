@extends('admins.layouts.app')
@section('title')
    Pickup Approve
@stop
@section('pickup','active')
@section('collapsed-pickup','')
@section('pickup-c','show')
@section('PA','active')
@section('css')

@stop

@section('content')

    <div class="row">
        <div class="col">
            <div class="card">
                <div class="card-header">
                    <h1>Pickup Approve</h1>
                </div>
                <div class="card-body">
                    <form action="{{route('admin.pickup.status')}}" method="post">
                        @csrf
                        <div class="table-responsive">
                            <table class="table table-striped" id="dataTable" width="100%" >
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Track ID</th>
                                    <th>Shop Name</th>
                                    <th>Customer Name</th>
                                    <th>Customer Phone</th>
                                </tr>
                                </thead>
                                <tbody>
                                    @foreach(\App\Order::where('delivery_status',1)->get() as $key=> $order)
                                        <tr>
                                            <td><input type="hidden" name="id[]" value="{{$order->id}}"> {{$key+1}}</td>
                                            <td>{{$order->tracking_id}}</td>
                                            <td>{{$order->merchantShop->shop_name}}</td>
                                            <td>{{$order->recipient_name}}</td>
                                            <td>{{$order->recipient_phone}}</td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                        <div class="row mt-2">
                            <input type="hidden" name="delivery_status" value="2">
                            <div class="offset-md-8 col-md-4">
                                <button type="submit" class="btn btn-admin-primary float-right mk">Pickup Approved</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@stop
@section('script')
    @toastr_render
    <script>
        $("#dataTable").dataTable();
    </script>
@stop