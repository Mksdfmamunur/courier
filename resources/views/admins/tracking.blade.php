@extends('admins.layouts.app')
@section('title')
    Admin Track Order
@stop
{{--@section('dashboard','active')--}}
@section('css')

@stop

@section('content')
    <div class="container-fluid mt-4">
        <div class="row">
            @if ($trackings==null)
                <div class="col-12">
                    <div class="card">
                        <div class="card-body text-center">
                            <h1 class=" text-danger">Invalid tracking id</h1>
                        </div>
                    </div>
                </div>
            @else
                <div class="col-md-8 form-row delivery_charge">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-12">
                                <h2 class="jm_bangla">ট্র্যাক অর্ডার</h2>
                            </div>
                            <div class="col-sm-4">
                                <div class="text-left pl-2 tracking-status-intransit">
                                    <h6>Tracking ID</h6>
                                    <h5>{{$order->tracking_id}}</h5>
                                </div>
                            </div>
                            <div class="col-sm-8">
                                <div class="tracking-list pt-3">
                                    @foreach($trackings as $tracking)
                                        @if ($tracking->status_id==13)

                                        @endif
                                        @if ($tracking->status_id==12)
                                            <div class="tracking-item">
                                                <div class="tracking-icon">
                                                    <img src="{{asset('public/icon/check4.svg')}}" class="trackItemSvg" alt="On processing"/>
                                                </div>
                                                <div class="tracking-date pt-2">{{Carbon\Carbon::parse($tracking->created_at)->format('M d Y')}}<span>{{Carbon\Carbon::parse($tracking->created_at)->format('g:i A')}}</span></div>
                                                <div class="tracking-content pt-2">{{$tracking->status->description}}</div>
                                            </div>
                                        @endif
                                        @if ($tracking->status_id==11)
                                            <div class="tracking-item">
                                                <div class="tracking-icon">
                                                    <img src="{{asset('public/icon/check3.svg')}}" class="trackItemSvg" alt="On processing"/>
                                                </div>
                                                <div class="tracking-date pt-2">{{Carbon\Carbon::parse($tracking->created_at)->format('M d Y')}}<span>{{Carbon\Carbon::parse($tracking->created_at)->format('g:i A')}}</span></div>
                                                <div class="tracking-content pt-2">{{$tracking->status->description}}</div>
                                            </div>
                                        @endif
                                        @if ($tracking->status_id==10)
                                            <div class="tracking-item">
                                                <div class="tracking-icon">
                                                    <img src="{{asset('public/icon/check5.svg')}}" class="trackItemSvg" alt="On processing"/>
                                                </div>
                                                <div class="tracking-date pt-2">{{Carbon\Carbon::parse($tracking->created_at)->format('M d Y')}}<span>{{Carbon\Carbon::parse($tracking->created_at)->format('g:i A')}}</span></div>
                                                <div class="tracking-content pt-2">{{$tracking->status->description}}</div>
                                            </div>
                                        @endif
                                        @if ($tracking->status_id==9)
                                            <div class="tracking-item">
                                                <div class="tracking-icon">
                                                    <img src="{{asset('public/icon/copyright.svg')}}" class="trackItemSvg" alt="On processing"/>
                                                </div>
                                                <div class="tracking-date pt-2">{{Carbon\Carbon::parse($tracking->created_at)->format('M d Y')}}<span>{{Carbon\Carbon::parse($tracking->created_at)->format('g:i A')}}</span></div>
                                                <div class="tracking-content pt-2">{{$tracking->status->description}}</div>
                                            </div>
                                        @endif
                                        @if ($tracking->status_id==8)
                                            <div class="tracking-item">
                                                <div class="tracking-icon">
                                                    <img src="{{asset('public/icon/half-time.svg')}}" class="trackItemSvg" alt="On processing"/>
                                                </div>
                                                <div class="tracking-date pt-2">{{Carbon\Carbon::parse($tracking->created_at)->format('M d Y')}}<span>{{Carbon\Carbon::parse($tracking->created_at)->format('g:i A')}}</span></div>
                                                <div class="tracking-content pt-2">{{$tracking->status->description}} <span>Ali Ahomed Akash (8801855527636)</span></div>
                                            </div>
                                        @endif
                                        @if ($tracking->status_id==7)
                                            <div class="tracking-item">
                                                <div class="tracking-icon">
                                                    <img src="{{asset("public/icon/trash-can.svg")}}" class="trackItemSvg" alt="On processing"/>
                                                </div>
                                                <div class="tracking-date pt-2">{{Carbon\Carbon::parse($tracking->created_at)->format('M d Y')}}<span>{{Carbon\Carbon::parse($tracking->created_at)->format('g:i A')}}</span></div>
                                                <div class="tracking-content pt-2">{{$tracking->status->description}} </div>
                                            </div>
                                        @endif
                                        @if ($tracking->status_id==6)
                                            <div class="tracking-item">
                                                <div class="tracking-icon">
                                                    <img src="{{asset('public/icon/check3.svg')}}" class="trackItemSvg" alt="On processing"/>
                                                </div>
                                                <div class="tracking-date pt-2">{{Carbon\Carbon::parse($tracking->created_at)->format('M d Y')}}<span>{{Carbon\Carbon::parse($tracking->created_at)->format('g:i A')}}</span></div>
                                                <div class="tracking-content pt-2">{{$tracking->status->description}} <span>Ali Ahomed Akash (8801855527636)</span></div>
                                            </div>
                                        @endif
                                        @if ($tracking->status_id==5)
                                            <div class="tracking-item">
                                                <div class="tracking-icon">
                                                    <img src="{{asset('public/icon/delivery-man2.svg')}}" class="trackItemSvg" alt="On processing"/>
                                                </div>
                                                <div class="tracking-date pt-2">{{Carbon\Carbon::parse($tracking->created_at)->format('M d Y')}}<span>{{Carbon\Carbon::parse($tracking->created_at)->format('g:i A')}}</span></div>
                                                <div class="tracking-content pt-2">{{$tracking->status->description}} <span>{{$tracking->rider->name}} ({{$tracking->rider->phone}})</span></div>
                                            </div>
                                        @endif
                                        @if ($tracking->status_id==4)
                                            <div class="tracking-item">
                                                <div class="tracking-icon">
                                                    <img src="{{asset('public/icon/check5.svg')}}" class="trackItemSvg" alt="On processing"/>
                                                </div>
                                                <div class="tracking-date pt-2">{{Carbon\Carbon::parse($tracking->created_at)->format('M d Y')}}<span>{{Carbon\Carbon::parse($tracking->created_at)->format('g:i A')}}</span></div>
                                                <div class="tracking-content pt-2">{{$tracking->status->description}}</div>
                                            </div>
                                        @endif

                                        @if ($tracking->status_id==3)
                                            <div class="tracking-item">
                                                <div class="tracking-icon">
                                                    <img src="{{asset('public/icon/check2.svg')}}" class="trackItemSvg" alt="On processing"/>
                                                </div>
                                                <div class="tracking-date pt-2">{{Carbon\Carbon::parse($tracking->created_at)->format('M d Y')}}<span>{{Carbon\Carbon::parse($tracking->created_at)->format('g:i A')}}</span></div>
                                                <div class="tracking-content pt-2">{{$tracking->status->description}}</div>
                                            </div>
                                        @endif
                                        @if ($tracking->status_id==2)
                                            <div class="tracking-item">
                                                <div class="tracking-icon">
                                                    <img src="{{asset('public/icon/check2.svg')}}" class="trackItemSvg" alt="On processing"/>
                                                </div>
                                                <div class="tracking-date pt-2">{{Carbon\Carbon::parse($tracking->created_at)->format('M d Y')}}<span>{{Carbon\Carbon::parse($tracking->created_at)->format('g:i A')}}</span></div>
                                                <div class="tracking-content pt-2">{{$tracking->status->description}}</div>
                                            </div>
                                        @endif
                                        @if ($tracking->status_id==1)
                                            <div class="tracking-item">
                                                <div class="tracking-icon">
                                                    <img src="{{asset('public/icon/check1.svg')}}" class="trackItemSvg" alt="On processing"/>
                                                </div>
                                                <div class="tracking-date pt-2">{{Carbon\Carbon::parse($tracking->created_at)->format('M d Y')}}<span>{{Carbon\Carbon::parse($tracking->created_at)->format('g:i A')}}</span></div>
                                                <div class="tracking-content pt-2">{{$tracking->status->description}}</div>
                                            </div>
                                        @endif

                                    @endforeach
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 col-sm-12">
                    <h2 class="jm_bangla">কাস্টমার ও অর্ডারের বিস্তারিত তথ্য</h2>
                    <div class="container">
                        <h6><i class="fas fa-user" aria-hidden="true"></i> {{$order->recipient_name}}</h6>
                        <h6><i class="fas fa-phone-square" aria-hidden="true"></i> {{$order->recipient_phone}} </h6>
                        <h6><i class="fas fa-location-arrow" aria-hidden="true"></i> {{$order->recipient_address}}</h6>
                    </div>
                    <div class="container">
                        <br>
                        <h6>Area</h6>
                        <span>{{$order->coverageArea->area}}</span><br><br>
                        <h6>Weight</h6>
                        <span>{{$order->order_weight}} Kg</span><br><br>
                        <h6>Collect To Amount</h6>
                        <span>Tk. {{$order->amount_to_collect}}</span><br><br>
                        <h6>Delivery Charge</h6>
                        <span>Tk. {{$order->delivery_charge}}</span><br><br>
                        <h6>COD Charge</h6>
                        <span>Tk. {{$order->cod_charge}}</span><br><br>
                    </div>
                </div>
            @endif
        </div>
    </div>
@stop
@section('script')
    @toastr_render
@stop