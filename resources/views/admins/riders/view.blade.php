@extends('admins.layouts.app')
@section('title')
    View Riders
@stop
@section('rider','active')
@section('collapsed-rider','')
@section('rider-c','show')
@section('RL','active')
@section('css')

@stop

@section('content')
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <div class="row p-0">
                        <div class="col text-left">
                            <h3>View Riders</h3>
                        </div>
                        <div class="col text-right">
                            <a href="{{route('admin.rider.register')}}" class="btn btn-admin-primary">Add Rider</a>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-striped" id="dataTable" width="100%" >
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Nick Name</th>
                                <th>Email</th>
                                <th>Phone Number</th>
                                <th>Present Address</th>
                                <th>Status</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach(\App\Rider::orderBy('id','desc')->get() as $key=> $rider)
                                <tr>
                                    <td>{{$key+1}}</td>
                                    <td>{{$rider->nick_name}}</td>
                                    <td>{{$rider->email}}</td>
                                    <td>{{$rider->phone}}</td>
                                    <td>{{$rider->present_address}}</td>
                                    <td>{{$rider->status}}</td>
                                    <td>
                                        <a href="" class="btn btn-admin-primary" data-toggle="modal" data-target="#riderModal{{$rider->id}}"><i class='fas fa-eye'></i></a>
                                        <a href="{{route('admin.rider.edit', encrypt($rider->id))}}" class="btn btn-admin-secondary"><i class='fas fa-wrench'></i></a>
                                    </td>
                                </tr>
                                <!-- Modal -->
                                <div class="modal fade" id="riderModal{{$rider->id}}" role="dialog">
                                    <div class="modal-dialog modal-xl">
                                        <!-- Modal content-->
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h4 class="modal-title">Rider Info</h4>
                                                <button type="button" class="close btn btn-admin-danger" data-dismiss="modal">&times;</button>
                                            </div>
                                            <div class="modal-body">
                                                <div class="container">
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <!-- Account details card-->
                                                            <div class="card mb-4">
                                                                <div class="card-header">Personal Information</div>
                                                                <div class="card-body">
                                                                    <div class="row">
                                                                        <div class="form-group col-md-6">
                                                                            <span class="heading"><u>Full Name :</u></span>
                                                                            <h6><b>{{$rider->name}}</b></h6>
                                                                        </div>
                                                                        <div class="form-group col-md-6">
                                                                            <span class="heading"><u>Nick Name :</u></span>
                                                                            <h6><b>{{$rider->nick_name}}</b></h6>
                                                                        </div>
                                                                    </div>
                                                                    <div class="row">
                                                                        <div class="form-group col-md-6">
                                                                            <span class="heading"><u>Email :</u></span>
                                                                            <h6><b>{{$rider->email}}</b></h6>
                                                                        </div>
                                                                        <div class="form-group col-md-6">
                                                                            <span class="heading"><u>Mobile Number :</u></span>
                                                                            <h6><b>{{$rider->phone}}</b></h6>
                                                                        </div>
                                                                    </div>
                                                                    <div class="row">
                                                                        <div class="form-group col-md-6">
                                                                            <span class="heading"><u>Father Name :</u></span>
                                                                            <h6><b>{{$rider->father_name}}</b></h6>
                                                                        </div>
                                                                        <div class="form-group col-md-6">
                                                                            <span class="heading"><u>Mother Name :</u></span>
                                                                            <h6><b>{{$rider->mother_name}}</b></h6>
                                                                        </div>
                                                                    </div>
                                                                    <div class="row">
                                                                        <div class="form-group col-md-6">
                                                                            <span class="heading"><u>Present Address :</u></span>
                                                                            <h6><b>{{$rider->present_address}}</b></h6>
                                                                        </div>
                                                                        <div class="form-group col-md-6">
                                                                            <span class="heading"><u>Permanent Address :</u></span>
                                                                            <h6><b>{{$rider->permanent_address}}</b></h6>
                                                                        </div>
                                                                    </div>
                                                                    <div class="row">
                                                                        <div class="form-group col-md-6">
                                                                            <span class="heading"><u>Referrer Name :</u></span>
                                                                            <h6><b>{{$rider->referrer_name}}</b></h6>
                                                                        </div>
                                                                        <div class="form-group col-md-6">
                                                                            <span class="heading"><u>Referrer Mobile Number:</u></span>
                                                                            <h6><b>{{$rider->referrer_phone}}</b></h6>
                                                                        </div>
                                                                    </div>
                                                                    <div class="row">
                                                                        <div class="form-group col-md-6">
                                                                            <span class="heading"><u>Referrer Address :</u></span>
                                                                            <h6><b>{{$rider->referrer_address}}</b></h6>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <!-- Profile picture card-->
                                                            <div class="card">
                                                                <div class="card-header">Verification details </div>
                                                                <div class="card-body">
                                                                    <div class="row">
                                                                        <div class="form-group col-md-6">
                                                                            <span class="heading"><u>Verification Type</u></span>
                                                                            <h6><b>{{$rider->verification_id_type}}</b></h6>
                                                                        </div>
                                                                        <div class="form-group col-md-6">
                                                                            <span class="heading"><u>{{$rider->verification_id_type}} Number</u></span>
                                                                            <h6><b>{{$rider->verification_id_number}}</b></h6>
                                                                        </div>
                                                                    </div>
                                                                    <div class="row">
                                                                        <div class="col-md-6">
                                                                            <span><u>Profile Pic :</u></span><br>
                                                                            <img class="img-account-profile rounded-circle mb-2" src="{{$rider->profile_pic}}" alt="" width="100" height="100">
                                                                        </div>
                                                                        <div class="col-md-6">
                                                                            <span><u>{{$rider->verification_id_type}} Pic :</u></span><br>
                                                                            <img class="img-account-profile mb-2" src="{{$rider->verification_id_pic}}" alt="" width="280" height="220">
                                                                        </div>
                                                                     </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                            <div class="modal-footer">
                                                <a href="{{route('admin.rider.edit', encrypt($rider->id))}}" class="btn btn-admin-secondary"><i class='fas fa-wrench'></i> Edit</a>
                                                <button type="button" class="btn btn-admin-danger" data-dismiss="modal">Close</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- End Modal -->

                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>


@stop
@section('script')
    @toastr_render
    <script>
        $("#dataTable").dataTable();
    </script>
@stop