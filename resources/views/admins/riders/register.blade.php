@extends('admins.layouts.app')
@section('title')
    Rider Register
@stop
@section('rider','active')
@section('collapsed-rider','')
@section('rider-c','show')
@section('RR','active')
@section('css')

@stop

@section('content')
    <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <h3>Rider Registration</h3>
                    </div>
                    <div class="card-body">
                        <form action="{{route('admin.rider.store')}}" method="post" enctype="multipart/form-data">
                        @csrf
                            <div class="row">
                                <div class="col mt-2">
                                    <h6>Personal Information</h6>
                                    <hr class="info_border">
                                </div>
                            </div>
                            <!-- Form Row-->
                            <div class="form-row">
                                <!-- Form Group (first name)-->
                                <div class="form-group col-md-6">
                                    <label class="small mb-1" for="name">Full Name <span class="text-danger">*</span></label>
                                    <input class="form-control" id="name" name="name" type="text" placeholder="Enter name as ID" >
                                    <div class="name invalid-feedback">Please enter name</div>
                                </div>
                                <!-- Form Group (last name)-->
                                <div class="form-group col-md-6">
                                    <label class="small mb-1" for="nick_name">Nick Name <span class="text-danger">*</span></label>
                                    <input class="form-control" id="nick_name" name="nick_name" type="text" placeholder="will be shown to all">
                                    <div class="nick_name invalid-feedback">Please enter nick name</div>
                                </div>
                            </div>
                            <!-- Form Row        -->
                            <div class="form-row">
                                <!-- Form Group (organization name)-->
                                <div class="form-group col-md-6">
                                    <label for="email" class="small mb-1">Email <span class="text-danger">*</span></label>
                                    <input type="email" class="form-control" id="email" name="email" placeholder="Enter email address">
                                    <div class="invalid-feedback email">Please enter valid email address</div>
                                </div>
                                <!-- Form Group (location)-->
                                <div class="form-group col-md-6">
                                    <label for="phone" class="small mb-1">Mobile Number <span class="text-danger">*</span></label>
                                    <input type="text" class="form-control number" id="phone" name="phone" placeholder="Enter mobile number">
                                    <div class="">Only english character is allowed</div>
                                    <div class="phone text-danger" style="display: none">Please enter valid phone number</div>
                                </div>
                            </div>
                            <!-- Form Row-->
                            <div class="form-row">
                                <!-- Form Group (phone number)-->
                                <div class="form-group col-md-6">
                                    <label for="father_name" class="small mb-1">Father Name <span class="text-danger">*</span></label>
                                    <input type="text" class="form-control" id="father_name" name="father_name" placeholder="Enter father name as ID">
                                    <div class="father_name invalid-feedback">Please enter father name</div>
                                </div>
                                <!-- Form Group (birthday)-->
                                <div class="form-group col-md-6">
                                    <label for="mother_name" class="small mb-1">Mother Name <span class="text-danger">*</span></label>
                                    <input type="text" class="form-control" id="mother_name" name="mother_name" placeholder="Enter mother name as ID">
                                    <div class="mother_namee invalid-feedback">Please enter mother name</div>
                                </div>
                            </div>
                            <!-- Form Row-->
                            <div class="form-row">
                                <!-- Form Group (phone number)-->
                                <div class="form-group col-md-6">
                                    <label for="present_address" class="small mb-1">Present Address <span class="text-danger">*</span></label>
                                    <textarea class="form-control" name="present_address" id="present_address" rows="2"></textarea>
                                    <div class="present_address invalid-feedback">Please enter present address</div>
                                </div>
                                <!-- Form Group (birthday)-->
                                <div class="form-group col-md-6">
                                    <label for="permanent_address" class="small mb-1">Permanent Address <span class="text-danger">*</span></label>
                                    <textarea class="form-control" name="permanent_address" id="permanent_address" rows="2"></textarea>
                                    <div class="permanent_address invalid-feedback">Please enter permanent address</div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col mt-2">
                                    <h6>Verification Information</h6>
                                    <hr class="info_border">
                                </div>
                            </div>
                            <div class="row mt-2">
                                <div class="col">
                                    <div class="form-group">
                                        <label>Verification Info (Id Type) <span class="text-danger">*</span></label>
                                        <div class="">
                                            <div class="form-check-inline">
                                                <label class="form-check-label">
                                                    <input type="radio" class="form-check-input" name="verification_id_type" value="NID" checked>NID
                                                </label>
                                            </div>
                                            <div class="form-check-inline">
                                                <label class="form-check-label">
                                                    <input type="radio" class="form-check-input" name="verification_id_type" value="Birth Certificate">Birth Certificate
                                                </label>
                                            </div>
                                            <div class="form-check-inline">
                                                <label class="form-check-label">
                                                    <input type="radio" class="form-check-input" name="verification_id_type" value="Passport">Passport
                                                </label>
                                            </div>
                                            <div class="form-check-inline">
                                                <label class="form-check-label">
                                                    <input type="radio" class="form-check-input" name="verification_id_type" value="Driving License">Driving License
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="nid" for="verification_id_number">NID Number <span class="text-danger">*</span></label>
                                        <label class="birth_certificate" for="verification_id_number" style="display: none">Birth Certificate Number <span class="text-danger">*</span></label>
                                        <label class="passport" for="verification_id_number" style="display: none">Passport Number <span class="text-danger">*</span></label>
                                        <label class="driving_license" for="verification_id_number" style="display: none">Driving License Number <span class="text-danger">*</span></label>
                                        <input type="text" class="form-control" id="verification_id_number" name="verification_id_number"  placeholder="Enter id number" >
                                        <div class="verification_id_number invalid-feedback">Please enter verification id number</div>
                                        <div>Only english character is allowed.</div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="verification_id_pic">ID's Font Image <span class="text-danger">*</span></label>
                                        <input type="file" class="form-control" id="verification_id_pic" name="verification_id_pic">
                                        <div class="verification_id_pic invalid-feedback">Please upload ID's font image <b>(jpeg/jpg/png)</b> </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="profile_pic">Profile picture<span class="text-danger">*</span></label>
                                        <input type="file" class="form-control" id="profile_pic" name="profile_pic">
                                        <div class="profile_pic invalid-feedback">Please upload Profile picture <b>(jpeg/jpg/png)</b> </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col mt-2">
                                    <h6>Guarantor Information</h6>
                                    <hr class="info_border">
                                </div>
                            </div>
                            <div class="form-row">
                                <!-- Form Group (phone number)-->
                                <div class="form-group col-md-6">
                                    <label for="referrer_name" class="small mb-1">Referrer Full Name <span class="text-danger">*</span></label>
                                    <input type="text" class="form-control" id="referrer_name" name="referrer_name" placeholder="Enter referrer name">
                                    <div class="referrer_name invalid-feedback">Enter referrer name</div>
                                </div>
                                <!-- Form Group (birthday)-->
                                <div class="form-group col-md-6">
                                    <label for="referrer_phone" class="small mb-1">Referrer Mobile Number <span class="text-danger">*</span></label>
                                    <input type="text" class="form-control number" id="referrer_phone" name="referrer_phone" placeholder="Referrer mobile number">
                                    <div class="">Only english character is allowed</div>
                                    <div class="referrer_phone invalid-feedback">Please enter valid phone number</div>
                                </div>
                            </div>
                            <div class="form-row">
                                <!-- Form Group (phone number)-->
                                <div class="form-group col-md-12">
                                    <label for="referrer_address" class="small mb-1">Referrer Address <span class="text-danger">*</span></label>
                                    <textarea class="form-control" name="referrer_address" id="referrer_address" rows="2"></textarea>
                                    <div class="referrer_address invalid-feedback">Please enter referrer address</div>
                                </div>
                            </div>

                            <div class="row mt-2">
                                <div class="col">
                                    <div class="text-center">
                                        <button class="btn btn-admin-primary" id="btn_fetch">REGISTER</button>
                                        <button id="btn_fetch_disable" class="btn btn-admin-primary" disabled style="display:none;">SAVING ... <i class="fas fa-spinner fa-spin" style="color: white"></i></button>
                                        <a href="{{route('admin.rider.view')}}" class="btn btn-admin-danger" >CANCEL</a>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

@stop
@section('script')
    @toastr_render
    <script>
        $("input[name='verification_id_type']"). click(function(){
            var radioValue = $("input[name='verification_id_type']:checked"). val();
            if(radioValue =="Birth Certificate"){
                $('.passport').hide();
                $('.birth_certificate').show();
                $('.driving_license').hide();
                $('.nid').hide();
            }
            else if(radioValue =="Passport"){
                $('.passport').show();
                $('.birth_certificate').hide();
                $('.driving_license').hide();
                $('.nid').hide();
            }
            else if(radioValue =="Driving License"){
                $('.passport').hide();
                $('.birth_certificate').hide();
                $('.driving_license').show();
                $('.nid').hide();
            }
            else{
                $('.passport').hide();
                $('.birth_certificate').hide();
                $('.driving_license').hide();
                $('.nid').show();
            }
        });
        $('#name').on('keyup',function (){
            var name =$('#name').val();
            if(!name.replace(/\s/g, '').length){
                $('#name').addClass('input-invalid');
                $('.name').show();
            }
            else{
                $("#name").removeClass('input-invalid');
                $('.name').hide();
            }
        });
        $('#nick_name').on('keyup',function (){
            var nick_name =$('#nick_name').val();
            if(!nick_name.replace(/\s/g, '').length){
                $('#nick_name').addClass('input-invalid');
                $('.nick_name').show();
            }
            else{
                $("#nick_name").removeClass('input-invalid');
                $('.nick_name').hide();
            }
        });
        $('#email').on('keyup',function (){
            var email =$('#email').val();
            if (email_pattern.test(email))
            {
                $('.email').hide();
                $("#email").removeClass('input-invalid');
            }
            else {
                $('.email').show();
                $(this).addClass('input-invalid');
            }
        });
        $('#phone').on('keyup',function () {
            var phone =$('#phone').val();
            if (wallet_number_pattern.test(phone))
            {
                $('.phone').hide();
                $(this).removeClass('input-invalid');
            }
            else {
                $('.phone').show();
                $(this).addClass('input-invalid');
            }
        });
        $('#father_name').on('keyup',function (){
            var father_name =$('#father_name').val();
            if(!father_name.replace(/\s/g, '').length){
                $('#father_name').addClass('input-invalid');
                $('.father_name').show();
            }
            else{
                $("#father_name").removeClass('input-invalid');
                $('.father_name').hide();
            }
        });
        $('#mother_name').on('keyup',function (){
            var mother_name =$('#mother_name').val();
            if(!mother_name.replace(/\s/g, '').length){
                $('#mother_name').addClass('input-invalid');
                $('.mother_name').show();
            }
            else{
                $("#mother_name").removeClass('input-invalid');
                $('.mother_name').hide();
            }
        });
        $('#present_address').on('keyup',function (){
            var present_address =$('#present_address').val();
            if(!present_address.replace(/\s/g, '').length){
                $('#present_address').addClass('input-invalid');
                $('.present_address').show();
            }
            else{
                $("#present_address").removeClass('input-invalid');
                $('.present_address').hide();
            }
        });
        $('#permanent_address').on('keyup',function (){
            var permanent_address =$('#permanent_address').val();
            if(!permanent_address.replace(/\s/g, '').length){
                $('#permanent_address').addClass('input-invalid');
                $('.permanent_address').show();
            }
            else{
                $("#permanent_address").removeClass('input-invalid');
                $('.permanent_address').hide();
            }
        });
        $('#verification_id_number').on('keyup',function (){
            var verification_id_number =$('#verification_id_number').val();
            if(!verification_id_number.replace(/\s/g, '').length){
                $("#verification_id_number").addClass('input-invalid');
                $('.verification_id_number').show();
            }
            else{
                $("#verification_id_number").removeClass('input-invalid');
                $('.verification_id_number').hide();
            }
        });
        $('#verification_id_pic').on('change',function (){
            var file = $('#verification_id_pic').val();
            var file1 = Validate(file);
            if(file1==1){
                $("#verification_id_pic").addClass('input-invalid');
                $('.verification_id_pic').show();
                $(this).val('');
            }
            else{
                $("#verification_id_pic").removeClass('input-invalid');
                $('.verification_id_pic').hide();
            }
        });
        $('#profile_pic').on('change',function (){
            var file = $('#profile_pic').val();
            var file1 = Validate(file);
            if(file1==1){
                $("#profile_pic").addClass('input-invalid');
                $('.profile_pic').show();
                $(this).val('');
            }
            else{
                $("#profile_pic").removeClass('input-invalid');
                $('.profile_pic').hide();
            }
        });
        $('#referrer_name').on('keyup',function (){
            var referrer_name =$('#referrer_name').val();
            if(!referrer_name.replace(/\s/g, '').length){
                $('#referrer_name').addClass('input-invalid');
                $('.referrer_name').show();
            }
            else{
                $("#referrer_name").removeClass('input-invalid');
                $('.referrer_name').hide();
            }
        });
        $('#referrer_phone').on('keyup',function () {
            var referrer_phone =$('#referrer_phone').val();
            if (wallet_number_pattern.test(referrer_phone))
            {
                $('.referrer_phone').hide();
                $(this).removeClass('input-invalid');
            }
            else {
                $('.referrer_phone').show();
                $(this).addClass('input-invalid');
            }
        });
        $('#referrer_address').on('keyup',function (){
            var referrer_address =$('#referrer_address').val();
            if(!referrer_address.replace(/\s/g, '').length){
                $('#referrer_address').addClass('input-invalid');
                $('.referrer_address').show();
            }
            else{
                $("#referrer_address").removeClass('input-invalid');
                $('.referrer_address').hide();
            }
        });

        $("#btn_fetch").click(function() {
            var name =$('#name').val();
            var nick_name =$('#nick_name').val();
            var email =$('#email').val();
            var phone =$('#phone').val();
            var father_name =$('#father_name').val();
            var mother_name =$('#mother_name').val();
            var present_address =$('#present_address').val();
            var permanent_address =$('#permanent_address').val();
            var verification_id_number =$('#verification_id_number').val();
            var verification_id_pic =$('#verification_id_pic').val();
            var profile_pic =$('#profile_pic').val();
            var referrer_name =$('#referrer_name').val();
            var referrer_phone =$('#referrer_phone').val();
            var referrer_address =$('#referrer_address').val();
            if (!name.replace(/\s/g, '').length || !nick_name.replace(/\s/g, '').length || !email_pattern.test(email) ||
                !(wallet_number_pattern.test(phone)) || !mother_name.replace(/\s/g, '').length ||
                !father_name.replace(/\s/g, '').length || !present_address.replace(/\s/g, '').length ||
                !permanent_address.replace(/\s/g, '').length || !verification_id_number.replace(/\s/g, '').length ||
                !verification_id_pic.replace(/\s/g, '').length || !profile_pic.replace(/\s/g, '').length ||
                !referrer_name.replace(/\s/g, '').length || !wallet_number_pattern.test(referrer_phone) || !referrer_address.replace(/\s/g, '').length )
            {
                $("#btn_fetch").hide();
                $('#btn_fetch_disable').show();
                setTimeout(function(){
                    $("#btn_fetch").show();
                    $('#btn_fetch_disable').hide();
                }, 1*1000);

                if(!name.replace(/\s/g, '').length){
                    $('#name').addClass('input-invalid');
                    $('.name').show();
                }
                if(!nick_name.replace(/\s/g, '').length){
                    $('#nick_name').addClass('input-invalid');
                    $('.nick_name').show();
                }
                if (!email_pattern.test(email)) {
                    $('.email').show();
                    $(this).addClass('input-invalid');
                }
                if (!wallet_number_pattern.test(phone)) {
                    $('.phone').show();
                    $('#phone').addClass('input-invalid');
                }
                if(!father_name.replace(/\s/g, '').length){
                    $('#father_name').addClass('input-invalid');
                    $('.father_name').show();
                }
                if(!mother_name.replace(/\s/g, '').length){
                    $('#mother_name').addClass('input-invalid');
                    $('.mother_name').show();
                }
                if(!present_address.replace(/\s/g, '').length){
                    $('#present_address').addClass('input-invalid');
                    $('.present_address').show();
                }
                if(!permanent_address.replace(/\s/g, '').length){
                    $('#permanent_address').addClass('input-invalid');
                    $('.permanent_address').show();
                }
                if(!verification_id_number.replace(/\s/g, '').length){
                    $("#verification_id_number").addClass('input-invalid');
                    $('.verification_id_number').show();
                }
                var file1 = Validate(verification_id_pic);
                if(file1==1){
                    $("#verification_id_pic").addClass('input-invalid');
                    $('.verification_id_pic').show();
                    $(this).val('');
                }
                var file2 = Validate(profile_pic);
                if(file2==1){
                    $("#profile_pic").addClass('input-invalid');
                    $('.profile_pic').show();
                    $(this).val('');
                }
                if(!referrer_name.replace(/\s/g, '').length){
                    $('#referrer_name').addClass('input-invalid');
                    $('.referrer_name').show();
                }
                if (!wallet_number_pattern.test(referrer_phone)) {
                    $('.phone').show();
                    $("#referrer_phone").addClass('input-invalid');
                }
                if(!referrer_address.replace(/\s/g, '').length){
                    $('#referrer_address').addClass('input-invalid');
                    $('.referrer_address').show();
                }
                toastr.error('Please fix the given errors.');
                return false;
            }
            else{
                $("#btn_fetch").hide();
                $('#btn_fetch_disable').show();
                return true;
            }
        });
    </script>
@stop