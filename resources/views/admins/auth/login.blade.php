@extends('admins.auth.master')
@section('title')
    MetroExpress || Admon LogIn
@stop
@section('css')
    <style>

    </style>
@stop
@section('content')
    <div class="container">
        <div class="row justify-content-center">

            <div class="col-xl-5 col-lg-5 col-md-4">

                <div class="card o-hidden border-0 shadow-lg my-5">
                    <div class="card-body p-0">
                        <!-- Nested Row within Card Body -->

                        <div class="text-center jm-logo mt-2">
                            <img src="{{asset('public/icon/logo.png')}}" height="100" class="rounded mx-auto d-block" alt="">                        </div>

                        <div class="p-5 pt-0 pb-0">

                            <form action="{{route('admin.submit')}}" method="post" class="user">
                                @csrf
                                <div class="form-group">
                                    <input type="email" class="form-control form-control-user" id="email" name="email" placeholder="Enter Email Address..." >
                                </div>
                                <div class="form-group">
                                    <input type="password" class="form-control form-control-user" minlength="6" id="password" name="password" placeholder="Password">
                                </div>
                                <div class="form-group">
                                    <div class="custom-control custom-checkbox small">
                                        <input type="checkbox" class="custom-control-input" id="customCheck">
                                        <label class="custom-control-label" for="customCheck">Remember Me</label>
                                    </div>
                                </div>
                                <div class="text-center">
                                    <button type="submit" class="btn btn-admin-primary btn-user btn-block " id="btn_fetch">LOGIN</button>
                                    <button type="submit" class="btn btn-admin-primary btn-user btn-block " disabled id="btn_fetch_disable" style="display: none; font-size: 12px;">LOGIN..<i class="fas fa-spinner fa-spin" style="color: white"></i></button>
                                </div>
                            </form>
                            <hr>
                            <div class="text-center">
                                <a class="small" href="#">Forgot Password?</a>
                            </div>

                        </div>


                    </div>
                </div>

            </div>

        </div>
    </div>
@stop
@section('script')
    @toastr_render
    <script>
        $("#btn_fetch").on('click',function () {
            $("#btn_fetch").hide();
            $("#btn_fetch_disable").show();
            return true;
        })

    </script>

@stop