@extends('admins.layouts.app')
@section('title')
    Rider Payment
@stop
@section('payment','active')
@section('collapsed-payment','')
@section('payment-c','show')
@section('RP','active')
@section('css')

@stop

@section('content')

    <div class="row">
        <div class="col-md-12">
            <div class="card shadow">
                <div class="card-header py-3">
                    <h6>Riders Overview</h6>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-striped" id="rider" width="100%" >
                            <thead>
                            <tr>
                                <th>Rider Name</th>
                                <th>Rider Phone</th>
                                <th>Order Assigned</th>
                                <th>Order Delivered</th>
                                <th>Order Hold</th>
                                <th>Order Failed</th>
                                <th>Cash Collection</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach (\App\RiderPayment::whereDate('created_at',\Carbon\Carbon::today())->where('status','Unpaid')->get() as $riderPayment)

                                <tr>
                                    <th>{{$riderPayment->rider->nick_name}}</th>
                                    <td>{{$riderPayment->rider->phone}}</td>
                                    <td>{{$riderPayment->total_assigned}}</td>
                                    <td>{{$riderPayment->total_delivered}}</td>
                                    <td>{{$riderPayment->total_hold}}</td>
                                    <td>{{$riderPayment->total_failed}}</td>
                                    <td>{{$riderPayment->cash_collecttion}}</td>
                                    <td>
                                        <div class="btn-group">
                                            <button type="button" class="btn btn-success" data-toggle="modal" data-target="#status-change-Modal"><i class='fas fa-dollar-sign'></i>Payment</button>
                                            <div class="modal fade" id="status-change-Modal" role="dialog">
                                                <div class="modal-dialog modal-xl">
                                                    <!-- Modal content-->
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <h4 class="modal-title">Rider Payment</h4>
                                                            <button type="button" class="close btn btn-warning" data-dismiss="modal">&times;</button>
                                                        </div>
                                                        <div class="modal-body">
                                                            <div class="container">
                                                                <div class="row">
                                                                    <div class="col-xl-12">
                                                                        <!-- Account details card-->
                                                                        <div class="card mb-4">
                                                                            <div class="card-header">Payment</div>
                                                                            <div class="card-body">
                                                                                    <h5>{{$riderPayment->rider->nick_name}}</h5>

                                                                                    <h5>{{$riderPayment->rider->phone}}</h5>

                                                                                <form action="{{route('admin.payment.rider-payment-store')}}" method="post">
                                                                                    @csrf
                                                                                    <input type="hidden" name="id" value="{{$riderPayment->id}}">
                                                                                    <div class="row">
                                                                                        <div class="col-md">
                                                                                            <input type="text" class="form-control search-slt currency" placeholder="Payment Amount" name="paid_amount" required>
                                                                                        </div>
                                                                                        <div class="col-md">
                                                                                            <select class="form-control search-slt" id="payment_type" name="payment_type">
                                                                                                <option value="Cash" selected>Cash</option>
                                                                                                <option value="Bkash">Bkash</option>
                                                                                            </select>
                                                                                        </div>
                                                                                        <div class="col">
                                                                                            <input type="text" class="form-control search-slt" name="reference" placeholder="Refference">
                                                                                        </div>
                                                                                        <div class="col-md">
                                                                                            <button type="submit" class="btn btn-admin-primary">Payment Confirmed</button>
                                                                                        </div>
                                                                                    </div>
                                                                                </form>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </td>
                            </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

@stop
@section('script')
    @toastr_render
    <script>
        $("#rider").dataTable();
        $(".currency").inputFilter(function(value) {
            return /^\d*[.]?\d{0,2}$/.test(value);
        });_
    </script>
@stop