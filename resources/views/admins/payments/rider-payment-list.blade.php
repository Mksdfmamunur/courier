@extends('admins.layouts.app')
@section('title')
    Rider Payment List
@stop
@section('payment','active')
@section('collapsed-payment','')
@section('payment-c','show')
@section('RPL','active')
@section('css')

@stop

@section('content')

    <div class="row">
        <div class="col-md-12">
            <div class="card shadow">
                <div class="card-header py-3">
                    <h6>Riders Payment List</h6>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-striped" id="rider" width="100%" >
                            <thead>
                            <tr>
                                <th>Payment Date</th>
                                <th>Invoice Id</th>
                                <th>Rider Name</th>
                                <th>Rider Phone</th>
                                <th>Order Assigned</th>
                                <th>Order Delivered</th>
                                <th>Order Hold</th>
                                <th>Order Failed</th>
                                <th>Cash Collection</th>
                                <th>Payment Amount</th>
                                <th>Payment By</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach (\App\RiderPayment::where('status','Paid')->get() as $riderPayment)

                                <tr>
                                    <th>{{$riderPayment->updated_at->format('M j, Y')}}</th>
                                    <th>{{$riderPayment->invoice_id}}</th>
                                    <th>{{$riderPayment->rider->nick_name}}</th>
                                    <td>{{$riderPayment->rider->phone}}</td>
                                    <td>{{$riderPayment->total_assigned}}</td>
                                    <td>{{$riderPayment->total_delivered}}</td>
                                    <td>{{$riderPayment->total_hold}}</td>
                                    <td>{{$riderPayment->total_failed}}</td>
                                    <td>{{$riderPayment->cash_collecttion}}</td>
                                    <td>{{$riderPayment->paid_amount}}</td>
                                    <td>{{$riderPayment->payment_type}}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

@stop
@section('script')
    @toastr_render
    <script>
        $("#rider").dataTable();
    </script>
@stop