@extends('admins.layouts.app')
@section('title')
    Merchant Payment Invoice
@stop
@section('payment','active')
@section('collapsed-payment','')
@section('payment-c','show')
@section('CMPIL','active')
@section('css')

@stop

@section('content')
    <div class="row">
        <div class="col">
            <div class="card">
                <div class="card-header">
                    Coverage Area List
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-striped" id="dataTable" width="100%" >
                            <thead>
                            <tr>
                                <th>Invoice Date</th>
                                <th>Invoice Id</th>
                                <th>Merchant Name</th>
                                <th>Merchant Phone</th>
                                <th>Collected</th>
                                <th>Fee</th>
                                <th>Receivable Amount</th>
                                <th>Status</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach(\App\MerchantPayment::where('status','Unpaid')->get() as $key=> $merchantPayment)
                                <tr>
                                    <td> {{$merchantPayment->created_at}}</td>
                                    <td> {{$merchantPayment->invoice_id}}</td>
                                    <td>{{$merchantPayment->merchant->first_name}} {{$merchantPayment->merchant->last_name}}</td>
                                    <td>{{$merchantPayment->merchant->phone_number}}</td>
                                    <td>{{$merchantPayment->collected_amount}}</td>
                                    <td>{{$merchantPayment->total_charge}}</td>
                                    <td>{{$merchantPayment->total_paid_amount}}</td>
                                    <td>{{$merchantPayment->status}}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
@section('script')
    @toastr_render
    <script>
        $("#dataTable").dataTable();
    </script>
@stop