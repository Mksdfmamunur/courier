@extends('admins.layouts.app')
@section('title')
    Merchant Payment Invoice
@stop
@section('payment','active')
@section('collapsed-payment','')
@section('payment-c','show')
@section('CMPI','active')
@section('css')
    <style>
        /*.readable{*/
        /*    background: transparent !important;*/
        /*    border: none !important;*/
        /*}*/
        /*.readable:focus{*/
        /*    outline: none !important;*/
        /*}*/

    </style>
@stop

@section('content')

    <div class="row">
        <div class="col-md-12">
            <div class="card shadow">
                <div class="card-header py-3">
                    <h6>Riders Overview</h6>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-striped" id="rider" width="100%" >
                            <thead>
                            <tr>
                                <th>Merchant Name</th>
                                <th>Merchant Phone</th>
                                <th>Total Order</th>
                                <th>Total Collection</th>
                                <th>Total Charge</th>
                                <th>Cash Payable</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>

                            @foreach (\App\Order::select('merchant_id')->where('delivery_status',4)->where('payment_status','Unpaid')->distinct()->get() as $merchantId)
                                @foreach(\App\Merchant::where('id',$merchantId->merchant_id)->get() as $merchant)
                                    <?php
                                        $orders = \App\Order::where('merchant_id',$merchant->id)->where('delivery_status',4)->where('payment_status','Unpaid')->get();


                                        $totalCollection =0;
                                        foreach ($orders as $order)
                                            {
                                                $totalCollection +=$order->amount_to_collect;
                                            }
                                        $totalCharge =0;
                                        foreach ($orders as $order)
                                        {
                                            $totalCharge +=$order->total_charge;
                                        }
                                    $totalPayable =0;
                                    foreach ($orders as $order)
                                    {
                                        $totalPayable +=$order->paid_out;
                                    }
                                    ?>
                                    <tr>
                                        <th>{{$merchant->first_name}} {{$merchant->last_name}}</th>
                                        <td>{{$merchant->mobile_number}}</td>
                                        <td>{{count($orders)}}</td>
                                        <td>{{$totalCollection}}</td>
                                        <td>{{$totalCharge}}</td>
                                        <td>{{$totalPayable}}</td>
                                        <td>
                                            <form action="{{route('admin.payment.merchant-payment-invoice-store')}}" method="post">
                                                @csrf
                                                @foreach ($orders as $order)
                                                    <input type="hidden" value="{{$order->id}}" name="order_id[]">
                                                @endforeach
                                                <input type="hidden" value="{{$merchant->id}}" name="merchant_id">
                                                <input type="hidden"  value="{{$totalCollection}}" name="collected_amount">
                                                <input type="hidden"  value="{{$totalCharge}}" name="total_charge">
                                                <input type="hidden"  value="{{$totalPayable}}" name="total_paid_amount">
                                                <button type="submit" class="btn btn-admin-primary">Create Invoice</button>
                                            </form>
                                        </td>
                                    </tr>
                                @endforeach
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

@stop
@section('script')
    @toastr_render
    <script>
        $("#rider").dataTable();
        $(".currency").inputFilter(function(value) {
            return /^\d*[.]?\d{0,2}$/.test(value);
        });
    </script>
@stop