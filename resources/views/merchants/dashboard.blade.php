@extends('merchants.layouts.app')
@section('title')
    MetroExpress || Dashboard
@stop
@section('dashboard','active')
@section('css')
    <style>
        .toast-body{
            background-color:#F5F6F7 ;
        }
        .percent{
            font-family: 'Titillium Web', sans-serif;
            font-size: 20px;
        }
    </style>
@stop

@section('content')

    <?php
            $order = \App\Order::where('merchant_id',\Illuminate\Support\Facades\Auth::id())->get();
            $order_deleted = \App\Order::where('merchant_id',\Illuminate\Support\Facades\Auth::id())->where('delivery_status',7)->get();
            $order_rejected = \App\Order::where('merchant_id',\Illuminate\Support\Facades\Auth::id())->where('delivery_status',11)->get();
            $total_order = (count($order)-(count($order_deleted)+count($order_rejected)));

            $order_paids = \App\Order::where('merchant_id',\Illuminate\Support\Facades\Auth::id())->where('payment_status',"Paid")->get();
            $paid_amount = 0;
            foreach ($order_paids as $paid)
                {
                    $paid_amount+=$paid->paid_out;
                }
            $order_dues = \App\Order::where('merchant_id',\Illuminate\Support\Facades\Auth::id())->where('delivery_status',4)->where('payment_status',"Unpaid")->get();
            $due_amount = 0;
            foreach ($order_dues as $due)
            {
                $due_amount+=$due->paid_out;
            }

            $order_delivered = \App\Order::where('merchant_id',\Illuminate\Support\Facades\Auth::id())->where('delivery_status',4)->get();

            $order_pickup_completed = \App\Order::where('merchant_id',\Illuminate\Support\Facades\Auth::id())->where('delivery_status',3)->get();
            $order_pickup_approved = \App\Order::where('merchant_id',\Illuminate\Support\Facades\Auth::id())->where('delivery_status',2)->get();
            $order_pickup_pending = \App\Order::where('merchant_id',\Illuminate\Support\Facades\Auth::id())->where('delivery_status',1)->get();
            $order_order_confirmed = \App\Order::where('merchant_id',\Illuminate\Support\Facades\Auth::id())->where('delivery_status',12)->get();
            $order_hold = \App\Order::where('merchant_id',\Illuminate\Support\Facades\Auth::id())->where('delivery_status',6)->get();
            $order_shipped = \App\Order::where('merchant_id',\Illuminate\Support\Facades\Auth::id())->where('delivery_status',5)->get();
            $order_returning = \App\Order::where('merchant_id',\Illuminate\Support\Facades\Auth::id())->where('delivery_status',9)->get();
            $order_returned = \App\Order::where('merchant_id',\Illuminate\Support\Facades\Auth::id())->where('delivery_status',10)->get();
            $on_processing = count($order_pickup_completed)+count($order_pickup_approved)+count($order_pickup_pending)+count($order_order_confirmed);
            ?>
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12">
                <div class="toast" data-autohide="false">
                    <div class="toast-header notice_board">
                        <strong class="ml-2 text-primary">{{\Carbon\Carbon::now()->format('l jS F Y h:i:s A')}}</strong>
                        <strong class="m-auto text-primary">Notice!!</strong>
                        <button type="button" class="mr-2 mb-1 close" data-dismiss="toast">&times;</button>
                    </div>
                    <div class="toast-body">
                        <div class="row">
                            <div class="col-sm-4">
                                Dear Merchant, welcome to MetroExpress
                            </div>
                            <div class="col-sm-8">
                                <div class="jm_bangla">বিকাল ৪ টার পর দেয়া রিকুয়েস্ট পরের দিন পিকাপ করা হবে|</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
    <div class="container-fluid mt-4">
        <div class="row">
            <div class="col-lg-3 col-md-3 col-sm-12 merchant_db_box box_1">
                <div class="row">
                    <div class="col-sm-9 mdb_box_content">
                        <span>Total Orders</span>{{$total_order}}
                    </div>
                    <div class="col-sm-3 db_icon">
                        <svg xmlns="http://www.w3.org/2000/svg" width="35.593" height="39.761" viewBox="0 0 35.593 39.761">
                            <path id="Icon_simple-codesandbox" data-name="Icon simple-codesandbox" d="M3,9.94,20.72,0,38.44,9.94l.153,19.8L20.72,39.761,3,29.821Zm3.539,4.11v7.881l5.669,3.081v5.825l6.732,3.8v-13.7Zm28.371,0-12.4,6.887v13.7l6.732-3.8V25.016l5.669-3.083V14.049Zm-26.6-3.115L20.689,17.8,33.1,10.876l-6.561-3.64-5.78,3.222L14.942,7.2,8.31,10.934Z" transform="translate(-3)" />
                        </svg></div>
                </div>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-12 merchant_db_box box_2">
                <div class="row">
                    <div class="col-sm-9 mdb_box_content">
                        <span>Total Delivered</span>{{count($order_delivered)}}
                    </div>
                    <div class="col-sm-3 db_icon">
                        <svg xmlns="http://www.w3.org/2000/svg" width="45" height="40.072" viewBox="0 0 45 40.072">
                            <g transform="translate(0 -26.775)">
                                <g transform="translate(0 26.775)">
                                    <path d="M0,64.6V32.8a2.243,2.243,0,0,1,2.245-2.245h28.2L25.96,35.034H4.482V62.356H31.8v-11.6l4.482-4.482V64.6a2.248,2.248,0,0,1-2.245,2.245H2.245A2.243,2.243,0,0,1,0,64.6ZM19.518,47.393l-4.785-4.785a3.446,3.446,0,0,0-5.88,2.439,3.388,3.388,0,0,0,1.012,2.429l7.215,7.215a3.444,3.444,0,0,0,4.868,0l22.04-22.04a3.442,3.442,0,0,0-4.868-4.868Z" transform="translate(0 -26.775)" />
                                </g>
                            </g>
                        </svg></div>
                </div>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-12 merchant_db_box box_1">
                <div class="row">
                    <div class="col-sm-9 mdb_box_content">
                        <span>Total Paid Amount</span>{{$paid_amount}}
                    </div>
                    <div class="col-sm-3 db_icon">
                        <svg xmlns="http://www.w3.org/2000/svg" width="45.018" height="45.017" viewBox="0 0 45.018 45.017">
                            <g transform="translate(-0.001 -0.006)">
                                <g transform="translate(0.001 20.909)">
                                    <g transform="translate(0)">
                                        <path d="M.614,239.414a.938.938,0,0,0-.557,1.2l.589,1.621,4.485-4.485Z" transform="translate(-0.001 -237.75)" />
                                    </g>
                                </g>
                                <g transform="translate(4.859 36.978)">
                                    <path d="M55.257,420.513l2.7,7.428a.929.929,0,0,0,.486.529.943.943,0,0,0,.4.088.918.918,0,0,0,.325-.058l2.98-1.1Z" transform="translate(-55.257 -420.513)" />
                                </g>
                                <g transform="translate(25.039 22.138)">
                                    <path d="M304.7,260.223l-3.091-8.5-7.418,7.418,3.676-1.354a.938.938,0,1,1,.649,1.759l-4.67,1.722a.937.937,0,0,1-1.2-.555c0-.006,0-.011,0-.017l-7.861,7.861,19.364-7.135A.935.935,0,0,0,304.7,260.223Z" transform="translate(-284.783 -251.724)" />
                                </g>
                                <g transform="translate(0.002 0.006)">
                                    <path d="M44.76,15.286,29.755.28a.939.939,0,0,0-1.326,0L.293,28.415a.939.939,0,0,0,0,1.326L15.3,44.747a.928.928,0,0,0,.662.276.942.942,0,0,0,.664-.274L44.76,16.614A.941.941,0,0,0,44.76,15.286ZM11,25.992,7.246,29.743a.938.938,0,1,1-1.328-1.326L9.67,24.666A.938.938,0,0,1,11,25.992Zm16.2,1.191a3.54,3.54,0,0,1-2.57.983,7.75,7.75,0,0,1-5.216-2.491,8.593,8.593,0,0,1-2.343-3.991,3.919,3.919,0,0,1,.835-3.794,3.907,3.907,0,0,1,3.794-.835A8.577,8.577,0,0,1,25.686,19.4C28.292,22,28.956,25.422,27.194,27.183ZM39.133,16.614l-3.751,3.751a.938.938,0,0,1-1.328-1.326L37.8,15.288a.938.938,0,0,1,1.328,1.326Z" transform="translate(-0.019 -0.006)" />
                                </g>
                            </g>
                        </svg></div>
                </div>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-12 merchant_db_box box_2">
                <div class="row">
                    <div class="col-sm-9 mdb_box_content">
                        <span>Total Due Amount</span>{{$due_amount}}
                    </div>
                    <div class="col-sm-3 db_icon">
                        <svg xmlns="http://www.w3.org/2000/svg" width="31.286" height="36.5" viewBox="0 0 31.286 36.5">
                            <defs>
                                <style>
                                    .a {
                                        fill-rule: evenodd;
                                    }
                                </style>
                            </defs>
                            <path class="a" d="M5.8,38.75a1.3,1.3,0,0,1,0-2.607H8.411V33.536a11.732,11.732,0,0,1,6.666-10.585,1.828,1.828,0,0,0,1.155-1.538V19.588a1.829,1.829,0,0,0-1.155-1.538A11.732,11.732,0,0,1,8.411,7.464V4.857H5.8a1.3,1.3,0,0,1,0-2.607H34.482a1.3,1.3,0,1,1,0,2.607H31.875V7.464a11.732,11.732,0,0,1-6.666,10.585,1.828,1.828,0,0,0-1.155,1.538v1.825a1.829,1.829,0,0,0,1.155,1.538,11.732,11.732,0,0,1,6.666,10.586v2.607h2.607a1.3,1.3,0,1,1,0,2.607ZM11.018,4.857V7.464a9.058,9.058,0,0,0,.878,3.911H28.388a9.092,9.092,0,0,0,.878-3.911V4.857H11.017Zm7.821,16.555A4.42,4.42,0,0,1,16.2,25.3a9.125,9.125,0,0,0-5.185,8.234s2.257-3.387,7.821-3.859V21.412Zm2.607,0a4.42,4.42,0,0,0,2.636,3.89,9.125,9.125,0,0,1,5.185,8.234s-2.257-3.387-7.821-3.859V21.412Z" transform="translate(-4.5 -2.25)" />
                        </svg></div>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid mt-4">
        <div class="row">
            <div class="col-lg-9 col-md-9 col-sm-12">
{{--                <div class="row">--}}
{{--                    <div class="offset-3 col-6">--}}
{{--                        <form action="">--}}
{{--                            <div class="mb-2 text-center">--}}
{{--                                <input type="text" class="form-control" name="date" id="date" readonly/>--}}
{{--                            </div>--}}
{{--                        </form>--}}
{{--                    </div>--}}
{{--                </div>--}}
                <?php
                    if ($total_order==0)
                    $total_order=1;
                ?>
                <div class="row">
                    <div class="col-lg-4 col-md-4 col-sm-6 mdbItem2">
                        <div class="mdbItem2Children">
                            <div class="row">
                                <div class="col-3 mb-1">
                                    <img src="{{asset('public/icon/shopping-bag.svg')}}" class="mdbItem2Svg" alt="Delivered"/>
                                </div>
                                <div class="col-9 mdbItem2Text">
                                    <h1>{{count($order_delivered)}}</h1>
                                    <div>
                                        <div class="mdbItem2Description"><span class="percent">{{number_format((count($order_delivered)/$total_order)*100)}}%</span> Delivered</div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col col-sm-12 progress-outer">

                                    <div class="progress">
                                        <div class="progress-bar bg-process" role="progressbar" style="width: {{(count($order_delivered)/$total_order)*100}}%;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-6 mdbItem2">
                        <div class="mdbItem2Children">
                            <div class="row">
                                <div class="col-3 mb-1">
                                    <img src="{{asset('public/icon/hourglass.svg')}}" class="mdbItem2Svg" alt="Hold at hub"/>
                                </div>
                                <div class="col-9 mdbItem2Text">
                                    <h1>{{count($order_hold)}}</h1>
                                    <div>
                                        <div class="mdbItem2Description"><span class="percent">{{number_format((count($order_hold)/$total_order)*100)}}%</span> Hold at hub</div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col col-sm-12 progress-outer">

                                    <div class="progress">
                                        <div class="progress-bar bg-process" role="progressbar" style="width: {{(count($order_hold)/$total_order)*100}}%;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-6 mdbItem2">
                        <div class="mdbItem2Children">
                            <div class="row">
                                <div class="col-3 mb-1">
                                    <img src="{{asset('public/icon/delivery-man.svg')}}" class="mdbItem2Svg" alt="Delivery On way"/>
                                </div>
                                <div class="col-9 mdbItem2Text">
                                    <h1>{{count($order_shipped)}}</h1>
                                    <div>
                                        <div class="mdbItem2Description"><span class="percent">{{number_format((count($order_shipped)/$total_order)*100)}}%</span> Delivery On way</div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col col-sm-12 progress-outer">

                                    <div class="progress">
                                        <div class="progress-bar bg-process" role="progressbar" style="width: {{(count($order_hold)/$total_order)*100}}%;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
{{--                    <div class="col-lg-3 col-md-4 col-sm-6 mdbItem2">--}}
{{--                        <div class="mdbItem2Children">--}}
{{--                            <div class="row">--}}
{{--                                <div class="col-3 mb-1">--}}
{{--                                    <img src="{{asset('public/icon/receive.svg')}}" class="mdbItem2Svg" alt="delivery faild"/>--}}
{{--                                </div>--}}
{{--                                <div class="col-9 mdbItem2Text">--}}
{{--                                    <h1>1256</h1>--}}
{{--                                    <div>--}}
{{--                                        <div class="mdbItem2Description">Delivery faild</div>--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                            </div>--}}
{{--                            <div class="row">--}}
{{--                                <div class="col col-sm-12 progress-outer">--}}

{{--                                    <div class="progress">--}}
{{--                                        <div class="progress-bar bg-process" role="progressbar" style="width: 70%;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100">70%</div>--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                    <div class="col-lg-3 col-md-4 col-sm-6 mdbItem2">--}}
{{--                        <div class="mdbItem2Children">--}}
{{--                            <div class="row">--}}
{{--                                <div class="col-3 mb-1">--}}
{{--                                    <img src="{{asset('public/icon/delivery-truck.svg')}}" class="mdbItem2Svg" alt="On transit"/>--}}
{{--                                </div>--}}
{{--                                <div class="col-9 mdbItem2Text">--}}
{{--                                    <h1>1256</h1>--}}
{{--                                    <div>--}}
{{--                                        <div class="mdbItem2Description">On transit</div>--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                            </div>--}}
{{--                            <div class="row">--}}
{{--                                <div class="col col-sm-12 progress-outer">--}}

{{--                                    <div class="progress">--}}
{{--                                        <div class="progress-bar bg-process" role="progressbar" style="width: 70%;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100">70%</div>--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                    </div>--}}
                    <div class="col-lg-4 col-md-4 col-sm-6 mdbItem2">
                        <div class="mdbItem2Children">
                            <div class="row">
                                <div class="col-3 mb-1">
                                    <img src="{{asset('public/icon/process (1).svg')}}" class="mdbItem2Svg" alt="On processing"/>
                                </div>
                                <div class="col-9 mdbItem2Text">
                                    <h1>{{$on_processing}}</h1>
                                    <div>
                                        <div class="mdbItem2Description"><span class="percent">{{number_format(($on_processing/$total_order)*100)}}%</span> On processing</div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col col-sm-12 progress-outer">

                                    <div class="progress">
                                        <div class="progress-bar bg-process" role="progressbar" style="width: {{($on_processing/$total_order)*100}}%;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-6 mdbItem2">
                        <div class="mdbItem2Children">
                            <div class="row">
                                <div class="col-3 mb-1">
                                    <img src="{{asset('public/icon/return-box.svg')}}" class="mdbItem2Svg" alt="Return pending"/>
                                </div>
                                <div class="col-9 mdbItem2Text">
                                    <h1>{{count($order_returning)}}</h1>
                                    <div>
                                        <div class="mdbItem2Description"><span class="percent">{{number_format((count($order_returning)/$total_order)*100)}}%</span> Return pending</div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col col-sm-12 progress-outer">

                                    <div class="progress">
                                        <div class="progress-bar bg-process" role="progressbar" style="width: {{(count($order_returning)/$total_order)*100}}%;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-6 mdbItem2">
                        <div class="mdbItem2Children">
                            <div class="row">
                                <div class="col-3 mb-1">
                                    <img src="{{asset('public/icon/product-return.svg')}}" class="mdbItem2Svg" alt="Returned to merchant"/>
                                </div>
                                <div class="col-9 mdbItem2Text">
                                    <h1>{{count($order_returned)}}</h1>
                                    <div>
                                        <div class="mdbItem2Description"><span class="percent">{{number_format((count($order_returned)/$total_order)*100)}}%</span> Returned to merchant</div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col col-sm-12 progress-outer">

                                    <div class="progress">
                                        <div class="progress-bar bg-process" role="progressbar" style="width: {{(count($order_returned)/$total_order)*100}}%;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100">70%</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
            <div class="col-lg-3 col-md-3 col-sm-6 mdb_r8_clmn">
                Quick Links
                <br>
                <a href="{{route("merchant.order.create")}}" class="btn btn-primary btn-icon-split">
                    <span class="icon text-white-50">
                        <i class="fas fa-flag"></i>
                    </span>
                    <span class="text">Create Order</span>
                </a><br>
                <a href="{{route('merchant.coverage-area')}}" class="btn btn-primary btn-icon-split">
                    <span class="icon text-white-50">
                        <i class="fas fa-flag"></i>
                    </span>
                    <span class="text">Coverage Area</span>
                </a>
                <br>
{{--                <a href="#" class="btn btn-primary btn-icon-split">--}}
{{--                    <span class="icon text-white-50">--}}
{{--                        <i class="fas fa-flag"></i>--}}
{{--                    </span>--}}
{{--                    <span class="text">Pricing Plan</span>--}}
{{--                </a><br>--}}
{{--                <a href="#" class="btn btn-primary btn-icon-split">--}}
{{--                    <span class="icon text-white-50">--}}
{{--                        <i class="fas fa-flag"></i>--}}
{{--                    </span>--}}
{{--                    <span class="text">Support Ticket</span>--}}
{{--                </a>--}}
{{--                <br>--}}
{{--                <a href="#" class="btn btn-primary btn-icon-split">--}}
{{--                    <span class="icon text-white-50">--}}
{{--                        <i class="fas fa-flag"></i>--}}
{{--                    </span>--}}
{{--                    <span class="text">Create Shop</span>--}}
{{--                </a><br>--}}
{{--                <a href="#" class="btn btn-primary btn-icon-split">--}}
{{--                    <span class="icon text-white-50">--}}
{{--                        <i class="fas fa-flag"></i>--}}
{{--                    </span>--}}
{{--                    <span class="text">Demo</span>--}}
{{--                </a>--}}
            </div>
        </div>

    </div>
@stop
@section('script')
    @toastr_render
    <script type="text/javascript" src="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.js"></script>    @toastr_render
    <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.css"/>
    <script>
        $(document).ready(function() {
            $('.toast').toast('show');
        });

        $(function () {
            let dateInterval = getQueryParameter('date');
            let start = moment().subtract(29, 'days').startOf('month');
            let end = moment().endOf('isoWeek');
            if (dateInterval) {
                dateInterval = dateInterval.split(' - ');
                start = dateInterval[0];
                end = dateInterval[1];
            }
            $('#date').daterangepicker({
                'autoApply':true,
                "showDropdowns": true,
                "showWeekNumbers": true,
                "alwaysShowCalendars": true,
                startDate: start,
                endDate: end,
                locale: {
                    format: 'DD/MM/YYYY',
                    firstDay: 1,
                },
                ranges: {
                    'Today': [moment(), moment()],
                    'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                    'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                    'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                    'This Month': [moment().startOf('month'), moment().endOf('month')],
                    'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')],
                    'This Year': [moment().startOf('year'), moment().endOf('year')],
                    'Last Year': [moment().subtract(1, 'year').startOf('year'), moment().subtract(1, 'year').endOf('year')],
                    'All time': [moment().subtract(30, 'year').startOf('month'), moment().endOf('month')],
                }
            });
            // $('#date').val('');
        });
        function getQueryParameter(name) {
            const url = window.location.href;
            name = name.replace(/[\[\]]/g, "\\$&");
            const regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
                results = regex.exec(url);
            if (!results) return null;
            if (!results[2]) return '';
            return decodeURIComponent(results[2].replace(/\+/g, " "));
        }
    </script>
@stop