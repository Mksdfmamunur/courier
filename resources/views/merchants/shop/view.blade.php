@extends('merchants.layouts.app')
@section('title')
    Merchant || View Shop
@stop
{{--@section('dashboard','active')--}}
@section('css')

@stop

@section('content')
<div class="container-fluid mt-3">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h4>My Shop</h4>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-striped">
                            <thead>
                            <tr>
                                <th>Shop Id</th>
                                <th>Shop Name</th>
                                <th>Pickup Number</th>
                                <th>Pickup Area</th>
                                <th>Pickup Address</th>
                                <th>Status</th>
{{--                                <th>Action</th>--}}
                            </tr>
                            </thead>
                            <tbody>
                            @foreach (\App\MerchantShop::where('merchant_id',\Illuminate\Support\Facades\Auth::id())->get() as $shop)
                                <tr>
                                    <td>{{$shop->shop_id}}</td>
                                    <td>{{$shop->shop_name}}</td>
                                    <td>{{$shop->pickup_number}}</td>
                                    <td>{{$shop->pickup_area}}</td>
                                    <td>{{$shop->pickup_address}}</td>
                                    <td>{{$shop->status}}</td>
{{--                                    <td><a href="{{route('merchant.shop.edit', encrypt($shop->id))}}" class="btn btn-warning phone1"><i class='fas fa-wrench'></i></a></td>--}}
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@stop
@section('script')
    @toastr_render
    <script>
        $('.table').DataTable();
    </script>
@stop