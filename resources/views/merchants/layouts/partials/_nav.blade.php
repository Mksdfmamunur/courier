<nav class="navbar navbar-expand-md navbar-light bg-light jm_nav">
    <button type="button" class="navbar-toggler" data-toggle="collapse" data-target="#navbarCollapse">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div>
        <a href="{{route('merchant.dashboard')}}" class="navbar-brand">
            <img src="{{asset('public/icon/logo.png')}}" width="120" alt="">
        </a>
    </div>
    <div class="collapse navbar-collapse " id="navbarCollapse">
        <div class="navbar-nav">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item @yield('dashboard')">
                    <a class="nav-link" href="{{route('merchant.dashboard')}}">DASHBOARD</a>
                </li>
                <li class="nav-item @yield('orders')">
                    <a class="nav-link" href="{{route('merchant.order.view')}}">ORDERS</a>
                </li>
                <li class="nav-item @yield('invoices')">
                    <a class="nav-link" href="{{route('merchant.invoice.receivable')}}">INVOICES</a>
                </li>
            </ul>
        </div>
        <form class="form-inline" id="track" action="{{route('merchant.track-order')}}" method="get">
            <div class="input-group">
                <input class="form-control" type="text" name="trackingId" placeholder="পার্সেল ID">
                <div class="input-group-append">
                    <button class="btn btn-pr" type="submit">Track করুন</button>
                </div>
            </div>
        </form>
        <div class="navbar-nav">
            <ul class="navbar-nav">
                <li class="nav-item">
                    <a class="btn btn-pr my-2 my-sm-0" href="{{route('merchant.order.create')}}">CREATE ORDERS</a>
                </li>
            </ul>
        </div>
    </div>
    <div>
        <div class="nav-item dropdown">
                <button type=" button" class="btn btn-2 dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <span class="user_icon"><i class="fas fa-user-tie"></i></span>
                </button>
                <div class="dropdown-menu">
                    <a href="{{route('merchant.profile')}}" class="dropdown-item" type="button">Profile</a>
                    <a href="{{route('merchant.shop.view')}}" class="dropdown-item" type="button">My shop</a>
                    <a href="{{route('merchant.payment-info.view')}}" class="dropdown-item" type="button">My Payment Info</a>
                    <form id="logout-form" action="{{ route('merchant.logout') }}" method="POST">
                        @csrf
                        <button class="dropdown-item" type="submit">Logout</button>
                    </form>
                </div>
            </div>
    </div>
</nav>