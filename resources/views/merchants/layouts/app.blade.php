<!doctype html>
<html lang="en">

<meta http-equiv="content-type" content="text/html;charset=utf-8"/><!-- /Added by HTTrack -->
<head>
    <title>@yield('title')</title>
    <meta charset="utf-8" />
    <meta name="_token" content="{{ csrf_token() }}">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="description" content="#">
    <meta name="keywords" content="#">
    <meta name="author" content="MetroExpress">
    <!-- Favicon icon -->
    <link rel="icon" href="{{asset('public/icon/logo.png')}}" type="image/x-icon">
    <link rel="stylesheet" href="{{ asset('public/css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{ asset('public/assets/fontawesome-5.14.0/css/all.css')}}">
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/css/select2.min.css" rel="stylesheet" />
    <link href='https://fonts.googleapis.com/css?family=Titillium Web' rel='stylesheet'>
    <link href="https://fonts.maateen.me/solaiman-lipi/font.css" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.css">
    <link rel="stylesheet" type="text/css" href="http://cdn.datatables.net/1.10.22/css/jquery.dataTables.min.css"/>

    {{--    custom--}}
    <link rel="stylesheet" href="{{ asset('public/css/merchant-custom.css')}}">
    <style>
        .user_icon {
            background-color: gray;
            border-radius: 50%;
            padding: 4px 11px;
            color: white;
            font-size: 20px;
        }
        .select2-container .select2-selection--single {

            height: 38px !important;
            padding: 2px 0px !important;
        }
        .dropdown-item:focus, .dropdown-item:hover {
            color: #fff;
            text-decoration: none;
            background-color: #0F996D;
        }
        .btn-pr:hover {
            color: #fff;
            text-decoration: none;
            background-color: #0F9949;
        }
        .collapse.show {
            display: block;
            background-color: #F5F6F7;
            z-index: 1000;
        }
        .dropdown-menu.show {
            display: block;
            margin-left: -80%;
        }
        button.btn.btn-2.dropdown-toggle {
            box-shadow: 0 0 black;
        }
         #dataTables_Filter{
             float: right;
         }
        .page-container {
            position: relative;
            min-height: 100vh;
        }

        .content-wrap {
            padding-bottom: 3.5rem;    /* Footer height */
        }

        footer {
            position: absolute;
            bottom: 0;
            width: 100%;
            height: 2.5rem;            /* Footer height */
        }
        .tracking-icon img{
            width: 42px;
            height: 42px;
        }
        @media (max-width: 768px){
            .navbar-expand-md .navbar-nav .dropdown-menu {
                margin-left: 0;
            }
            #track{
                display: none;
            }
        }

    </style>

    @yield('css')
</head>
<body>
<div class="page-container">
    <div class="content-wrap">
        @include('merchants.layouts.partials._nav')
        @yield('content')
    </div>
    <footer class="sticky-footer bg-white">
        <div class="container my-auto">
            <div class="copyright text-center my-auto">
                <span>Copyright &copy; MetroExpress 2020</span>
            </div>
        </div>
    </footer>
</div>

<script src="{{ asset('public/js/jquery.min.js')}}"></script>
<script src="{{ asset('public/js/popper.min.js')}}"></script>
<script type="text/javascript" src="//cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script><script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js"></script>
<script src="{{ asset('public/js/numberCheck.js')}}"></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.22/js/jquery.dataTables.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>
@yield('script')

</body>
</html>