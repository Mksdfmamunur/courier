@if (count($orders)!=0)
    @foreach($orders as $key =>$order)
        @if ($key % 2 == 0)
            <div class="row ap_table_body pt-1 pb-1 border-bottom bg-white">
                <div class="col-lg-1 col-md-1 col-sm-12 ap_table_body_odd">
                    {{$order->created_at->format('M j, Y')}}
                </div>
                <div class="col-lg-2 col-md-2 col-sm-12 ap_table_body_odd">
                    <h6><a href="{{route('merchant.track-order',['trackingId'=>$order->tracking_id])}}">{{$order->tracking_id}}</a></h6> Type: <span class="badge badge-success"> {{$order->order_type}} </span>
                </div>
                <div class="col-lg-1 col-md-1 col-sm-12 ap_table_body_odd">
                    {{$order->merchant_order_id}}
                </div>
                <div class="col-lg-1 col-md-1 col-sm-12 ap_table_body_odd">
                    {{$order->merchantShop->shop_name}}
                </div>
                <div class="col-lg-3 col-md-3 col-sm-12 ap_table_body_odd">
                    <span class="txt_bottom_all"><i class="fas fa-user" aria-hidden="true"></i></span> {{$order->recipient_name}} <br>
                    <span class="txt_bottom_all"><i class="fas fa-phone-square" aria-hidden="true"></i></span> {{$order->recipient_phone}} <br>
                    <span class="txt_bottom_all"><i class="fas fa-location-arrow" aria-hidden="true"></i></span> {{$order->recipient_address}}
                </div>
                <div class="col-lg-1 col-md-1 col-sm-12 ap_table_body_odd">
                    @if ($order->delivery_status==7 || $order->delivery_status==11)
                        <span class="badge badge-danger">{{$order->status->name}}</span>
                    @else
                        <span class="badge badge-success">{{$order->status->name}}</span>
                    @endif

                    <span class="txt_bottom">Updated_on</span> {{Carbon\Carbon::parse($order->delivery_status_date)->format('d/m/Y g:i A')}}

                </div>
                <div class="col-lg-1 col-md-1 col-sm-12 ap_table_body_odd">
                    <span class="payment">Amount: {{$order->amount_to_collect}}</span>
                    <span class="payment">Charge: {{$order->delivery_charge +$order->cod_charge}}</span>
                    @if($order->cod_charge>0)
                        <span class="badge badge-primary">COD ADDED</span>
                    @endif
                </div>
                <div class="col-lg-1 col-md-1 col-sm-12 ap_table_body_odd">
                    <span class="badge badge-success">{{$order->payment_status}}</span>
                    @if($order->payment_status=="Paid")
                        <span class="txt_bottom">Paid_at</span> Carbon\Carbon::parse($order->payment_date)->format('d/m/Y')}}
                    @endif
                </div>
                @if ($order->delivery_status==1 || $order->delivery_status == 2)
                    <div class="col-lg-1 col-md-1 col-sm-12 ap_table_body_odd">
                        {{--                    <a href="#" class="btn btn-primary phone1"><i class='fas fa-eye'></i> </a>--}}
                        <a href="{{route('merchant.order.edit', encrypt($order->id))}}" class="btn btn-warning phone1"><i class='fas fa-wrench'></i></a>
                        <a onclick="confirm_modal('{{route('merchant.order.status', encrypt($order->id))}}','Are you sure delete Order ?');" class="btn btn-danger"><i class="far fa-trash-alt text-white"></i></a>
                    </div>
                @endif
            </div>
        @else
            <div class="row ap_table_body pt-1 pb-1 border-bottom bg-transparent">
                <div class="col-lg-1 col-md-1 col-sm-12 ap_table_body_odd">
                    {{$order->created_at->format('M j, Y')}}
                </div>
                <div class="col-lg-2 col-md-2 col-sm-12 ap_table_body_odd">
                    <h6><a href="{{route('merchant.track-order',['trackingId'=>$order->tracking_id])}}">{{$order->tracking_id}}</a></h6> Type: <span class="badge badge-success"> {{$order->order_type}} </span>
                </div>
                <div class="col-lg-1 col-md-1 col-sm-12 ap_table_body_odd">
                    {{$order->merchant_order_id}}
                </div>
                <div class="col-lg-1 col-md-1 col-sm-12 ap_table_body_odd">
                    {{$order->merchantShop->shop_name}}
                </div>
                <div class="col-lg-3 col-md-2 col-sm-12 ap_table_body_odd">
                    <span class="txt_bottom_all"><i class="fas fa-user" aria-hidden="true"></i></span> {{$order->recipient_name}} <br>
                    <span class="txt_bottom_all"><i class="fas fa-phone-square" aria-hidden="true"></i></span> {{$order->recipient_phone}} <br>
                    <span class="txt_bottom_all"><i class="fas fa-location-arrow" aria-hidden="true"></i></span> {{$order->recipient_address}}
                </div>
                <div class="col-lg-1 col-md-1 col-sm-12 ap_table_body_odd">
                    @if ($order->delivery_status==7 || $order->delivery_status==11)
                        <span class="badge badge-danger">{{$order->status->name}}</span>
                        @else
                        <span class="badge badge-success">{{$order->status->name}}</span>
                    @endif

                    <span class="txt_bottom">Updated_on</span> {{Carbon\Carbon::parse($order->delivery_status_date)->format('d/m/Y g:i A')}}

                </div>
                <div class="col-lg-1 col-md-1 col-sm-12 ap_table_body_odd">
                    <span class="payment">Amount: {{$order->amount_to_collect}}</span>
                    <span class="payment">Charge: {{$order->delivery_charge +$order->cod_charge}}</span>
                    @if($order->cod_charge>0)
                        <span class="badge badge-primary">COD ADDED</span>
                    @endif
                </div>
                <div class="col-lg-1 col-md-1 col-sm-12 ap_table_body_odd">
                    <span class="badge badge-success">{{$order->payment_status}}</span>
                    @if($order->payment_status=="Paid")
                        <span class="txt_bottom">Paid_at</span> Carbon\Carbon::parse($order->payment_date)->format('d/m/Y')}}
                    @endif
                </div>
                @if ($order->delivery_status==1 || $order->delivery_status == 2)
                    <div class="col-lg-1 col-md-1 col-sm-12 ap_table_body_odd">
                        {{--                    <a href="#" class="btn btn-primary phone1"><i class='fas fa-eye'></i> </a>--}}
                        <a href="{{route('merchant.order.edit', encrypt($order->id))}}" class="btn btn-warning phone1"><i class='fas fa-wrench'></i></a>
                        <a onclick="confirm_modal('{{route('merchant.order.status', encrypt($order->id))}}','Are you sure delete Order ?');" class="btn btn-danger"><i class="far fa-trash-alt text-white"></i></a>
                    </div>
                @endif
            </div>
        @endif
    @endforeach
 @else
    <div class="row bg-white pt-2 pb-2 border-bottom">
        <div class="col text-center">
            <h6 class="text-danger">No data available</h6>
        </div>
    </div>
@endif
<div class="d-flex flex-row-reverse">
    @unless (!count($orders))
        <tr>
            <td colspan="10" align="center" id="page">
                {!!  $orders->links() !!}
            </td>
        </tr>
    @endunless
</div>
<script type="text/javascript">
    function confirm_modal(delete_url,message)
    {
        jQuery('#confirm-delete').modal('show', {backdrop: 'static'});
        document.getElementById('delete_link').setAttribute('href' , delete_url);
        $('#message').html(message);
    }
</script>
<div class="modal fade" id="confirm-delete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">

            <div class="modal-header">

                <h4 class="modal-title" id="myModalLabel"><?php echo e(__('Confirmation')); ?></h4>
            </div>

            <div class="modal-body">
                <p id="message"></p>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo e(__('Cancel')); ?></button>
                <a id="delete_link" class="btn btn-danger btn-ok"><?php echo e(__('Delete')); ?></a>
            </div>
        </div>
    </div>
</div>