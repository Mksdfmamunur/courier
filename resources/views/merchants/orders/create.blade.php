@extends('merchants.layouts.app')
@section('title')
    Merchant || Create Order
@stop
{{--@section('dashboard','active')--}}
@section('css')
    <style>
        .input-invalid{
            border-color: #dc3545 !important;
        }
        .input_show{
            background-color: transparent !important;
            border: none !important;
            text-align: right;
        }
    </style>
@stop

@section('content')
    <div class="container-fluid mt-1 pb-2">
        <form action="{{route('merchant.order.store')}}" method="post" class=" jm_create_parcel">
            @csrf
            <div class="row">
                <div class="col-sm-8 form-row delivery_charge pt-3">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-sm-12">
                                <h3 style="border-bottom: 1px solid green;">Create parcel</h3>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group jm_select_Shop">
                                    <div class="section-title mt-0">Selected Shop</div>

                                    <select class="form-control form-control-sm" id="shop_id" name="shop_id">
                                        @foreach(\App\MerchantShop::where('status',"Active")->where('merchant_id',\Illuminate\Support\Facades\Auth::id())->get() as $shop)
                                            <option value="{{$shop->id}}">{{$shop->shop_name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group jm_select_Shop">
                                    <div class="section-title mt-0">Product Type</div>
                                    <select class="form-control form-control-sm" id="order_type" name="order_type">
                                        <option value="Parcel" selected>Parcel</option>
                                        <option value="Document" disabled>Document</option>
                                    </select>
                                </div>
                            </div>

                            <div class="col-sm-8">
                                <div class="section-title mt-0 border-bottom" style="font-weight: bold">Pickup From:</div>
                                <blockquote>
                                    <div>Shop : <span id="shop_name"></span></div>
                                    <div>Phone : <span id="pickup_number"></span></div>
                                    <div>Area : <span id="pickup_area"></span></div>
                                    <div>Address : <span id="pickup_address"></span></div>
                                </blockquote>

                            </div>
                        </div><hr>

                        <h5>Customer Information</h5>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <input type="text" class="form-control" id="recipient_name" placeholder="Enter Customer Name" name="recipient_name">
                                    <div class="recipient_name invalid-feedback">Please enter customer name</div>
                                </div>
                                <div class="form-group">
                                    <input type="text" class="form-control number" id="recipient_phone" placeholder="Enter Customer Phone" name="recipient_phone">
                                    <div class="recipient_phone invalid-feedback">Please enter valid phone no</div>
                                </div>
                                <div class="form-group">
                                    <textarea class="form-control" rows="2" id="recipient_address" placeholder="Enter Customer Address" name="recipient_address" ></textarea>
                                    <div class="recipient_address invalid-feedback">Please enter address</div>
                                </div>
                                <div class="form-group">
                                    <input type="text" class="form-control" id="merchant_order_id" placeholder="Merchant Order ID" name="merchant_order_id">
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="row form-group">

                                    <div class="col-md">
                                        <select class="custom-select" id="recipient_area" name="recipient_area"  data-dependent="area">
                                            <option disabled selected>Select Recipient Area</option>
                                            @foreach (App\City::where('city_name',"Dhaka")->orderBy('city_name','asc')->get() as $city)
                                                <optgroup label="{{$city->city_name}}">
                                                    @foreach(App\CoverageArea::where('city',$city->city_name)->where('status',1)->orderBy('area','asc')->get() as $area)
                                                        <option value="{{$area->id}}">{{$area->area}}</option>
                                                    @endforeach
                                                </optgroup>
                                            @endforeach
                                        </select>
                                        <div class="recipient_area invalid-feedback">Please select recipient area</div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <select class="custom-select" id="delivery_time" name="delivery_time" >
                                        <option value="" disabled selected>Select Delivery Time</option>
                                    </select>
                                    <div class="delivery_time invalid-feedback">Please select delivery time</div>
                                </div>

                                <div class="row form-group">
                                    <div class="col-md-6">
                                        <select class="custom-select" id="order_weight" name="order_weight" >
                                            <option value="1" selected>1Kg</option>
                                            <option value="2">2Kg</option>
                                            <option value="3">3Kg</option>
                                            <option value="4">4Kg</option>
                                        </select>
                                    </div>
                                    <div class="col-md-6">
                                        <input type="text" class="form-control currency" id="amount_to_collect" value="" placeholder="Cash Collection Amount" name="amount_to_collect">
                                        <div class="amount_to_collect invalid-feedback">Please enter cash collection Amount</div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <textarea class="form-control" rows="2" id="merchant_instruction" placeholder="Instruction" name="merchant_instruction"></textarea>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
                <div class="col-sm-4 pt-3">
                    <h2 class="jm_bangla" style="border-bottom: 1px solid green;">ডেলিভারি চার্জের বিস্তারিত</h2>
                    <div class="container">
                        <div class="form-group row mb-0">
                            <label for="cash_collection" class="col-6 col-form-label">Cash Collection</label>
                            <div class="col-6">
                                <input type="text" readonly class="form-control-plaintext input_show" id="cash_collection" name="cash_collection" value="0">
                            </div>
                        </div>
                        <div class="form-group row mb-0">
                            <label for="delivery_charge" class="col-6 col-form-label">Delivery Charge</label>
                            <div class="col-6">
                                <input type="text" readonly class="form-control-plaintext input_show" id="delivery_charge" name="delivery_charge" value="0">
                            </div>
                        </div>
                        <div class="form-group row mb-0">
                            <label for="cod_charge" class="col-6 col-form-label">COD Charge</label>
                            <div class="col-6">
                                <input type="text" readonly class="form-control-plaintext input_show" id="cod_charge" name="cod_charge" value="0">
                            </div>
                        </div>
                        <hr style="margin-top: 0; margin-bottom: 0;">
                        <div class="form-group row mb-0">
                            <label for="paid_out" class="col-6 col-form-label">Total Payable Amount</label>
                            <div class="col-6">
                                <input type="text" readonly class="form-control-plaintext input_show" id="paid_out" name="paid_out" value="0">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="text-center mt-2">
                <button class="btn btn-pr my-2 my-sm-0" id="create_order" type="submit">CREATE ORDER</button>
                <button type="submit" class="btn btn-pr" disabled id="btn_fetch_disable" style="display: none;">Creating..<i class="fas fa-spinner fa-spin" style="color: white"></i></button>

            </div>
        </form>
    </div>
@stop
@section('script')
    @toastr_render
    <script>

        $(document).ready(function () {
            $(".custom-select").select2({
                allowClear: false
            });
            shop();
        });

        $('#recipient_area').on('change',function () {
            var recipient_area = $('#recipient_area').val();
            if(recipient_area==null){
                $("#recipient_area").addClass('input-invalid');
                $('.recipient_area').show();
            }
            else{
                $("#recipient_area").removeClass('input-invalid');
                $('.recipient_area').hide();
            }
            fetchDelivery();
            calculation();
        });
        $('#order_weight').on('change',function () {
            calculation();
        });
        $('#delivery_time').on('change',function () {
            var delivery_time = $('#delivery_time').val();
            if(delivery_time==null){
                $("#delivery_time").addClass('input-invalid');
                $('.delivery_time').show();
            }
            else{
                $("#delivery_time").removeClass('input-invalid');
                $('.delivery_time').hide();
            }
            calculation();
        });
        $('#amount_to_collect').on('keyup',function () {
            var amount_to_collect = $('#amount_to_collect').val();
            if(!amount_to_collect.replace(/\s/g, '').length ){
                $("#amount_to_collect").addClass('input-invalid');
                $('.amount_to_collect').show();
            }
            else{
                $("#amount_to_collect").removeClass('input-invalid');
                $('.amount_to_collect').hide();
            }
            calculation();

        });
        $('#recipient_name').on('keyup',function () {
            var recipient_name = $('#recipient_name').val();
            if(!recipient_name.replace(/\s/g, '').length ){
                $("#recipient_name").addClass('input-invalid');
                $('.recipient_name').show();
            }
            else{
                $("#recipient_name").removeClass('input-invalid');
                $('.recipient_name').hide();
            }
        });
        $('#recipient_phone').on('keyup',function () {
            var recipient_phone = $('#recipient_phone').val();
            if(!recipient_phone.replace(/\s/g, '').length ){
                $("#recipient_phone").addClass('input-invalid');
                $('.recipient_phone').show();
            }
            else{
                $("#recipient_phone").removeClass('input-invalid');
                $('.recipient_phone').hide();
            }

        });
        $('#recipient_address').on('keyup',function () {
            var recipient_address = $('#recipient_address').val();
            if(!recipient_address.replace(/\s/g, '').length ){
                $("#recipient_address").addClass('input-invalid');
                $('.recipient_address').show();
            }
            else{
                $("#recipient_address").removeClass('input-invalid');
                $('.recipient_address').hide();
            }

        });
        $(document).ready(function () {
            $(".currency").inputFilter(function(value) {
                return /^[+]?\d*[.]?\d{0,2}$/.test(value);
            });
            $("#recipient_phone").on('keyup',function () {
                var recipient_phone = $('#recipient_phone').val();
                if(!(mobile_pattern.test(recipient_phone))) {
                    $("#recipient_phone").addClass('input-invalid');
                    $('.recipient_phone').show();
                }
                else {
                    $("#recipient_phone").removeClass('input-invalid');
                    $('.recipient_phone').hide();
                }
            });
        });
        $('#shop_id').on('change',function () {
            shop();
        });
        $("#create_order").on('click',function () {
            var recipient_name = $('#recipient_name').val();
            var recipient_phone = $('#recipient_phone').val();
            var recipient_address = $('#recipient_address').val();
            var recipient_area = $('#recipient_area').val();
            var amount_to_collect = $('#amount_to_collect').val();
            var delivery_time = $('#delivery_time').val();
            if (!recipient_name.replace(/\s/g, '').length ||  !(mobile_pattern.test(recipient_phone))|| !recipient_phone.replace(/\s/g, '').length ||!recipient_address.replace(/\s/g, '').length||recipient_area==null || delivery_time==null|| !amount_to_collect.replace(/\s/g, '').length){
                if(!recipient_name.replace(/\s/g, '').length){
                    $("#recipient_name").addClass('input-invalid');
                    $('.recipient_name').show();
                }
                if(!recipient_phone.replace(/\s/g, '').length){
                    $("#recipient_phone").addClass('input-invalid');
                    $('.recipient_phone').show();
                }
                if (recipient_phone.replace(/\s/g, '').length && !(mobile_pattern.test(recipient_phone))) {
                    $("#recipient_phone").addClass('input-invalid');
                    $('.recipient_phone').show();
                }
                if(!recipient_address.replace(/\s/g, '').length){
                    $("#recipient_address").addClass('input-invalid');
                    $('.recipient_address').show();
                }
                if(!amount_to_collect.replace(/\s/g, '').length ){
                    $("#amount_to_collect").addClass('input-invalid');
                    $('.amount_to_collect').show();
                }
                if (recipient_area==null){
                    $("#recipient_area").addClass('input-invalid');
                    $('.recipient_area').show();
                }
                if (delivery_time==null){
                    $("#delivery_time").addClass('input-invalid');
                    $('.delivery_time').show();
                }
                toastr.error('Please fix the given errors.');
                return false;
            }

            else{
                $("#create_order").hide();
                $('#btn_fetch_disable').show();
                return true;
            }
        });


        function shop(){
            if ($('#shop_id').val() !=null)
            {
                var shop_id = $('#shop_id').val();
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                    }
                });
                $.ajax({
                    url: "{{route('merchant.order.search-shop')}}",
                    method: 'POST',
                    data: {
                        shop_id:shop_id
                    },
                    success:function(data)
                    {
                        $('#shop_name').html(data.shop.shop_name);
                        $('#pickup_number').html(data.shop.pickup_number);
                        $('#pickup_area').html(data.area.area_name +", "+data.city.city_name);
                        $('#pickup_address').html(data.shop.pickup_address);
                    }

                });
            }
        }
        function fetchDelivery() {
            var recipient_area = $("#recipient_area").val();
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                }
            });
            $.ajax({
                url: "{{route('merchant.order.fetch-delivery')}}",
                method: 'POST',
                data: {
                    recipient_area:recipient_area
                },
                success:function(data)
                {
                    $('#delivery_time').html(data);
                }

            });
        }
        function calculation() {
            var shop_id = $('#shop_id').val();
            var recipient_area = $("#recipient_area").val();
            var order_weight = $("#order_weight").val();
            var  delivery_time= $("#delivery_time").val();
            var  amount_to_collect= $("#amount_to_collect").val();
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                }
            });
            $.ajax({
                url: "{{route('merchant.order.delivery-charge-plan')}}",
                method: 'POST',
                data: {
                    shop_id:shop_id,
                    recipient_area:recipient_area,
                    order_weight:order_weight,
                    amount_to_collect:amount_to_collect,
                    delivery_time:delivery_time
                },
                success:function(data)
                {
                    if (data.recipientArea.division == "Inside Dhaka")
                    {
                        if (data.deliveryTime=="idu"){
                            var deliveryCharge =parseFloat(data.deliveryPlan.idu_charge);
                            if (data.orderWeight==1){
                                $('#delivery_charge').val(deliveryCharge);
                            }
                            else{

                                var cpw = parseFloat(data.deliveryPlan.cpw);
                                var deliveryCharge =(deliveryCharge+(cpw*(data.orderWeight-1)));
                                $('#delivery_charge').val(parseInt(deliveryCharge));
                            }
                            var cod = data.amountToCollect*(data.deliveryPlan.idu_cod/100);
                            $('#cod_charge').val(cod);
                            if (data.amountToCollect==null) {
                                $('#cash_collection').val(0);
                            }
                            else{
                                $('#cash_collection').val(data.amountToCollect);
                            }
                            $('#paid_out').val(data.amountToCollect-(deliveryCharge+cod));
                        }
                        else
                        {
                            var deliveryCharge =parseFloat(data.deliveryPlan.idr_charge);
                            if (data.orderWeight==1){
                                $('#delivery_charge').val(deliveryCharge);
                            }
                            else{
                                var cpw = parseFloat(data.deliveryPlan.cpw);
                                var deliveryCharge =(deliveryCharge+(cpw*(data.orderWeight-1)));
                                $('#delivery_charge').val(deliveryCharge);
                            }
                            var cod = data.amountToCollect*(data.deliveryPlan.idr_cod/100);
                            $('#cod_charge').val(cod);
                            if (data.amountToCollect==null) {
                                $('#cash_collection').val(0);
                            }
                            else{
                                $('#cash_collection').val(data.amountToCollect);
                            }
                            $('#paid_out').val(data.amountToCollect-(deliveryCharge+cod));
                        }
                    }
                    else if(data.recipientArea.division =="Dhaka Sub")
                    {
                        var deliveryCharge =parseFloat(data.deliveryPlan.ds_charge);
                        if (data.orderWeight==1){
                            $('#delivery_charge').val(deliveryCharge);
                        }
                        else{
                            var cpw = parseFloat(data.deliveryPlan.cpw);
                            var deliveryCharge =(deliveryCharge+(cpw*(data.orderWeight-1)));
                            $('#delivery_charge').val(deliveryCharge);
                        }
                        var cod = data.amountToCollect*(data.deliveryPlan.ds_cod/100);
                        $('#cod_charge').val(cod);
                        if (data.amountToCollect==null) {
                            $('#cash_collection').val(0);
                        }
                        else{
                            $('#cash_collection').val(data.amountToCollect);
                        }
                        $('#paid_out').val(data.amountToCollect-(deliveryCharge+cod));
                    }
                    else{
                        var deliveryCharge =parseFloat(data.deliveryPlan.od_charge);
                        if (data.orderWeight==1){
                            $('#delivery_charge').val(deliveryCharge);
                        }
                        else{
                            var cpw = parseFloat(data.deliveryPlan.cpw);
                            var deliveryCharge =(deliveryCharge+(cpw*(data.orderWeight-1)));
                            $('#delivery_charge').val(deliveryCharge);
                        }
                        var cod = data.amountToCollect*(data.deliveryPlan.od_cod/100);
                        $('#cod_charge').val(cod);
                        if (data.amountToCollect==null) {
                            $('#cash_collection').val(0);
                        }
                        else{
                            $('#cash_collection').val(data.amountToCollect);
                        }
                        $('#paid_out').val(data.amountToCollect-(deliveryCharge+cod));
                    }
                }

            });
        }


    </script>

@stop