@extends('merchants.layouts.app')
@section('title')
    Merchant || Invoice
@stop
@section('invoices','active')
@section('css')

@stop

@section('content')

    <div class="container-fluid mt-4">
{{--        <div class="row mb-4">--}}
{{--            <div class="col-md-4">--}}
{{--                <input type="text" class="form-control">--}}
{{--            </div>--}}
{{--        </div>--}}
        <div class="row col_name_show mt-4 pt-2 pb-2 border">
            <div class="col-lg-2 col-md-2 col-sm-12 ap_table">
                Invoice ID
            </div>
            <div class="col-lg-2 col-md-2 col-sm-12 ap_table">
                Invoice Date
            </div>
            <div class="col-lg-2 col-md-2 col-sm-12 ap_table">
                Collected
            </div>
            <div class="col-lg-1 col-md-1 col-sm-12  ap_table">
                Fee
            </div>
            <div class="col-lg-2 col-md-2 col-sm-12  ap_table">
                Receivable Amount
            </div>
            <div class="col-lg-1 col-md-1 col-sm-12  ap_table">
                Status
            </div>
{{--            <div class="col-lg-2 col-md-2 col-sm-12 ap_table">--}}
{{--                download--}}
{{--            </div>--}}
        </div>
        @foreach (\App\MerchantPayment::where('merchant_id',\Illuminate\Support\Facades\Auth::id())->get() as $merchantPayment)

        <div class="row ap_table_body">
            <div class="col-lg-2 col-md-2 col-sm-12 ap_table_body_odd"><span class="col_name_show">Invoice ID:</span>
                {{$merchantPayment->invoice_id}}
            </div>
            <div class="col-lg-2 col-md-2 col-sm-12 ap_table_body_odd"><span class="col_name_show">Invoice Date:</span>
                {{$merchantPayment->created_at}}
            </div>
            <div class="col-lg-2 col-md-2 col-sm-12 ap_table_body_odd"><span class="col_name_show">Collected:</span>
                {{$merchantPayment->collected_amount}}
            </div>
            <div class="col-lg-1 col-md-1 col-sm-12 ap_table_body_odd"><span class="col_name_show">Fee:</span>
                {{$merchantPayment->total_charge}}
            </div>
            <div class="col-lg-2 col-md21 col-sm-12 ap_table_body_odd"><span class="col_name_show">Receivable Amount:</span>
                {{$merchantPayment->total_paid_amount}}
            </div>
            <div class="col-lg-1 col-md-1 col-sm-12 ap_table_body_odd"><span class="col_name_show">Status:</span>
                {{$merchantPayment->status}}
            </div>
{{--            <div class="col-lg-2 col-md-2 col-sm-12 ap_table_body_odd">--}}
{{--                <a  class="btn btn-warning phone1"><i class='fas fa-download'></i></a>--}}
{{--                <a  class="btn btn-warning phone1" data-toggle="modal" data-target="#myModal"><i class='fas fa-eye'></i></a>--}}
{{--                <!-- Modal -->--}}
{{--                <div class="modal fade" id="myModal" role="dialog">--}}
{{--                    <div class="modal-dialog modal-xl">--}}
{{--                        <!-- Modal content-->--}}
{{--                        <div class="modal-content">--}}
{{--                            <div class="modal-header">--}}
{{--                                <h4 class="modal-title">Order list of Invoice - 090920JN3UWA60</h4>--}}
{{--                                <button type="button" class="close" data-dismiss="modal">&times;</button>--}}
{{--                            </div>--}}
{{--                            <div class="modal-body">--}}
{{--                                <div class="container">--}}
{{--                                    <div class="row col_name_show">--}}
{{--                                        <div class="col-lg-1 col-md-1 col-sm-12  ap_table">--}}
{{--                                            Created At--}}
{{--                                        </div>--}}
{{--                                        <div class="col-lg-2 col-md-2 col-sm-12  ap_table">--}}
{{--                                            Track.ID--}}
{{--                                        </div>--}}
{{--                                        <div class="col-lg-2 col-md-2 col-sm-12 ap_table">--}}
{{--                                            Merchant Order Id--}}
{{--                                        </div>--}}
{{--                                        <div class="col-lg-1 col-md-1 col-sm-12  ap_table">--}}
{{--                                            Recipient--}}
{{--                                        </div>--}}
{{--                                        <div class="col-lg-1 col-md-1 col-sm-12 ap_table">--}}
{{--                                            Recipient Phone--}}
{{--                                        </div>--}}
{{--                                        <div class="col-lg-1 col-md-1 col-sm-12  ap_table">--}}
{{--                                            Collected Amount--}}
{{--                                        </div>--}}
{{--                                        <div class="col-lg-1 col-md-1 col-sm-12  ap_table">--}}
{{--                                            COD--}}
{{--                                        </div>--}}
{{--                                        <div class="col-lg-1 col-md-1 col-sm-12 ap_table">--}}
{{--                                            Delivery charge--}}
{{--                                        </div>--}}

{{--                                        <div class="col-lg-1 col-md-1 col-sm-12 ap_table">--}}
{{--                                            Total charge--}}
{{--                                        </div>--}}

{{--                                        <div class="col-lg-1 col-md-1 col-sm-12 ap_table">--}}
{{--                                            Paid out--}}
{{--                                        </div>--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                                    <div class="row ap_table_body">--}}
{{--                                        <div class="col-lg-1 col-md-1 col-sm-12 ap_table_body_odd"><span class="col_name_show">Invoice ID:</span>--}}
{{--                                            15/05/2020--}}
{{--                                        </div>--}}
{{--                                        <div class="col-lg-2 col-md-2 col-sm-12 ap_table_body_odd"><span class="col_name_show">Invoice ID:</span>--}}
{{--                                            130920JN85DR7N--}}
{{--                                        </div>--}}
{{--                                        <div class="col-lg-2 col-md-2 col-sm-12 ap_table_body_odd"><span class="col_name_show">Invoice Date:</span>--}}
{{--                                            13-09-2020--}}
{{--                                        </div>--}}
{{--                                        <div class="col-lg-1 col-md-1 col-sm-12 ap_table_body_odd"><span class="col_name_show">Collected:</span>--}}
{{--                                            20000--}}
{{--                                        </div>--}}
{{--                                        <div class="col-lg-1 col-md-1 col-sm-12 ap_table_body_odd"><span class="col_name_show">Fee:</span>--}}
{{--                                            5000--}}
{{--                                        </div>--}}
{{--                                        <div class="col-lg-1 col-md-1 col-sm-12 ap_table_body_odd"><span class="col_name_show">Receivable Amount:</span>--}}
{{--                                            15000--}}
{{--                                        </div>--}}
{{--                                        <div class="col-lg-1 col-md-1 col-sm-12 ap_table_body_odd"><span class="col_name_show">Status:</span>--}}
{{--                                            Paid--}}
{{--                                        </div>--}}
{{--                                        <div class="col-lg-1 col-md-1 col-sm-12 ap_table_body_odd"><span class="col_name_show">Status:</span>--}}
{{--                                            Paid--}}
{{--                                        </div>--}}
{{--                                        <div class="col-lg-1 col-md-1 col-sm-12 ap_table_body_odd"><span class="col_name_show">Status:</span>--}}
{{--                                            Paid--}}
{{--                                        </div>--}}
{{--                                        <div class="col-lg-1 col-md-1 col-sm-12 ap_table_body_odd"><span class="col_name_show">Status:</span>--}}
{{--                                            Paid--}}
{{--                                        </div>--}}
{{--                                    </div>--}}

{{--                            </div>--}}
{{--                        </div>--}}
{{--                        <div class="modal-footer">--}}
{{--                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--            </div>--}}
        </div>
        @endforeach
@stop
@section('script')
    @toastr_render
@stop