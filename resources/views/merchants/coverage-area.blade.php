@extends('merchants.layouts.app')
@section('title')
    Merchant || Coverage Area
@stop
{{--@section('dashboard','active')--}}
@section('css')

@stop

@section('content')
 <div class="row">
    <div class="col">
        <div class="card">
            <div class="card-header">
                Coverage Area List
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-striped" id="dataTable" width="100%" >
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Location</th>
                            <th>City</th>
                            <th>Area</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach(\App\CoverageArea::where('status',1)->get() as $key=> $coverageArea)
                            <tr>
                                <td> {{$key+1}}</td>
                                <td>{{$coverageArea->division}}</td>
                                <td>{{$coverageArea->city}}</td>
                                <td>{{$coverageArea->area}}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
 </div>
@stop
@section('script')
    @toastr_render
    <script>
        $("#dataTable").dataTable();
    </script>
@stop