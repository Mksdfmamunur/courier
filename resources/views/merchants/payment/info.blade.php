@extends('merchants.layouts.app')
@section('title')
    Merchant || My Payment Information
@stop
{{--@section('dashboard','active')--}}
@section('css')

@stop

@section('content')
<div class="container-fluid mt-4">
    <div class="row">
        <div class="offset-md-3 col-md-6">
            <div class="card">
                <div class="card-header">
                    <h5>Manage your Payment Method</h5>
                </div>
                <div class="card-body">
                    <?php
                        $paymentInfo = \App\MerchantPaymentInformation::where('merchant_id',\Illuminate\Support\Facades\Auth::id())->first();
                    ?>
                        @if ($paymentInfo->payment_type == "Mobile")
                            <h5>Wallet :  <b>{{$paymentInfo->mobile_wallet}}</b></h5>
                            <h5>Wallet Number :  <b>{{$paymentInfo->wallet_number}}</b></h5>
                            {{--                            <a href="{{route("merchant.payment-info.edit",encrypt($paymentInfo->id))}}" class="btn btn-warning text-white"><i class="fas fa-wrench"></i> Edit</a>--}}
                        @endif
                        @if ($paymentInfo->payment_type == "Cash")
                            <h5>Wallet :  <b>{{$paymentInfo->payment_type}}</b></h5>
                            {{--                            <a href="{{route("merchant.payment-info.edit",encrypt($paymentInfo->id))}}" class="btn btn-warning text-white"><i class="fas fa-wrench"></i> Edit</a>--}}
                        @endif
                        @if ($paymentInfo->payment_type == "Bank")
                            <h5>Account Holder Name :  <b>{{$paymentInfo->account_holder_name}}</b></h5>
                            <h5>Account Type :  <b>{{$paymentInfo->account_type}}</b></h5>
                            <h5>Account Number :  <b>{{$paymentInfo->account_number}}</b></h5>
                            <h5>Bank Name :  <b>{{$paymentInfo->bank_name}}</b></h5>
                            <h5>Branch Name :  <b>{{$paymentInfo->branch_name}}</b></h5>
                            <h5>Routing Number :  <b>{{$paymentInfo->routing_number}}</b></h5>
                            {{--                            <a href="{{route("merchant.payment-info.edit",encrypt($paymentInfo->id))}}" class="btn btn-warning text-white"><i class="fas fa-wrench"></i> Edit</a>--}}
                        @endif
                </div>
            </div>
        </div>
    </div>
</div>
@stop
@section('script')
    @toastr_render
@stop