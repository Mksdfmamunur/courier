@extends('merchants.auth.master')
@section('title')
    Merchant || Send reset link
@stop
@section('css')

@stop
@section('content')
    <div class="container">
        <div class="row">
            <div class="offset-sm-4 col-sm-4">
                <div class="card">
                    <div class="card-body">
                        <div class="text-center">
                            <a  href="http://www.metroexpress.com.bd"> <img src="{{asset('public/icon/logo.png')}}" height="100" class="rounded mx-auto d-block" alt=""></a>
                            <h2 class="mt-3">Check your Email!</h2>
                        </div>
                        <div>
                            <p>We have sent an email to {{$merchant->email}} . Click the reset button in the email to reset your password.</p>

                            <p>Need Help? Call 09639-103314 or Feel free to write to <a href = "mailto: info@metroexpress.com.bd">info@metroexpress.com.bd</a>, for any queries and suggession.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
@section('script')
    @toastr_render
    <script>
        // $("#btn_fetch").on('click',function () {
        //     $("#btn_fetch").hide();
        //     $("#btn_fetch_disable").show();
        //     return true;
        // })

    </script>

@stop