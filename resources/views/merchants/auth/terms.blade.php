<div class="modal fade" id="termsModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Terms & Conditions</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div>
                    <h5>Scope of Work</h5>
                    <ul style="list-style-type:square">
                        <li>
                            Delivery service of Metro Express shall extent to Home Delivery of the
                            Products or Cash on Delivery, return of the products and Reverse pickup.
                        </li>
                        <li>
                            Metro Express will provide interface for tracking delivery information
                            so that adequate information is available to the Merchant to satisfy Customers’
                            queries in respect of Delivery of the Products.
                        </li>
                        <li>
                            In the course of the Delivery Services provided by Metro Express,
                            if there is any loss or theft of the Products solely on account of gross
                            negligence and willful misconduct of the Delivery Personnel any damage
                            or loss to the Products shall be borne by Metro Express, provided that
                            the Merchant shall ensure that the packaging of the Products is safe,
                            proper and adequate to withstand the normal transportation and environmental
                            hazards. Metro Express shall be allowed to inspect the packaging and make
                            reasonable suggestions on improvements accordingly and refuse to carry any
                            package which it doesn’t find transit worthy.
                        </li>
                    </ul>
                </div>
                <div>
                    <h5>Deliverable Materials</h5>
                    <ul style="list-style-type:square">
                        <li>
                            Metro Express won’t deliver any product which is illegal or banned by the law
                            of Bangladesh Govt. Any kind of vulnerable chemicals/things along with fragile
                            items or any product without proper packaging won’t be carried as a package by
                            Metro Express. Packages must not contain goods which might endanger human or
                            animal life or any means of transportation, or which might otherwise taint or
                            damage other goods being transported by Metro Express, or the carriage, export
                            or import of which is prohibited by applicable law.
                        </li>
                        <li>
                            If any damage happens to the parcel mentioned above, Metro Express will not be liable for it.
                        </li>
                        <li>
                            Metro Express will not receive any unsealed electronics products which were originally sealed
                            by the manufacturer such as mobile phone or camera.
                        </li>
                        <li>Metro Express reserves the right to revise and update all Merchant Service Fees. These
                            changes shall be notified to the Merchant in writing.
                        </li>
                    </ul>
                </div>
                <div>
                    <h5>Merchant Obligations</h5>
                    <ul style="list-style-type:square">
                        <li>
                            The Merchant shall not book / handover or allows to be handed over any Product which is
                            banned, restricted, illegal, prohibited, stolen, infringing of any third party rights,
                            hazardous or dangerous or in breach of any tax laws.
                        </li>
                        <li>
                            The Merchant shall ensure that the packaging of the Products is safe, proper and adequate
                            to withstand the normal transportation and environmental hazards and in compliance with
                            applicable laws.
                        </li>
                        <li>
                            Merchant should take back the package that gets returned by the customer and return fee
                            will be applicable.
                        </li>
                        <li>
                            Merchant should provide all the required information regarding customer on the body of
                            the package to Metro Express in order to serve customers on time.
                        </li>
                        <li>
                            If any packages get rejected by customer and returned back to the merchant via Metro Express,
                            Merchant will have to bear the delivery charge.
                        </li>
                    </ul>
                </div>
                <div>
                    <h5>Payment and refund to merchants</h5>
                    <ul style="list-style-type:square">
                        <li>
                            Metro Express shall issue invoice to the Merchant at every payment cycle for the Delivery Services that have been rendered at weekly with the terms of this Agreement. Metro Express shall collect the cash from customers on behalf of the merchant and from the collected COD amount Metro Express will deduct its shipping charge. The rest of the amount will be deposited or disbursed to the merchant’s designated bank account. In the event of Non COD scenario, Metro Express shall issue invoice to the Merchant at every week and Merchant will pay the service charges within 3 days after they receive the invoice. For safe and secured money transaction it is recommended to have Bank transaction. For Cash on Delivery, 1% COD Charge will be applicable.</li>
                        <li>
                            For all transaction on Cash on Delivery (COD), the collected cash will be held by Metro Express as an agent of Merchant and in trust for the Merchant. Such cash collected by Metro Express from the Customers will be transferred to merchant’s designated bank account within three (3) Business Days after the collection of the cash from customer upon delivery. The shipping charge will be deducted from COD.</li>
                        <li>
                            If for any reason the Customer refuses to receive the product which is already picked up from Merchant’s place (Merchant requested to pick up the product and Metro Express already collected it from that place, and before going for the delivery, customer requested to the Merchant to cancel the order) and already the product is at the Metro Express’s warehouse, Merchant must pay the delivery charge for that undelivered product. Metro Express will return the product to the Merchant by next business days for inside Dhaka and if the product has already been sent to outside Dhaka, the product will be return within 10 business days. If the product for outside Dhaka delivery is in inside Dhaka at Metro Express’s warehouse, it will be return by the next business day.</li>
                        <li>
                            Every package of merchant will be under insurance compensation on conditional basis.</li>
                    </ul>
                </div>
                <div>
                    <h5>Completion Criteria</h5>
                    <ul style="list-style-type:square">
                        <li>Contractor accomplishes the Contractor activities described within this SOW, including delivery to Client of the materials listed in the Section entitled “Deliverable Materials,” and Client accepts such activities and materials without unreasonable objections. No response from Client within 2-business days of deliverables being delivered by Contractor is deemed acceptance.</li>
                        <li>Contractor and/or Client has the right to cancel services or deliverables not yet provided with 30 business days advance written notice to the other party.</li>
                    </ul>
                </div>
                <div>
                    <h5>General Terms and Conditions</h5>
                    <ul style="list-style-type:square">
                        <li>Metro Express will allocate and send Delivery Personnel to the location(s) mutually agreed with the Merchant within 24 hours (business days) after Metro Express receives the pickup request.</li>
                        <li>Delivery Personnel shall give the best effort to pick up and load the Products carefully to prevent the Products from being pressed, wrinkled and/or damaged. If any situation arise which is not in human control or any of the parties control (like natural calamity, political turmoil etc) then Metro Express won’t be liable for that.</li>
                        <li>Every delivery rider should be well trained and dressed up properly ensuring decent behavior with the customer and merchant.</li>
                        <li>If Metro Express finds the customer, location of the customer, timing of the delivery and the product is not secured then Metro Express can reject the delivery/Pickup request for greater safety and inform the merchant.</li>
                        <li>Metro Express won’t be responsible for any rejection or refusal of any package by the customer.</li>
                        <li>If any pickup request is put after the mentioned regular pickup hour, then the package will be picked up on the next day.</li>
                        <li>Before receiving the return parcels, the merchants should check them properly before receiving it. Metro Express will not be liable for any issues raised after receiving the parcels.</li>
                        <li>Customer should be encouraged to check the parcels before receiving it in front of our delivery agent, until there is no clause from the merchant. Metro Express will not be liable for any issues raised after receiving the parcels.</li>
                        <li>Termination: This agreement may be terminated by either party by providing written notice for at least 30 (thirty) calendar days in advance.</li>
                    </ul>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" id="check" class="btn btn-primary">I AGREE</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>