@extends('merchants.auth.master')
@section('title')
    Merchant ||Account Verification
@stop
@section('css')

@stop
@section('content')
    <div class="container">
        <div class="row">
            <div class="offset-sm-2 col-sm-8">
                <div class="card">
                    <div class="card-body">
                        <div class="text-center">
                            <a  href="http://www.metroexpress.com.bd"> <img src="{{asset('public/icon/logo.png')}}" height="100" class="rounded mx-auto d-block" alt=""></a>
                            <h1 class="mt-2"><i class="fas fa-check-circle text-success"></i></h1>
                            <h3>Account Successfully Created!</h3>
                            <p>Your account has been created successfully. Check your email inbox/spam for details</p>

                        </div>
                        <div>
                            <p>To start Creating Order you will have to wait for Verification Process. It will take maximum 2 business days to get the account verified. When the verification process is completed, an email will be sent to you. You will find the link to your merchant web platform and login credentials in that email.</p>
                            <p>Need Help? Call 09639-103314 or Feel free to write to <a href = "mailto: info@metroexpress.com.bd">info@metroexpress.com.bd</a>, for any queries and suggession.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
@section('script')
    @toastr_render
    <script>
        // $("#btn_fetch").on('click',function () {
        //     $("#btn_fetch").hide();
        //     $("#btn_fetch_disable").show();
        //     return true;
        // })

    </script>

@stop
