@extends('merchants.auth.master')
@section('title')
    Merchant || Registration
@stop
@section('css')
    <style>
        .info_border{
            border: 1px solid #0F996D;
            width: 100%;
            margin: 0;
        }

        .modal-dialog {
            max-width: 100% !important;
            margin: 0!important;
        }

    </style>
@stop
@section('content')
    <div class="container">
        <div class="row">
            <div class="offset-sm-2 col-sm-8">
                <div class="card">
                    <div class="card-body">
                        <div class="text-center">
                            <img src="{{asset('public/icon/logo.png')}}" height="100" class="rounded mx-auto d-block" alt="">
                            <h3>Merchant Registration</h3>
                        </div>
                        <form action="{{route('merchant.registration.submit')}}" method="post" enctype="multipart/form-data">
                            @csrf
                            <div class="row">
                                <div class="col mt-2">
                                    <h6>Account Information</h6>
                                    <hr class="info_border">
                                </div>
                            </div>
                            <div class="row mt-2">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="first_name">First Name <span class="text-danger">*</span></label>
                                        <input type="text" class="form-control" id="first_name" name="first_name"  placeholder="Enter first name" >
                                        <div class="first-name invalid-feedback">Please enter first name</div>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="last_name">Last Name <span class="text-danger">*</span></label>
                                        <input type="text" class="form-control" id="last_name" name="last_name"  placeholder="Enter last name" >
                                        <div class="last_name invalid-feedback">Please enter last name</div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="email">Email <span class="text-danger">*</span></label>
                                        <input type="email" class="form-control" id="email" name="email"  placeholder="Enter email address" >
                                        <div class="invalid-feedback email">Please enter valid email address</div>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="mobile_number">Mobile Number <span class="text-danger">*</span></label>
                                        <input type="text" class="form-control number" id="mobile_number" name="mobile_number"  placeholder="Enter mobile number" >
                                        <div class="invalid-feedback mobile_number">Please enter phone number</div>
                                        <div class="invalid-feedback-mobile text-danger" style="display: none">Please enter valid phone number</div>
                                        <div>Only english character is allowed.</div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col mt-2">
                                    <h6>Shop Information</h6>
                                    <hr class="info_border">
                                </div>
                            </div>
                            <div class="row mt-2">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="shop_name">Shop Name <span class="text-danger">*</span></label>
                                        <input type="text" class="form-control" id="shop_name" name="shop_name"  placeholder="Enter shop name" >
                                        <div class="shop_name invalid-feedback">Please enter shop name</div>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="shop_link">Website / Page link <span class="text-danger">*</span></label>
                                        <input type="text" class="form-control" id="shop_link" name="shop_link"  placeholder="Enter website or shop link" >
                                        <div class="shop_link invalid-feedback">Please enter shop link</div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="pickup_number">Shop Pickup Number <span class="text-danger">*</span></label>
                                        <input type="text" class="form-control number" id="pickup_number" name="pickup_number"  placeholder="Enter shop pickup number" >
                                        <div class="pickup_number invalid-feedback">Please enter valid pickup number</div>
                                        <div>Only english character is allowed.</div>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="pickup_area">Pickup Area <span class="text-danger">*</span></label>
                                        <select class="form-control pickup_area_select" id="pickup_area" name="pickup_area" >
                                            <option value="" selected disabled>Select pickup area</option>
                                            @foreach (App\City::where('city_name',"Dhaka")->orderBy('city_name','asc')->get() as $city)
                                                <optgroup label="{{$city->city_name}}">
                                                    @foreach(App\Area::where('city_id',$city->id)->orderBy('area_name','asc')->get() as $area)
                                                    <option value="{{$area->id}}">{{$area->area_name}}</option>
                                                    @endforeach
                                                </optgroup>
                                            @endforeach
                                        </select>
                                        <div class="pickup_area invalid-feedback">Please select pickup area</div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col">
                                    <div class="form-group">
                                        <label for="pickup_address">PickUp Address <span class="text-danger">*</span></label>
                                        <textarea class="form-control" name="pickup_address" id="pickup_address" rows="2"></textarea>
                                        <div class="pickup_address invalid-feedback">Please enter pickup address</div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col mt-2">
                                    <h6>Payment Information</h6>
                                    <hr class="info_border">
                                </div>
                            </div>
                            <div class="row mt-2">
                                <div class="col">
                                    <div class="form-group">
                                        <label>Payment Type</label>
                                        <div class="">
                                            <div class="form-check-inline">
                                                <label class="form-check-label">
                                                    <input type="radio" class="form-check-input" name="payment_type" value="Mobile" checked>Mobile (MFS)
                                                </label>
                                            </div>
                                            <div class="form-check-inline">
                                                <label class="form-check-label">
                                                    <input type="radio" class="form-check-input" name="payment_type"  value="Bank" >Bank
                                                </label>
                                            </div>
                                            <div class="form-check-inline">
                                                <label class="form-check-label">
                                                    <input type="radio" class="form-check-input" name="payment_type"  value="Cash" >Cash
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row mobile">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="mobile_wallet">Select an wallet provider <span class="text-danger">*</span></label>
                                        <select class="form-control" id="mobile_wallet" name="mobile_wallet" >
                                            <option value="Bkash" selected>Bkash</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="wallet_number">Mobile Wallet Number <span class="text-danger">*</span></label>
                                        <input type="text" class="form-control number" id="wallet_number" name="wallet_number"  placeholder="Enter wallet number" >
                                        <div class="wallet_number invalid-feedback" >Please enter valid mobile wallet number</div>
                                        <div>Only english character is allowed.</div>
                                    </div>
                                </div>
                            </div>
                            <div class="row bank" style="display: none">
                                <div class="col">
                                    <div class="form-group">
                                        <label for="bank_name">Select Bank <span class="text-danger">*</span></label>
                                        <select class="form-control" id="bank_name" name="bank_name" >
                                            @foreach (\App\Bank::orderBy('name','asc')->get() as $bank)
                                                <option value="{{$bank->name}}">{{$bank->name}}</option>
                                            @endforeach
                                        </select>
                                        <div class="bank_name invalid-feedback" >Select bank name</div>
                                    </div>
                                </div>
                                <div class="col">
                                    <div class="form-group">
                                        <label for="branch_name">Branch Name <span class="text-danger">*</span></label>
                                        <input type="text" class="form-control" id="branch_name" name="branch_name"  placeholder="Enter branch name" >
                                        <div class="branch_name invalid-feedback" >Enter branch name</div>
                                    </div>
                                </div>
                            </div>
                            <div class="row bank" style="display: none">
                                <div class="col">
                                    <div class="form-group">
                                        <label for="account_type">Select Account Type <span class="text-danger">*</span></label>
                                        <select class="form-control" id="account_type" name="account_type" >
                                            <option value="Savings" selected>Savings</option>
                                        </select>
                                        <div class="account_type invalid-feedback" >Select account type</div>
                                    </div>
                                </div>
                                <div class="col">
                                    <div class="form-group">
                                        <label for="routing_number">Routing Number <span class="text-danger">*</span></label>
                                        <input type="text" class="form-control" id="routing_number" name="routing_number" placeholder="Enter routing number" >
                                        <div class="routing_number invalid-feedback" >Enter routing number</div>
                                    </div>
                                </div>
                            </div>
                            <div class="row bank" style="display: none">
                                <div class="col">
                                    <div class="form-group">
                                        <label for="account_holder_name">Account Holder Name <span class="text-danger">*</span></label>
                                        <input type="text" class="form-control" id="account_holder_name" name="account_holder_name" placeholder="Enter account holder name" >
                                        <div class="account_holder_name invalid-feedback" >Enter account holder name</div>
                                    </div>
                                </div>
                                <div class="col">
                                    <div class="form-group">
                                        <label for="account_number">Account Number <span class="text-danger">*</span></label>
                                        <input type="text" class="form-control" id="account_number" name="account_number" placeholder="Enter account number" >
                                        <div class="account_number invalid-feedback" >Enter account number</div>
                                        <div class="phone">Only english character is allowed.</div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col mt-2">
                                    <h6>Verification Information</h6>
                                    <hr class="info_border">
                                </div>
                            </div>
                            <div class="row mt-2">
                                <div class="col">
                                    <div class="form-group">
                                        <label>Verification Info (Id Type) <span class="text-danger">*</span></label>
                                        <div class="">
                                            <div class="form-check-inline">
                                                <label class="form-check-label">
                                                    <input type="radio" class="form-check-input" name="verification_id_type" value="NID" checked>NID
                                                </label>
                                            </div>
                                            <div class="form-check-inline">
                                                <label class="form-check-label">
                                                    <input type="radio" class="form-check-input" name="verification_id_type" value="Birth Certificate">Birth Certificate
                                                </label>
                                            </div>
                                            <div class="form-check-inline">
                                                <label class="form-check-label">
                                                    <input type="radio" class="form-check-input" name="verification_id_type" value="Passport">Passport
                                                </label>
                                            </div>
                                            <div class="form-check-inline">
                                                <label class="form-check-label">
                                                    <input type="radio" class="form-check-input" name="verification_id_type" value="Driving License">Driving License
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label class="nid" for="verification_id_number">NID Number <span class="text-danger">*</span></label>
                                        <label class="birth_certificate" for="verification_id_number" style="display: none">Birth Certificate Number <span class="text-danger">*</span></label>
                                        <label class="passport" for="verification_id_number" style="display: none">Passport Number <span class="text-danger">*</span></label>
                                        <label class="driving_license" for="verification_id_number" style="display: none">Driving License Number <span class="text-danger">*</span></label>
                                        <input type="text" class="form-control" id="verification_id_number" name="verification_id_number"  placeholder="Enter id number" >
                                        <div class="verification_id_number invalid-feedback">Please enter verification id number</div>
                                        <div>Only english character is allowed.</div>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="verification_id_pic">ID's Font Image <span class="text-danger">*</span></label>
                                        <input type="file" class="form-control" id="verification_id_pic" name="verification_id_pic">
                                        <div class="verification_id_pic invalid-feedback">Please uploaded ID's font image <b>(jpeg/jpg/png)</b> </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-check">
                                        <input type="checkbox" class="form-check-input" id="agree" >
                                        <label class="form-check-label" for="agree">I agree with this <a
                                                    href="#" data-toggle="modal" data-target="#termsModal">Terms & Conditions*</a></label>
{{--                                        <button type="button" class="btn btn-primary" >--}}
{{--                                            Launch demo modal--}}
{{--                                        </button>--}}
                                        @include('merchants.auth.terms')
                                        <div class="agree invalid-feedback">You must agree with the terms and conditions.</div>
                                    </div>
                                </div>
                            </div>
                            <div class="row mt-2">
                                <div class="col">
                                    <div class="text-center">
                                        <button type="submit" id="btn_fetch" class="btn btn-pr">REGISTER</button>
                                        <button id="btn_fetch_disable" class="btn btn-pr" disabled style="display:none;">SAVING ... <i class="fas fa-spinner fa-spin" style="color: white"></i></button>
                                        <a href="{{route('merchant.login')}}" class="btn btn-se">CANCEL</a>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
@section('script')
    @toastr_render
    <script src="{{ asset('public/js/merchant-reg.js')}}"></script>
    <script>
        $("#check").on('click',function () {
            document.getElementById("agree").checked = true;
            $('#exampleModal').modal('hide');
        });
        $(".pickup_area_select").select2({
            placeholder: "Select pickup area",
            allowClear: false
        });


        $("#btn_fetch").click(function() {

            var first_name =$('#first_name').val();
            var last_name =$('#last_name').val();
            var email =$('#email').val();
            var mobile_number =$('#mobile_number').val();
            var shop_name =$('#shop_name').val();
            var shop_link =$('#shop_link').val();
            var pickup_address =$('#pickup_address').val();
            var pickup_number =$('#pickup_number').val();
            var pickup_area =$('#pickup_area').val();
            var radioValue = $("input[name='payment_type']:checked"). val();
            var wallet_number =$('#wallet_number').val();
            var bank_name= $("#bank_name").val();
            var branch_name= $("#branch_name").val();
            var account_type= $("#account_type").val();
            var routing_number= $("#routing_number").val();
            var account_holder_name= $("#account_holder_name").val();
            var account_number= $("#account_number").val();
            var verification_id_number =$('#verification_id_number').val();
            var verification_id_pic =$('#verification_id_pic').val();
            if (!first_name.replace(/\s/g, '').length || !last_name.replace(/\s/g, '').length ||
                !email.replace(/\s/g, '').length || !mobile_number.replace(/\s/g, '').length||
                !(mobile_pattern.test(mobile_number)) || !shop_name.replace(/\s/g, '').length ||
                !shop_link.replace(/\s/g, '').length || !pickup_number.replace(/\s/g, '').length
                || !(mobile_pattern.test(pickup_number)) || pickup_area==null ||
                !pickup_address.replace(/\s/g, '').length ||
                !verification_id_number.replace(/\s/g, '').length ||
                !verification_id_pic.replace(/\s/g, '').length || !$("#agree").is(':checked'))
            {
                $("#btn_fetch").hide();
                $('#btn_fetch_disable').show();
                setTimeout(function(){
                    $("#btn_fetch").show();
                    $('#btn_fetch_disable').hide();
                }, 1*1000);
                if(!first_name.replace(/\s/g, '').length){
                    $('#first_name').addClass('input-invalid');
                    $('.first-name').show();
                }
                if(!last_name.replace(/\s/g, '').length){
                    $("#last_name").addClass('input-invalid');
                    $('.last_name').show();
                }
                if(!email.replace(/\s/g, '').length){
                    $("#email").addClass('input-invalid');
                    $('.email').show();
                }
                if(!mobile_number.replace(/\s/g, '').length){
                    $("#mobile_number").addClass('input-invalid');
                    $('.mobile_number').show();
                }
                if (mobile_number.replace(/\s/g, '').length && !(mobile_pattern.test(mobile_number))) {
                    $('.invalid-feedback-mobile').show();
                    $('.mobile_number').hide();
                }
                if(!shop_name.replace(/\s/g, '').length){
                    $("#shop_name").addClass('input-invalid');
                    $('.shop_name').show();
                }
                if(!shop_link.replace(/\s/g, '').length){
                    $("#shop_link").addClass('input-invalid');
                    $('.shop_link').show();
                }
                if(!pickup_number.replace(/\s/g, '').length){
                    $("#pickup_number").addClass('input-invalid');
                    $('.pickup_number').show();
                }
                if (pickup_number.replace(/\s/g, '').length && !(mobile_pattern.test(pickup_number))) {
                    $('.invalid-feedback-mobile').show();
                    $('.mobile_number').hide();
                }
                if(pickup_area==null){
                    $("#pickup_area").addClass('input-invalid');
                    $('.pickup_area').show();
                }
                if(!pickup_address.replace(/\s/g, '').length){
                    $("#pickup_address").addClass('input-invalid');
                    $('.pickup_address').show();
                }
                if(!verification_id_number.replace(/\s/g, '').length){
                    $("#verification_id_number").addClass('input-invalid');
                    $('.verification_id_number').show();
                }
                if(!verification_id_pic.replace(/\s/g, '').length){
                    $("#verification_id_pic").addClass('input-invalid');
                    $('.verification_id_pic').show();
                }
                if(!$("#agree").is(':checked')){
                    // $("#agree").addClass('input-invalid');
                    $('.agree').show();
                }
                toastr.error('Please fix the given errors.');
                return false;
            }
            else if(radioValue=="Mobile" && !wallet_number_pattern.test(wallet_number))
            {
                if (!wallet_number_pattern.test(wallet_number))
                {
                    $('.wallet_number').show();
                    $('#wallet_number').addClass('input-invalid');
                }
                toastr.error('Please fix the given errors.');
                return false;
            }
            else if (radioValue=="Bank" && ( !bank_name.replace(/\s/g, '').length || !branch_name.replace(/\s/g, '').length
                || !account_type.replace(/\s/g, '').length || !routing_number.replace(/\s/g, '').length
                || !account_holder_name.replace(/\s/g, '').length || !account_number.replace(/\s/g, '').length))
            {
                if(!bank_name.replace(/\s/g, '').length){
                    $('#bank_name').addClass('input-invalid');
                    $('.bank_name').show();
                }
                if(!branch_name.replace(/\s/g, '').length){
                    $('#branch_name').addClass('input-invalid');
                    $('.branch_name').show();
                }
                if(!account_type.replace(/\s/g, '').length){
                    $('#account_type').addClass('input-invalid');
                    $('.account_type').show();
                }
                if(!routing_number.replace(/\s/g, '').length){
                    $('#routing_number').addClass('input-invalid');
                    $('.routing_number').show();
                }
                if(!account_holder_name.replace(/\s/g, '').length){
                    $('#account_holder_name').addClass('input-invalid');
                    $('.account_holder_name').show();
                }
                if(!account_number.replace(/\s/g, '').length){
                    $('#account_number').addClass('input-invalid');
                    $('.account_number').show();
                }
                toastr.error('Please fix the given errors.');
                return false;
            }
            else{
                $("#btn_fetch").hide();
                $('#btn_fetch_disable').show();
                return true;
            }
        });

    </script>
@stop