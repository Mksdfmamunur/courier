<!doctype html>
<html lang="en">

<meta http-equiv="content-type" content="text/html;charset=utf-8"/><!-- /Added by HTTrack -->
<head>
    <title>@yield('title')</title>
    <meta charset="utf-8" />
    <meta name="_token" content="{{ csrf_token() }}">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="description" content="#">
    <meta name="keywords" content="#">
    <meta name="author" content="MetroExpress">
    <!-- Favicon icon -->
    <link rel="icon" href="{{asset('public/icon/logo.png')}}" type="image/x-icon">
    <link rel="stylesheet" href="{{ asset('public/css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{ asset('public/assets/fontawesome-5.14.0/css/all.css')}}">
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/css/select2.min.css" rel="stylesheet" />
    <link href="https://fonts.maateen.me/solaiman-lipi/font.css" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.css">

    {{--    custom--}}
    <link rel="stylesheet" href="{{ asset('public/css/custom.css')}}">
    <style>

        .card{
            margin-top: 30px;
            border-radius: 20px;
        }
        .select2-container .select2-selection--single {

            height: 38px !important;
            padding: 2px 0px !important;
        }
        .footer{
            padding-top: 20px;
            padding-bottom: 20px;
        }
    </style>

    @yield('css')
</head>
<body>
@yield('content')
<div class="footer">
    <div class="container">
        <div class="row">
            <div class="col-12 text-center">
                <p><i class="fas fa-copyright"></i> Copyright by MetroExpress Ltd.</p>
            </div>
        </div>
    </div>
</div>
<script src="{{ asset('public/js/jquery.min.js')}}"></script>
<script src="{{ asset('public/js/popper.min.js')}}"></script>
<script src="{{ asset('public/js/bootstrap.min.js')}}"></script>
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js"></script>
<script src="{{ asset('public/js/numberCheck.js')}}"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>
@yield('script')

</body>
</html>