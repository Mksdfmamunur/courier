@extends('merchants.auth.master')
@section('title')
    Merchant || Reset Password
@stop
@section('css')

@stop
@section('content')
    <div class="container">
        <div class="row">
            <div class="offset-sm-4 col-sm-4">
                <div class="card">
                    <div class="card-body">
                        <div class="text-center">
                            <img src="{{asset('public/icon/logo.png')}}" height="100" class="rounded mx-auto d-block" alt="">
                            <h3 class="mt-2">Reset Password</h3>
                        </div>
                        <div class="container">
                            <div class="row">
                                <div class="col-sm-12">
                                    <form  method="post" action="{{route('merchant.reset-password')}}">
                                        @csrf
                                        <input type="hidden" value="{{$merchant->id}}" name="id">
                                        <div class="form-group">
                                            <label for="password">New Password</label>
                                            <input type="password" class="form-control" minlength="6" id="new_password" name="password"  placeholder="Enter new password" required  >
                                            <div class="new_password invalid-feedback">Please enter minimum 6 character password</div>
                                        </div>
                                        <div class="form-group">
                                            <label for="confirm_password">Confirm Password</label>
                                            <input type="password" class="form-control" minlength="6" id="confirm_password"  placeholder="Confirm new password"  name="password" required >
                                            <div class="invalid-feedback confirm_password">Confirm password not match</div>
                                        </div>
                                        <div class="text-center">
                                            <button type="submit" class="btn btn-pr" id="btn_fetch">RESET</button>
                                            <button type="submit" class="btn btn-pr" disabled id="btn_fetch_disable" style="display: none; font-size: 12px;">RESET..<i class="fas fa-spinner fa-spin" style="color: white"></i></button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
@section('script')
    @toastr_render
    <script>
        $('#new_password').on('keyup',function (){
            var new_password =$('#new_password').val();
            var confirm_password =$('#confirm_password').val();
            if (new_password.length<6){
                $('#new_password').addClass('input-invalid');
                $('.new_password').show();
            }
            else if (new_password!=confirm_password){
                $('#confirm_password').addClass('input-invalid');
                $('.confirm_password').show();
                $("#new_password").removeClass('input-invalid');
                $('.new_password').hide();
            }
            else if (new_password==confirm_password){
                $('#confirm_password').removeClass('input-invalid');
                $('.confirm_password').hide();
                $("#new_password").removeClass('input-invalid');
                $('.new_password').hide();
            }
            else{
                $("#new_password").removeClass('input-invalid');
                $('.new_password').hide();
            }
        });
        $('#confirm_password').on('keyup',function (){
            var new_password =$('#new_password').val();
            var confirm_password =$('#confirm_password').val();
            if (confirm_password.length<6){
                $('#confirm_password').addClass('input-invalid');
                $('.confirm_password').show();
            }
            else if (new_password!=confirm_password){
                $('#confirm_password').addClass('input-invalid');
                $('.confirm_password').show();
            }
            else if (new_password==confirm_password){
                $('#confirm_password').removeClass('input-invalid');
                $('.confirm_password').hide();
                $("#new_password").removeClass('input-invalid');
                $('.new_password').hide();
            }
            else{
                $("#confirm_password").removeClass('input-invalid');
                $('.confirm_password').hide();
            }
        });
        $("#btn_fetch").on('click',function () {
            var new_password = $('#new_password').val();
            var confirm_password = $('#confirm_password').val();
            if ( new_password!=confirm_password)
            {
                $("#btn_fetch").hide();
                $('#btn_fetch_disable').show();
                setTimeout(function(){
                    $("#btn_fetch_password").show();
                    $('#btn_fetch__password_disable').hide();
                }, 1*1000);

                $('#confirm_password').addClass('input-invalid');
                $('.confirm_password').show();
                toastr.error('Please fix the given errors.');
                return false;
            }
            $("#btn_fetch").hide();
            $("#btn_fetch_disable").show();
            return true;
        })

    </script>

@stop
