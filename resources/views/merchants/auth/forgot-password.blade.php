@extends('merchants.auth.master')
@section('title')
    Merchant || Forgot Password
@stop
@section('css')

@stop
@section('content')
    <div class="container">
        <div class="row">
            <div class="offset-sm-4 col-sm-4">
                <div class="card">
                    <div class="card-body">
                        <div class="text-center">
                            <img src="{{asset('public/icon/logo.png')}}" height="100" class="rounded mx-auto d-block" alt="">
                            <h3 class="mt-2">Forgot Password</h3>
                        </div>
                        <div class="container">
                            <div class="row">
                                <div class="col-sm-12">
                                    <form  method="post" action="{{route('merchant.forgot-password')}}">
                                        @csrf
                                            <div class="login-form-1">
                                                <form id="forgot-password-form" class="text-left">
                                                    <div class="etc-login-form">
                                                        <p>Please enter your merchant email address to get password reset instructions.</p>
                                                    </div>
                                                    <div class="login-form-main-message"></div>
                                                    <div class="main-login-form">
                                                        <div class="login-group">
                                                            <div class="form-group">
                                                                <label for="email" class="sr-only">Email address</label>
                                                                <input type="email" class="form-control" id="email" name="email" placeholder="email address" required>
                                                            </div>
                                                        </div>
                                                        <div class="text-center">
                                                            <button type="submit" class="btn btn-pr" id="btn">Submit</button>
                                                            <button type="submit" class="btn btn-pr" id="btn_disable" disabled style="display:none;">Submitting...<i class="fas fa-spinner fa-spin" style="color: white"></i> </button>
                                                        </div>
                                                    </div>
                                                    <div class="etc-login-form mt-1">
                                                        <p>Already have an account? <a href="{{route('merchant.login')}}">login here</a></p>
                                                        <p>New Merchant? <a href="{{route('merchant.registration')}}">Register here</a></p>
                                                    </div>
                                                </form>
                                            </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
@section('script')
    @toastr_render
    <script>
        $("#btn").on('click',function () {
            $("#btn").hide();
            $("#btn_disable").show();
            return true;
        })

    </script>

@stop
