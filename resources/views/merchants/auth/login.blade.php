@extends('merchants.auth.master')
@section('title')
    Merchant || LogIn
@stop
@section('css')

@stop
@section('content')
    <div class="container">
        <div class="row">
            <div class="offset-lg-4 col-lg-4 offset-md-3 col-md-6">
                <div class="card">
                    <div class="card-body">
                        <div class="text-center">
                        <img src="{{asset('public/icon/logo.png')}}" height="100" class="rounded mx-auto d-block" alt="">
                        <h2 class="bangla">প্রবেশ করুন</h2>
                    </div>
                        <div class="container">
                        <div class="row">
                            <div class="col-sm-12">
                                <form  method="post" action="{{route('merchant.submit')}}">
                                    @csrf
                                    <div class="form-group">
                                        <label for="email1">Email</label>
                                        <input type="email" class="form-control" id="email1" name="email"  placeholder="Enter email address"   autocomplete="email" autofocus>
                                    </div>
                                    <div class="form-group">
                                        <label for="password">Password</label>
                                        <input type="password" class="form-control" id="password"  placeholder="Enter password"  name="password"  autocomplete="current-password">
                                    </div>
                                    <div class="form-group form-check">
                                        <input type="checkbox" class="form-check-input" id="remember" name="remember" {{ old('remember') ? 'checked' : '' }}>
                                        <label class="form-check-label" for="remember_token">Remember Me</label>
                                    </div>
                                    <div class="text-center">
                                        <button type="submit" class="btn btn-pr" id="btn_fetch">LOGIN</button>
                                        <button type="submit" class="btn btn-pr" disabled id="btn_fetch_disable" style="display: none; font-size: 12px;">LOGIN..<i class="fas fa-spinner fa-spin" style="color: white"></i></button>
                                        <a href="{{route('merchant.registration')}}" class="btn btn-se">REGISTER</a>
                                    </div>
                                    <div class="text-center mt-2">
                                        <a href="{{route('merchant.forgot')}}" class="text-danger">Forgot Password?</a>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
@section('script')
    @toastr_render
    <script>
        $("#btn_fetch").on('click',function () {
            $("#btn_fetch").hide();
            $("#btn_fetch_disable").show();
            return true;
        })

    </script>

@stop