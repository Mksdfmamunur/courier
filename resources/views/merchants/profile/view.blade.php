@extends('merchants.layouts.app')
@section('title')
    Merchant || View Shop
@stop
@section('dashboard','active')
@section('css')

@stop

@section('content')
    <div class="container-fluid mt-1 pb-2">
        <div class="row pt-3">
                <div class="col-sm-6">
                    <div class="container-fluid">
                        <div class="card">
                            <div class="card-header">
                                Personal Information
                            </div>
                            <div class="card-body">
                                <form action="{{route('merchant.update-profile')}}" method="post">
                                    @csrf
                                    <input type="hidden" id="id" name="id" value="{{$merchant->id}}">
                                    <div class="form-group">
                                        <label for="first_name">First Name <span class="text-danger">*</span></label>
                                        <input type="text" class="form-control" id="first_name" name="first_name" value="{{$merchant->first_name}}"  placeholder="Enter first name" required>
                                        <div class="first-name invalid-feedback">Please enter first name</div>
                                    </div>
                                    <div class="form-group">
                                        <label for="last_name">Last Name <span class="text-danger">*</span></label>
                                        <input type="text" class="form-control" id="last_name" name="last_name" value="{{$merchant->last_name}}" placeholder="Enter last name" required>
                                        <div class="last_name invalid-feedback">Please enter last name</div>
                                    </div>
                                    <div class="form-group">
                                        <label for="email">Email</label>
                                        <input type="email" class="form-control" id="email" name="email" value="{{$merchant->email}}" placeholder="Enter email address" readonly>
                                        <div class="invalid-feedback email">Please enter valid email address</div>
                                    </div>
                                    <div class="form-group">
                                        <label for="mobile_number">Mobile Number <span class="text-danger">*</span></label>
                                        <input type="text" class="form-control number" id="mobile_number" name="mobile_number" value="{{$merchant->mobile_number}}"  placeholder="Enter mobile number" required>
                                        <div class="invalid-feedback mobile_number">Please enter valid phone number</div>
                                        <div>Only english character is allowed.</div>
                                    </div>
                                    <button type="submit" id="btn_fetch" class="btn btn-pr">UPDATE</button>
                                    <button id="btn_fetch_disable" class="btn btn-pr" disabled style="display:none;">UPDATING ... <i class="fas fa-spinner fa-spin" style="color: white"></i></button>
                                </form>
                            </div>
                        </div>
                    </div>

                </div>
                <div class="col-sm-6">
                    <div class="container-fluid">
                        <div class="card">
                            <div class="card-header">
                                Change Password
                            </div>
                            <div class="card-body">
                                <form action="{{route('merchant.change-password')}}" method="post">
                                    @csrf
                                    <input type="hidden" id="merchant_id" name="merchant_id" value="{{$merchant->id}}">
                                    <div class="form-group">
                                        <label for="old_password">Old Password<span class="text-danger">*</span></label>
                                        <input type="password" class="form-control" id="old_password" name="old_password"  placeholder="Old Password" >
                                        <div class="old_password invalid-feedback">Please enter old password</div>
                                    </div>
                                    <div class="form-group">
                                        <label for="new_password">New Password<span class="text-danger">*</span></label>
                                        <input type="password" class="form-control" id="new_password" name="new_password"  placeholder="New Password" >
                                        <div class="new_password invalid-feedback">Please enter new password</div>
                                    </div>
                                    <div class="form-group">
                                        <label for="confirm_password">Confirm Password<span class="text-danger">*</span></label>
                                        <input type="password" class="form-control" id="confirm_password" name="confirm_password"  placeholder="Confirm Password" >
                                        <div class="invalid-feedback confirm_password">Confirm password not match</div>
                                    </div>
                                    <button type="submit" id="btn_fetch_password" class="btn btn-pr">Save Changes</button>
                                    <button id="btn_fetch_password_disable" class="btn btn-pr" disabled style="display:none;">Saving ... <i class="fas fa-spinner fa-spin" style="color: white"></i></button>
                                </form>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
    </div>
@stop
@section('script')
    @toastr_render
    <script>
        $('#mobile_number').on('keyup',function () {
            var mobile =$('#mobile_number').val();
            if (wallet_number_pattern.test(mobile))
            {
                $('.mobile_number').hide();
                $(this).removeClass('input-invalid');
            }
            else {
                $('.mobile_number').show();
                $(this).addClass('input-invalid');
            }
        });
        $('#first_name').on('keyup',function (){
            var first_name =$('#first_name').val();
            if(first_name==""){
                $('#first_name').addClass('input-invalid');
                $('.first-name').show();
            }
            else{
                $("#first_name").removeClass('input-invalid');
                $('.first_name').hide();
            }
        });
        $('#last_name').on('keyup',function (){
            var last_name =$('#last_name').val();
            if(last_name==""){
                $("#last_name").addClass('input-invalid');
                $('.last_name').show();
            }
            else{
                $("#last_name").removeClass('input-invalid');
                $('.last_name').hide();
            }
        });
        $("#btn_fetch").click(function() {
            var first_name =$('#first_name').val();
            var last_name =$('#last_name').val();
            var mobile_number =$('#mobile_number').val();
            if (!first_name.replace(/\s/g, '').length || !last_name.replace(/\s/g, '').length || !mobile_number.replace(/\s/g, '').length|| !(mobile_pattern.test(mobile_number)))
            {
                $("#btn_fetch").hide();
                $('#btn_fetch_disable').show();
                setTimeout(function(){
                    $("#btn_fetch").show();
                    $('#btn_fetch_disable').hide();
                }, 1*1000);
                if(!first_name.replace(/\s/g, '').length){
                    $('#first_name').addClass('input-invalid');
                    $('.first-name').show();
                }
                if(!last_name.replace(/\s/g, '').length){
                    $("#last_name").addClass('input-invalid');
                    $('.last_name').show();
                }
                if(!mobile_number.replace(/\s/g, '').length){
                    $("#mobile_number").addClass('input-invalid');
                    $('.mobile_number').show();
                }
                if (mobile_number.replace(/\s/g, '').length && !(mobile_pattern.test(mobile_number))) {
                    $('.invalid-feedback-mobile').show();
                    $('.mobile_number').hide();
                }
                toastr.error('Please fix the given errors.');
                return false;
            }
            else{
                $("#btn_fetch").hide();
                $('#btn_fetch_disable').show();
                return true;
            }
        });

        //change password
        $('#old_password').on('keyup',function (){
            var old_password =$('#old_password').val();
            if(!old_password.replace(/\s/g, '').length){
                $('#old_password').addClass('input-invalid');
                $('.old_password').show();
            }
            else if (old_password.length<6){
                $('#old_password').addClass('input-invalid');
                $('.old_password').show();
                $('.old_password').html("Please enter minimum 6 character password")
            }
            else{
                $("#old_password").removeClass('input-invalid');
                $('.old_password').hide();
            }
        });
        $('#new_password').on('keyup',function (){
            var new_password =$('#new_password').val();
            var confirm_password =$('#confirm_password').val();
             if (new_password.length<6){
                $('#new_password').addClass('input-invalid');
                $('.new_password').show();
                $('.new_password').html("Please enter minimum 6 character password")
            }
            else if (new_password!=confirm_password){
                $('#confirm_password').addClass('input-invalid');
                $('.confirm_password').show();
                $("#new_password").removeClass('input-invalid');
                $('.new_password').hide();
            }
             else if (new_password==confirm_password){
                 $('#confirm_password').removeClass('input-invalid');
                 $('.confirm_password').hide();
                 $("#new_password").removeClass('input-invalid');
                 $('.new_password').hide();
             }
            else{
                $("#new_password").removeClass('input-invalid');
                $('.new_password').hide();
            }
        });
        $('#confirm_password').on('keyup',function (){
            var new_password =$('#new_password').val();
            var confirm_password =$('#confirm_password').val();
            if (confirm_password.length<6){
                $('#confirm_password').addClass('input-invalid');
                $('.confirm_password').show();
            }
            else if (new_password!=confirm_password){
                $('#confirm_password').addClass('input-invalid');
                $('.confirm_password').show();
            }
            else if (new_password==confirm_password){
                $('#confirm_password').removeClass('input-invalid');
                $('.confirm_password').hide();
                $("#new_password").removeClass('input-invalid');
                $('.new_password').hide();
            }
            else{
                $("#confirm_password").removeClass('input-invalid');
                $('.confirm_password').hide();
            }
        });

        $("#btn_fetch_password").click(function() {
            var new_password =$('#new_password').val();
            var old_password =$('#old_password').val();
            var confirm_password =$('#confirm_password').val();
            if (!confirm_password.replace(/\s/g, '').length || !old_password.replace(/\s/g, '').length || !old_password.replace(/\s/g, '').length|| new_password!=confirm_password || old_password.length<6 || new_password.length<6||confirm_password.length<6)
            {
                $("#btn_fetch_password").hide();
                $('#btn_fetch__password_disable').show();
                setTimeout(function(){
                    $("#btn_fetch_password").show();
                    $('#btn_fetch__password_disable').hide();
                }, 1*1000);
                if(!old_password.replace(/\s/g, '').length){
                    $('#old_password').addClass('input-invalid');
                    $('.old_password').show();
                }
                else if (old_password.length<6){
                    $('#old_password').addClass('input-invalid');
                    $('.old_password').show();
                    $('.old_password').html("Please enter minimum 6 character password")
                }
                else{
                    $("#old_password").removeClass('input-invalid');
                    $('.old_password').hide();
                }
                if (new_password.length<6){
                    $('#new_password').addClass('input-invalid');
                    $('.new_password').show();
                    $('.new_password').html("Please enter minimum 6 character password")
                }
                if (new_password!=confirm_password){
                    $('#confirm_password').addClass('input-invalid');
                    $('.confirm_password').show();
                    $("#new_password").removeClass('input-invalid');
                    $('.new_password').hide();
                }
                if (confirm_password.length<6){
                    $('#confirm_password').addClass('input-invalid');
                    $('.confirm_password').show();
                }
                toastr.error('Please fix the given errors.');
                return false;
            }
            else{
                $("#btn_fetch_password").hide();
                $('#btn_fetch_password_disable').show();
                return true;
            }
        });
    </script>
@stop