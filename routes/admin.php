<?php

use Illuminate\Support\Facades\Route;

Route::group(['namespace' => 'Admin','prefix'=>'admin','as'=>'admin.'], function () {

    Route::group(['namespace' => 'Auth'], function () {
        Route::get('login', 'LoginController@login')->name('login');
        Route::post('login', 'LoginController@loginSubmit')->name('submit');
        Route::post('logout', 'LoginController@logout')->name('logout');
    });

    Route::group(['middleware'=>['auth:admin']], function () {
        Route::get('dashboard', 'AdminController@index')->name('dashboard');
        Route::get('track-order', 'AdminController@trackOrder')->name('track-order');


        Route::group(['prefix'=>'rider','as'=>'rider.'], function () {
            Route::get('view', 'RiderController@index')->name('view');
            Route::get('register', 'RiderController@register')->name('register');
            Route::post('register', 'RiderController@store')->name('store');
            Route::get('edit/{id}', 'RiderController@edit')->name('edit');
            Route::post('edit', 'RiderController@update')->name('update');
        });
        Route::group(['prefix'=>'merchant','as'=>'merchant.'], function () {
            Route::get('approve', 'MerchantController@approve')->name('approve');
            Route::get('request', 'MerchantController@request')->name('request');
            Route::get('reject', 'MerchantController@reject')->name('reject');
            Route::post('register', 'MerchantController@store')->name('store');
            Route::get('plan', 'MerchantController@plan')->name('plan');
            Route::post('plan', 'MerchantController@updatePlan')->name('update-plan');
            Route::post('edit', 'MerchantController@update')->name('update');
            Route::post('status', 'MerchantController@status')->name('status');
        });
        Route::group(['prefix'=>'business','as'=>'business.'], function () {
            Route::get('plan', 'BusinessSettingController@plan')->name('plan');
            Route::post('store-plan', 'BusinessSettingController@storePlan')->name('store-plan');
            Route::post('update-plan', 'BusinessSettingController@updatePlan')->name('update-plan');
            Route::get('coverage-area', 'BusinessSettingController@coverageArea')->name('coverage-area');
            Route::post('store-coverage-area', 'BusinessSettingController@StoreCoverageArea')->name('store-coverage-area');
            Route::post('status', 'BusinessSettingController@status')->name('status');
        });
        Route::group(['prefix'=>'order','as'=>'order.'], function () {
            Route::get('pending', 'OrderController@pending')->name('pending');
            Route::get('confirm', 'OrderController@confirm')->name('confirm');
            Route::get('shipped', 'OrderController@shipped')->name('shipped');
            Route::get('delivered', 'OrderController@delivered')->name('delivered');
            Route::get('print/{id}', 'OrderController@print')->name('print');
            Route::get('view', 'OrderController@view')->name('view');
            Route::get('edit/{id}', 'OrderController@edit')->name('edit');
            Route::post('edit', 'OrderController@update')->name('update');
            Route::post('search-shop', 'OrderController@searchShop')->name('search-shop');
            Route::post('search-shop', 'OrderController@searchShop')->name('search-shop');
            Route::post('delivery-charge-plan', 'OrderController@deliveryChargePlan')->name('delivery-charge-plan');
            Route::post('fetch-delivery', 'OrderController@fetchDelivery')->name('fetch-delivery');
            Route::get('delivery', 'OrderController@delivery')->name('delivery');
            Route::get('pending', 'OrderController@pending')->name('pending');
            Route::post('fetch-data', 'OrderController@fetchData')->name('fetch-data');
            Route::post('fetch-data-assign', 'OrderController@fetchDataAssign')->name('fetch-data-assign');
            Route::post('search-data', 'OrderController@searchData')->name('search-data');
            Route::post('status', 'OrderController@status')->name('status');
            Route::post('single-status', 'OrderController@singleStatus')->name('single-status');
        });
        Route::group(['prefix'=>'pickup','as'=>'pickup.'], function () {
            Route::get('list', 'PickupController@list')->name('list');
            Route::get('approve', 'PickupController@approve')->name('approve');
            Route::post('status', 'PickupController@status')->name('status');
        });
        Route::group(['prefix'=>'payment','as'=>'payment.'], function () {
            Route::get('rider-payment', 'PaymentController@riderPayment')->name('rider-payment');
            Route::get('rider-payment-list', 'PaymentController@riderPaymentList')->name('rider-payment-list');
            Route::post('rider-payment', 'PaymentController@riderPaymentStore')->name('rider-payment-store');
            Route::get('merchant-payment-invoice', 'PaymentController@createMerchantPaymentInvoice')->name('merchant-payment-invoice');
            Route::post('merchant-payment-invoice', 'PaymentController@createMerchantPaymentInvoiceStore')->name('merchant-payment-invoice-store');
            Route::get('merchant-invoice-list', 'PaymentController@merchantPaymentInvoiceList')->name('merchant-invoice-list');
        });
    });
});
