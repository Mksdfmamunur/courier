<?php

use Illuminate\Support\Facades\Route;

Route::group(['namespace' => 'Merchant','prefix'=>'merchant','as'=>'merchant.'], function () {

    /*authentication*/
    Route::group(['namespace' => 'Auth'], function () {
        Route::get('login', 'LoginController@showMerchantLoginForm')->name('login');
        Route::post('login', 'LoginController@merchantLoginSubmit')->name('submit');
        Route::post('logout', 'LoginController@merchantLogout')->name('logout');
        Route::get('registration', 'RegisterController@showMerchantRegisterForm')->name('registration');
        Route::post('registration', 'RegisterController@submitMerchantRegisterForm')->name('registration.submit');
        Route::get('registration-success', 'RegisterController@registrationSuccess')->name('registration-success');
        Route::get('reset-success', 'RegisterController@resetSuccess')->name('reset-success');
        Route::get('forgot-password', 'RegisterController@forgot')->name('forgot');
        Route::post('forgot-password', 'RegisterController@forgotPassword')->name('forgot-password');
        Route::get('reset-password', 'RegisterController@reset')->name('reset');
        Route::post('reset-password', 'RegisterController@resetPassword')->name('reset-password');
    });

    /*authenticated*/
    Route::group(['middleware'=>['auth:merchant']], function () {
        Route::get('dashboard', 'MerchantController@index')->name('dashboard');
        Route::get('track-order', 'MerchantController@trackOrder')->name('track-order');
        Route::get('coverage-area', 'MerchantController@coverageArea')->name('coverage-area');
        Route::get('profile', 'MerchantController@viewProfile')->name('profile');
        Route::post('update-profile', 'MerchantController@updateProfile')->name('update-profile');
        Route::post('change-password', 'MerchantController@changePassword')->name('change-password');
        Route::group(['prefix'=>'order','as'=>'order.'], function () {
            Route::get('status/{id}', 'OrderController@status')->name('status');
            Route::get('view-all', 'OrderController@index')->name('view');
            Route::get('create', 'OrderController@create')->name('create');
            Route::post('create', 'OrderController@store')->name('store');
            Route::get('edit/{id}', 'OrderController@edit')->name('edit');
            Route::post('edit', 'OrderController@update')->name('update');
            Route::post('search-shop', 'OrderController@searchShop')->name('search-shop');
            Route::post('delivery-charge-plan', 'OrderController@deliveryChargePlan')->name('delivery-charge-plan');
            Route::post('fetch-delivery', 'OrderController@fetchDelivery')->name('fetch-delivery');
            Route::post('fetch-data', 'OrderController@fetchData')->name('fetch-data');
        });
        Route::group(['prefix'=>'shop','as'=>'shop.'], function () {
            Route::get('view-all', 'ShopController@index')->name('view');
            Route::get('create', 'ShopController@create')->name('create');
            Route::post('create', 'ShopController@store')->name('store');
            Route::get('edit/{id}', 'ShopController@edit')->name('edit');
            Route::post('edit', 'ShopController@update')->name('update');
        });

        Route::group(['prefix'=>'payment-info','as'=>'payment-info.'], function () {
            Route::get('view', 'PaymentInfoController@index')->name('view');
            Route::get('edit/{id}', 'PaymentInfoController@edit')->name('edit');
            Route::post('edit', 'PaymentInfoController@update')->name('update');
        });
        Route::group(['prefix'=>'invoice','as'=>'invoice.'], function () {
            Route::get('receivable', 'InvoiceController@index')->name('receivable');
        });

    });

});