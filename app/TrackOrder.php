<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TrackOrder extends Model
{
    public function rider()
    {
        return $this->hasOne('App\Rider','id','rider_id');
    }
    public function status()
    {
        return $this->hasOne('App\Status','id','status_id');
    }
    public function subStatus()
    {
        return $this->hasOne('App\SubStatus','id','sub_status_id');
    }
}
