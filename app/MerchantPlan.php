<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MerchantPlan extends Model
{
    public function merchant()
    {
        return $this->hasOne('App\Merchant','id');
    }
    public function merchantShop()
    {
        return $this->hasOne('App\MerchantShop','id');
    }
    public function plan()
    {
        return $this->hasOne('App\Plan','id','plan_id');
    }
}
