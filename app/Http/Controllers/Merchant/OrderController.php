<?php

namespace App\Http\Controllers\Merchant;

use App\Area;
use App\City;
use App\CoverageArea;
use App\Http\Controllers\Controller;
use App\MerchantPlan;
use App\MerchantShop;
use App\Order;
use App\Plan;
use App\Status;
use App\TrackOrder;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class OrderController extends Controller
{
    protected $orderLogic;
    public function __construct()
    {

        $this->orderLogic = new \App\BLL\OrderLogic();
    }
    public function index()
    {
        return view('merchants.orders.view');
    }

    public function create()
    {
        return view('merchants.orders.create');
    }

    public function store(Request $request)
    {
        $this->orderLogic->createOrder($request);
        toastr()->success("Order created successfully.");
        return redirect()->back();
    }

    public function edit($id)
    {
        $order = Order::findOrFail(decrypt($id));
        return view('merchants.orders.edit',compact('order'));
    }

    public function update(Request $request)
    {
        $this->orderLogic->updateOrder($request);
        toastr()->success("Order updated successfully.");
        return redirect()->route('merchant.order.view');
    }

    public function searchShop(Request $request)
    {
        $shop = MerchantShop::where('id',$request->shop_id)->first();
        $area = Area::where('id',$shop->pickup_area)->first();
        $city = City::where('id',$area->city_id)->first();
        return response()->json(['shop'=>$shop,'area'=>$area ,'city'=>$city]);
    }
    public function deliveryChargePlan(Request $request)
    {
        $recipientArea = CoverageArea::where('id',$request->recipient_area)->first();
        $orderWeight = $request->order_weight;
        $deliveryTime = $request->delivery_time;
        $amountToCollect = $request->amount_to_collect;
        $merchantPlan = MerchantPlan::where('shop_id',$request->shop_id)->first();
        $deliveryPlan= Plan::where('id',$merchantPlan->plan_id)->first();
        return response()->json(['deliveryPlan'=>$deliveryPlan,'deliveryTime'=>$deliveryTime,'recipientArea'=>$recipientArea,'orderWeight'=>$orderWeight,'amountToCollect'=>$amountToCollect]);
    }

    public function fetchDelivery(Request $request)
    {
        $value = $request->recipient_area;
        $data = CoverageArea::where('id', $value)->first();
        $output="";
        if ($data->division == "Inside Dhaka")
        {
            $output .= '<option value="idr" selected>Regular Delivery (24-48 Hours)</option>';
            $output .= '<option value="idu">Urgent Delivery (8-12 Hours)</option>';
        }
        else if ($data->division == "Inside Dhaka")
        {
            $output .= '<option value="ds" selected>Regular Delivery (48-72 Hours)</option>';
        }
        else{
            $output .= '<option value="od" selected>Regular Delivery (72-96 Hours)</option>';
        }

        echo $output;
    }

    public function fetchData(Request $request)
    {
        if($request->ajax())
        {
            $orders =$this->orderLogic->searchOrder($request);
            $data =  view('merchants.orders.data',['orders' => $orders])->render();
            return response()->json(['success' =>$data]);
        }
    }
    public function status($id)
    {
        $order = Order::where('id',decrypt($id))->first();
        $order->delivery_status = 7;
        $order->save();
        $tracking = new TrackOrder;
        $tracking->tracking_id = $order->tracking_id;
        $tracking->status =7;
        $tracking->save();
        toastr()->success("Order deleted successfully.");
        return redirect()->back();
    }

}
