<?php

namespace App\Http\Controllers\Merchant;

use App\Http\Controllers\Controller;
use App\Merchant;
use App\MerchantShop;
use App\Order;
use App\TrackOrder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class MerchantController extends Controller
{

    public function index(){
        return view('merchants.dashboard');
    }
    public function viewProfile()
    {
        $merchant = Merchant::where('id',Auth::id())->first();
        return view('merchants.profile.view', compact('merchant'));
    }
    public function updateProfile(Request $request)
    {
        $merchant = Merchant::where('id',$request->id)->first();
        $merchant->first_name = $request->first_name;
        $merchant->last_name = $request->last_name;
        $merchant->mobile_number = $request->mobile_number;
        $merchant->save();
        toastr()->success("Profile updated successfully.");
        return redirect()->route('merchant.profile');
    }
    public function changePassword(Request $request)
    {
        $merchant = Merchant::where('id',$request->merchant_id)->first();
        if (!(Hash::check($request->old_password, $merchant->password)))
        {
            toastr()->error("Old password not match");
            return redirect()->back();
        }
        else{
            $merchant->password = Hash::make($request->new_password);
            $merchant->save();
            toastr()->success("Password change successfully.");
            return redirect()->route('merchant.profile');
        }
    }
    public function trackOrder(Request $request)
    {
        $trackingId = $request->get('trackingId');
//        $shops = MerchantShop::where('merchant_id',Auth::id())->get();
//        foreach ($shops as $shop)
//        {
            $order = Order::where('merchant_id',Auth::id())->where('tracking_id',$trackingId)->first();
//        }
        if (empty($order))
        {
            $trackings =null;
            return view('merchants.tracking',['trackings'=>$trackings]);
        }
        else{
            $trackings = TrackOrder::where('tracking_id',$order->tracking_id)->orderBy('id','desc')->get();
            return view('merchants.tracking',['trackings'=>$trackings,'order'=>$order]);
        }
    }

    public function coverageArea()
    {
        return view('merchants.coverage-area');
    }
}
