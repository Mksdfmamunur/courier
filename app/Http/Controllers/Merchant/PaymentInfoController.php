<?php

namespace App\Http\Controllers\Merchant;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class PaymentInfoController extends Controller
{
    public function index()
    {
        return view('merchants.payment.info');
    }

    public function edit($id)
    {

    }

    public function update(Request $request)
    {

    }
}
