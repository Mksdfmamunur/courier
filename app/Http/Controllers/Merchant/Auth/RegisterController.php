<?php

namespace App\Http\Controllers\Merchant\Auth;

use App\Http\Controllers\Controller;
use App\Mail\MerchantForgotPassword;
use App\Mail\MerchantRegistration;
use App\Mail\MerchantResetPassword;
use App\Mail\MerchantVerification;
use App\Merchant;
use App\MerchantPaymentInformation;
use App\MerchantShop;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;

class RegisterController extends Controller
{
    protected $registerClass;
    public function __construct()
    {

        $this->registerClass = new \App\BLL\MerchantLogic();
    }
    public function showMerchantRegisterForm()
    {
        return view('merchants.auth.registration');
    }
    public function submitMerchantRegisterForm(Request $request)
    {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ@#';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < 8; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        $password = $randomString;
        $data = $this->registerClass->register($request,$password);

            if($data==true)
            {
                $email = $request->email;
                Mail::to($email)->send(new MerchantRegistration());
                toastr()->success('Your account has been created successfully');
                return redirect()->route('merchant.registration-success');
            }
        toastr()->error('You are already registered.');
        return redirect()->back();
    }

    public function registrationSuccess()
    {
        return view('merchants.auth.verification');
    }
    public function forgot()
    {
        return view('merchants.auth.forgot-password');
    }
    public function forgotPassword(Request $request)
    {
        $merchant = Merchant::where('email',$request->email)->where('status',"approved")->first();
        if (empty($merchant))
        {
            toastr()->error('Email address not matched.');
            return redirect()->back();
        }
        else{
            $reset_token =bin2hex(random_bytes(20));
            $merchant->password_reset_token = $reset_token;
            $merchant->save();
            $id = encrypt($merchant->id);
            $data = array(
                'token'   =>   $reset_token,
                'email'   =>   $request->email
            );
            $email = $request->email;
            Mail::to($email)->send(new MerchantResetPassword($data));
            toastr()->success('Reset link successfully send your email!');
            return redirect()->route('merchant.reset-success',compact('id'));
        }
    }

    public function resetSuccess(Request $request)
    {
        $id = decrypt($request->id);
        $merchant = Merchant::where('id',$id)->first();
        return view('merchants.auth.reset-confirm',compact('merchant'));
    }
    public function reset(Request $request)
    {
        $token = decrypt($request->id);
        $email = decrypt($request->email);
        $merchant = Merchant::where('email',$email)->where('password_reset_token',$token)->first();
        if (empty($merchant))
        {
            toastr()->error('Something is wrong');
            return redirect()->route('merchant.login');
        }

        return view('merchants.auth.reset-password',compact('merchant'));
    }
    public function resetPassword(Request $request)
    {
        $id = $request->id;
        $password = $request->password;
        $merchant = Merchant::where('id',$id)->first();
        $merchant->password = Hash::make($password);
        $merchant->password_reset_token = null;
        $merchant->save();
        toastr()->success('Your password changed Successfully');
        return redirect()->route('merchant.login');
    }

}
