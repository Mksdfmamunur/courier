<?php

namespace App\Http\Controllers\Merchant\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    public function __construct()
    {

    }

    public function showMerchantLoginForm()
    {
//        return view('auth.merchantLogin', ['url' => 'merchant']);
        return view('merchants.auth.login', [route('merchant.login')]);
    }

    public function merchantLoginSubmit(Request $request)
    {
        $request->validate([
            'email' => 'required|email',
            'password' => 'required|min:6'
        ]);

        if (Auth::guard('merchant')->attempt(['email' => $request->email, 'password' => $request->password], $request->remember)) {
            toastr()->success('Welcome merchant dashboard.');
            return redirect()->route('merchant.dashboard');
        }
        toastr()->error('Credentials does not match.');
        return redirect()->back()->withInput($request->only('email', 'remember'));

    }

    public function merchantLogout(Request $request)
    {
        Auth::guard('merchant')->logout();

        return redirect()->route('merchant.login');
    }
}