<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Order;
use App\TrackOrder;
use Illuminate\Http\Request;

class AdminController extends Controller
{
    public function index()
    {
        return view('admins.dashboard');
    }

    public function trackOrder(Request $request)
    {
        $trackingId = $request->get('trackingId');

        $order = Order::where('tracking_id',$trackingId)->first();

        if (empty($order))
        {
            $trackings =null;
            return view('admins.tracking',['trackings'=>$trackings]);
        }
        else{
            $trackings = TrackOrder::where('tracking_id',$order->tracking_id)->orderBy('id','desc')->get();
            return view('admins.tracking',['trackings'=>$trackings,'order'=>$order]);
        }
    }
}
