<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Mail\MerchantVerification;
use App\Merchant;
use App\MerchantPlan;
use App\MerchantShop;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;

class MerchantController extends Controller
{
    public function approve()
    {
        return view('admins.merchants.approved');
    }
    public function reject()
    {
        return view('admins.merchants.rejected');
    }

    public function request()
    {
        return view('admins.merchants.request');
    }
    public function status(Request $request)
    {
        $merchant = Merchant::where('id',$request->id)->first();
        if ($request->status == "Approved")
        {
            $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ@#';
            $charactersLength = strlen($characters);
            $randomString = '';
            for ($i = 0; $i < 8; $i++) {
                $randomString .= $characters[rand(0, $charactersLength - 1)];
            }
            $password = $randomString;

            $merchant->status = $request->status;
            $merchant->password = Hash::make($password);
            $data = array(
                'password'      =>  $password,
                'email'     =>  $merchant->email
            );
            Mail::to($merchant->email)->send(new MerchantVerification($data));
            $merchant->save();
            $shop = MerchantShop::where('merchant_id',$merchant->id)->first();
            $merchantPlan= new MerchantPlan;
            $merchantPlan->plan_id =1;
            $merchantPlan->shop_id =$shop->id;
            $merchantPlan->merchant_id = $merchant->id;
            $merchantPlan->save();
            toastr()->success('Merchant account verified successfully');
            return redirect()->back();
        }
        if ($request->status == "Rejected")
        {
            $merchant->status ="Rejected";
            toastr()->error('Merchant account rejected successfully');
            return redirect()->back();
        }
    }
    public function plan()
    {
        return view('admins.merchants.plan');
    }
    public function updatePlan(Request $request)
    {
            $merchantPlan= MerchantPlan::where('id',$request->id)->first();
            $merchantPlan->plan_id =$request->plan_id;
            $merchantPlan->save();
            toastr()->success('Merchant plan update successfully');
            return redirect()->back();
    }
}
