<?php

namespace App\Http\Controllers\Admin;

use App\Area;
use App\City;
use App\CoverageArea;
use App\Http\Controllers\Controller;
use App\MerchantPlan;
use App\MerchantShop;
use App\Order;
use App\Plan;
use App\RiderPayment;
use App\Status;
use App\TrackOrder;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class OrderController extends Controller
{
    public function view()
    {
        return view('admins.orders.view');
    }
    public function pending()
    {
        return view('admins.orders.pending');
    }
    public function confirm()
    {
        return view('admins.orders.confirm');
    }
    public function shipped()
    {
        return view('admins.orders.shipped');
    }
    public function delivered()
    {
        return view('admins.orders.delivered');
    }
    public function print($id)
    {
        $order =Order::where('id',decrypt($id))->first();
        return view('admins.orders.print')->with(['order'=>$order]);
    }
    public function edit($id)
    {
        $order = Order::findOrFail(decrypt($id));
        return view('admins.orders.edit',compact('order'));
    }
    public function searchShop(Request $request)
    {
        $shop = MerchantShop::where('id',$request->shop_id)->first();
        $area = Area::where('id',$shop->pickup_area)->first();
        $city = City::where('id',$area->city_id)->first();
        return response()->json(['shop'=>$shop,'area'=>$area ,'city'=>$city]);
    }
    public function deliveryChargePlan(Request $request)
    {
        $recipientArea = CoverageArea::where('id',$request->recipient_area)->first();
        $orderWeight = $request->order_weight;
        $deliveryTime = $request->delivery_time;
        $amountToCollect = $request->amount_to_collect;
        $merchantPlan = MerchantPlan::where('shop_id',$request->shop_id)->first();
        $deliveryPlan= Plan::where('id',$merchantPlan->plan_id)->first();
        return response()->json(['deliveryPlan'=>$deliveryPlan,'deliveryTime'=>$deliveryTime,'recipientArea'=>$recipientArea,'orderWeight'=>$orderWeight,'amountToCollect'=>$amountToCollect]);
    }
    public function fetchDelivery(Request $request)
    {
        $value = $request->recipient_area;
        $data = CoverageArea::where('id', $value)->first();
        $output="";
        if ($data->division == "Inside Dhaka")
        {
            $output .= '<option value="idr" selected>Regular Delivery (24-48 Hours)</option>';
            $output .= '<option value="idu">Urgent Delivery (8-12 Hours)</option>';
        }
        else if ($data->division == "Inside Dhaka")
        {
            $output .= '<option value="ds" selected>Regular Delivery (48-72 Hours)</option>';
        }
        else{
            $output .= '<option value="od" selected>Regular Delivery (72-96 Hours)</option>';
        }

        echo $output;
    }
    public function update(Request $request)
    {
        $order = Order::where('id',$request->id)->first();
        $order->shop_id = $request->shop_id;
        $order->order_type = $request->order_type;
        $order->merchant_order_id = $request->merchant_order_id;
        $order->recipient_name = $request->recipient_name;
        $order->recipient_phone = $request->recipient_phone;
        $order->recipient_address = $request->recipient_address;
        $order->recipient_area = $request->recipient_area;
        $order->delivery_time = $request->delivery_time;
        $order->order_weight = $request->order_weight;
        $order->amount_to_collect = $request->amount_to_collect;
        $order->delivery_charge = $request->delivery_charge;
        $order->cod_charge = $request->cod_charge;
        $order->total_charge = $request->delivery_charge+$request->cod_charge;
        $order->paid_out = $request->paid_out;
        $order->merchant_instruction = $request->merchant_instruction;
        $order->save();
        toastr()->success("Order updated successfully.");
        return redirect()->route('admin.order.view');
    }
    public function searchData(Request $request)
    {
        $page = $request->page;
        $date = $request->date;
        $tracking_id = $request->tracking_id;
        $receiver = $request->receiver;
        $shop = $request->shop;
        $delivery_status = $request->delivery_status;
        $parts = explode(' - ' , $date);
        $date_from = Carbon::createFromFormat('d/m/Y', $parts[0])->format('Y-m-d');
        $date_to = Carbon::createFromFormat('d/m/Y', $parts[1])->format('Y-m-d');
        if ($tracking_id!=null && $receiver!=null && $shop!="0" && $delivery_status!="0")
        {
            $orders = Order::whereDate('created_at','>=',$date_from)
                ->whereDate('created_at','<=',$date_to)
                ->where('tracking_id','like','%' .$tracking_id.'%')
                ->where('shop_id',$shop)
                ->where('delivery_status',$delivery_status)
                ->where(function ($query) use ($receiver) {
                    $query->where('recipient_name', 'like', '%' . $receiver . '%')
                        ->orWhere('recipient_phone', 'like', '%' . $receiver . '%');
                })
                ->orderBy('id','desc')->paginate(50);
        }
        else if ($tracking_id!=null && $receiver!=null && $shop!="0" && $delivery_status=="0")
        {
            $orders = Order::whereDate('created_at','>=',$date_from)
                ->whereDate('created_at','<=',$date_to)
                ->where('tracking_id','like','%' .$tracking_id.'%')
                ->where('shop_id',$shop)
                ->where(function ($query) use ($receiver) {
                    $query->where('recipient_name', 'like', '%' . $receiver . '%')
                        ->orWhere('recipient_phone', 'like', '%' . $receiver . '%');
                })
                ->orderBy('id','desc')->paginate(50);
        }
        else if ($tracking_id!=null && $receiver!=null && $shop=="0" && $delivery_status!="0")
        {
            $orders = Order::whereDate('created_at','>=',$date_from)
                ->whereDate('created_at','<=',$date_to)
                ->where('tracking_id','like','%' .$tracking_id.'%')
                ->where('delivery_status',$delivery_status)
                ->where(function ($query) use ($receiver) {
                    $query->where('recipient_name', 'like', '%' . $receiver . '%')
                        ->orWhere('recipient_phone', 'like', '%' . $receiver . '%');
                })
                ->orderBy('id','desc')->paginate(50);
        }
        else if ($tracking_id!=null && $receiver==null && $shop!="0" && $delivery_status!="0")
        {
            $orders = Order::whereDate('created_at','>=',$date_from)
                ->whereDate('created_at','<=',$date_to)
                ->where('tracking_id','like','%' .$tracking_id.'%')
                ->where('shop_id',$shop)
                ->where('delivery_status',$delivery_status)
                ->orderBy('id','desc')->paginate(50);
        }
        else if ($tracking_id==null && $receiver!=null && $shop!="0" && $delivery_status!="0")
        {
            $orders = Order::whereDate('created_at','>=',$date_from)
                ->whereDate('created_at','<=',$date_to)
                ->where('shop_id',$shop)
                ->where('delivery_status',$delivery_status)
                ->where(function ($query) use ($receiver) {
                    $query->where('recipient_name', 'like', '%' . $receiver . '%')
                        ->orWhere('recipient_phone', 'like', '%' . $receiver . '%');
                })
                ->orderBy('id','desc')->paginate(50);
        }
        else if ($tracking_id==null && $receiver==null && $shop!="0" && $delivery_status!="0")
        {
            $orders = Order::whereDate('created_at','>=',$date_from)
                ->whereDate('created_at','<=',$date_to)
                ->where('shop_id',$shop)
                ->where('delivery_status',$delivery_status)
                ->orderBy('id','desc')->paginate(50);
        }
        else if ($tracking_id!=null && $receiver!=null && $shop=="0" && $delivery_status=="0")
        {
            $orders = Order::whereDate('created_at','>=',$date_from)
                ->whereDate('created_at','<=',$date_to)
                ->where('tracking_id','like','%' .$tracking_id.'%')
                ->where(function ($query) use ($receiver) {
                    $query->where('recipient_name', 'like', '%' . $receiver . '%')
                        ->orWhere('recipient_phone', 'like', '%' . $receiver . '%');
                })
                ->orderBy('id','desc')->paginate(50);
        }
        else if ($tracking_id==null && $receiver!=null && $shop=="0" && $delivery_status!="0")
        {
            $orders = Order::whereDate('created_at','>=',$date_from)
                ->whereDate('created_at','<=',$date_to)
                ->where('delivery_status',$delivery_status)
                ->where(function ($query) use ($receiver) {
                    $query->where('recipient_name', 'like', '%' . $receiver . '%')
                        ->orWhere('recipient_phone', 'like', '%' . $receiver . '%');
                })
                ->orderBy('id','desc')->paginate(50);
        }
        else if ($tracking_id==null && $receiver!=null && $shop!="0" && $delivery_status=="0")
        {
            $orders = Order::whereDate('created_at','>=',$date_from)
                ->whereDate('created_at','<=',$date_to)
                ->where('shop_id',$shop)
                ->where(function ($query) use ($receiver) {
                    $query->where('recipient_name', 'like', '%' . $receiver . '%')
                        ->orWhere('recipient_phone', 'like', '%' . $receiver . '%');
                })
                ->orderBy('id','desc')->paginate(50);
        }
        else if ($tracking_id!=null && $receiver==null && $shop=="0" && $delivery_status!="0")
        {
            $orders = Order::whereDate('created_at','>=',$date_from)
                ->whereDate('created_at','<=',$date_to)
                ->where('tracking_id','like','%' .$tracking_id.'%')
                ->where('delivery_status',$delivery_status)
                ->orderBy('id','desc')->paginate(50);
        }
        else if ($tracking_id!=null && $receiver==null && $shop!="0" && $delivery_status=="0")
        {
            $orders = Order::whereDate('created_at','>=',$date_from)
                ->whereDate('created_at','<=',$date_to)
                ->where('tracking_id','like','%' .$tracking_id.'%')
                ->where('shop_id',$shop)
                ->orderBy('id','desc')->paginate(50);
        }
        else if ($tracking_id!=null && $receiver==null && $shop=="0" && $delivery_status=="0")
        {
            $orders = Order::whereDate('created_at','>=',$date_from)
                ->whereDate('created_at','<=',$date_to)
                ->where('tracking_id','like','%' .$tracking_id.'%')
                ->orderBy('id','desc')->paginate(50);
        }
        else if ($tracking_id==null && $receiver!=null && $shop=="0" && $delivery_status=="0" )
        {
            $orders = Order::whereDate('created_at','>=',$date_from)
                ->whereDate('created_at','<=',$date_to)
                ->where('recipient_name', 'like', '%' . $receiver . '%')
                ->orWhere('recipient_phone', 'like', '%' . $receiver . '%')
                ->orderBy('id','desc')->paginate(50);
        }
        else if ($tracking_id==null && $receiver==null && $shop!="0" && $delivery_status=="0")
        {
            $orders = Order::whereDate('created_at','>=',$date_from)
                ->whereDate('created_at','<=',$date_to)
                ->where('shop_id',$shop)
                ->orderBy('id','desc')->paginate(50);
        }
        else if ($tracking_id==null && $receiver==null && $shop=="0" &&$delivery_status!="0")
        {
            $orders = Order::whereDate('created_at','>=',$date_from)
                ->whereDate('created_at','<=',$date_to)
                ->where('delivery_status',$delivery_status)
                ->orderBy('id','desc')->paginate(50);
        }
        else
        {
            $orders = Order::whereDate('created_at','>=',$date_from)
                ->whereDate('created_at','<=',$date_to)
                ->orderBy('id','desc')->paginate(50);
        }
        $data =  view('admins.orders.order-data',['orders' => $orders])->render();
        return response()->json(['success' =>$data]);
    }

    public function fetchData(Request $request)
    {
        $search = $request->search;
        $delivery_status = $request->delivery_status;
        $order = Order::where('delivery_status',$delivery_status)
            ->where(function ($query)  use ($search){
                $query->where('recipient_phone', $search)
                    ->orWhere('tracking_id', $search);
            })
            ->first();

        if (empty($order))
        {
            return response()->json($order);
        }
        $shop = MerchantShop::where('id',$order->shop_id)->first();
        $coverageArea = CoverageArea::where('id',$order->recipient_area)->first();
        return response()->json(['order'=>$order,'shop'=>$shop,'coverageArea'=>$coverageArea]);
    }
    public function fetchDataAssign(Request $request)
    {
        $search = $request->search;
        $delivery_status = $request->delivery_status;
        $delivery_status1 = $request->delivery_status1;
        $order = Order::where(function ($query)  use ($delivery_status,$delivery_status1){
            $query->where('delivery_status', $delivery_status)
                ->orWhere('delivery_status', $delivery_status1);
        })
            ->where(function ($query)  use ($search){
                $query->where('recipient_phone', $search)
                    ->orWhere('tracking_id', $search);
            })
            ->first();

        if (empty($order))
        {
            return response()->json($order);
        }
        $shop = MerchantShop::where('id',$order->shop_id)->first();
        $coverageArea = CoverageArea::where('id',$order->recipient_area)->first();
        return response()->json(['order'=>$order,'shop'=>$shop,'coverageArea'=>$coverageArea]);
    }
    public function status(Request $request)
    {
        $id1 = $request->id;
        if ($id1=="")
        {
            toastr()->error("Order not added.");
            return redirect()->back();
        }
        $id = array_unique($id1);
        for ($i=0;$i<count($id);$i++)
        {
            $order = Order::where('id',$id[$i])->first();
            $order->delivery_status = $request->delivery_status;
            $order->save();
            $tracking = new TrackOrder;
            $tracking->tracking_id = $order->tracking_id;
            $tracking->status_id =$request->delivery_status;
            if ($request->delivery_status==5)
            {
                $characters = '0123456789';
                $charactersLength = strlen($characters);
                $randomString = '';
                for ($k = 0; $k < 8; $k++) {
                    $randomString .= $characters[rand(0, $charactersLength - 1)];
                }
                $date= Carbon::now();
                $date1 = $date->isoFormat('YYMMD');
                $invoiceId = $date1.$randomString;
                $tracking->rider_id =$request->rider;
                $riderPayment = RiderPayment::where('rider_id',$request->rider)->whereDate('created_at',Carbon::today())->first();
                if ($riderPayment)
                {
                    $riderPayment->total_assigned  = $riderPayment->total_assigned+1;
                    $riderPayment->save();
                }
                else{
                    $riderPayment1 = new RiderPayment;
                    $riderPayment1->invoice_id = $invoiceId;
                    $riderPayment1->rider_id = $request->rider;
                    $riderPayment1->total_assigned = 1;
                    $riderPayment1->save();
                }
            }

            if ($request->delivery_status==6)
            {
                $trackOrder = TrackOrder::whereDate('created_at',Carbon::today())->where('tracking_id',$order->tracking_id)->orderBy('updated_at','desc')->first();
                $tracking->rider_id =$trackOrder->rider_id;
                $riderPayment = RiderPayment::where('rider_id',$trackOrder->rider_id)->whereDate('created_at',Carbon::today())->first();
                $riderPayment->total_hold = $riderPayment->total_hold+1;
                $riderPayment->save();
            }
            if ($request->delivery_status==4)
            {
                $trackOrder = TrackOrder::whereDate('created_at',Carbon::today())->where('tracking_id',$order->tracking_id)->orderBy('updated_at','desc')->first();
                $tracking->rider_id =$trackOrder->rider_id;
                $riderPayment = RiderPayment::where('rider_id',$trackOrder->rider_id)->whereDate('created_at',Carbon::today())->first();
                $riderPayment->total_delivered = $riderPayment->total_delivered+1;
                $riderPayment->save();
            }
            if ($request->delivery_status==13)
            {
                $trackOrder = TrackOrder::whereDate('created_at',Carbon::today())->where('tracking_id',$order->tracking_id)->orderBy('updated_at','desc')->first();
                $tracking->rider_id =$trackOrder->rider_id;
                $riderPayment = RiderPayment::where('rider_id',$trackOrder->rider_id)->whereDate('created_at',Carbon::today())->first();
                $riderPayment->total_failed = $riderPayment->total_failed+1;
                $riderPayment->save();
            }

            $tracking->save();
        }

        toastr()->success("Order status change successfully.");
        return redirect()->back();


    }
    public function singleStatus(Request $request)
    {
        $id = $request->id;
        $order = Order::where('id',$id)->first();
        $order->delivery_status = $request->delivery_status;
        $order->save();
        $tracking = new TrackOrder;
        $tracking->tracking_id = $order->tracking_id;
        $tracking->status_id =$request->delivery_status;
        $tracking->save();
        toastr()->success("Order status change successfully.");
        return redirect()->back();
    }
}
