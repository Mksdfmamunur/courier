<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\MerchantPayment;
use App\Order;
use App\RiderPayment;
use Carbon\Carbon;
use Illuminate\Http\Request;

class PaymentController extends Controller
{
    public function riderPayment()
    {
        return view('admins.payments.rider-payment');
    }
    public function riderPaymentStore(Request $request)
    {
        $id = $request->id;
        $paidAmount = $request->paid_amount;
        $paymentType = $request->payment_type;
        $reference = $request->reference;
        $riderPayment = RiderPayment::where('id',$id)->first();
        $riderPayment->paid_amount= $paidAmount;
        $riderPayment->payment_type= $paymentType;
        $riderPayment->reference= $reference;
        $riderPayment->status= "Paid";
        $riderPayment->save();
        toastr()->success('Rider payment successfully');
        return redirect()->back();
    }

    public function riderPaymentList()
    {
        return view('admins.payments.rider-payment-list');
    }

    public function createMerchantPaymentInvoice()
    {
        return view('admins.payments.merchant-payment-invoice');
    }

    public function createMerchantPaymentInvoiceStore(Request $request)
    {
        $characters = '0123456789';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < 8; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        $date= Carbon::now();
        $date1 = $date->isoFormat('YYMMD');
        $invoiceId = $date1.$randomString;

        $merchantPayment = new MerchantPayment;
        $merchantPayment->invoice_id = $invoiceId;
        $merchantPayment->merchant_id = $request->merchant_id;
        $merchantPayment->order_id = json_encode($request->order_id);
        $merchantPayment->total_charge = $request->total_charge;
        $merchantPayment->total_paid_amount = $request->total_paid_amount;
        $merchantPayment->collected_amount = $request->collected_amount;
        $merchantPayment->save();
        for ($i=0;$i<count($request->order_id);$i++)
        {
            $order = Order::where('id',$request->order_id[$i])->first();
            $order->payment_status = "Processing";
            $order->save();
        }
        toastr()->success('Merchant payment invoice created successfully');
        return redirect()->back();
    }
    public function merchantPaymentInvoiceList()
    {
        return view('admins.payments.merchant-payment-invoice-list');
    }
}
