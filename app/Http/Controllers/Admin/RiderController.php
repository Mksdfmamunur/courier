<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Merchant;
use App\Rider;
use Carbon\Carbon;
use Illuminate\Http\Request;

class RiderController extends Controller
{
    public function index()
    {
        return view('admins.riders.view');
    }

    public function register()
    {
        return view('admins.riders.register');
    }

    public function store(Request $request)
    {
        if (empty(Rider::where('email',$request->email)->first()) ) {
            $pv= Carbon::now()->toDateString() . "-" . uniqid();
            $rider_verification_id_pic = $request->verification_id_pic;
            $imageName = $rider_verification_id_pic->getClientOriginalName();
            $extension = pathinfo($imageName, PATHINFO_EXTENSION);
            $imagemove = $rider_verification_id_pic->move('public/storage/rider/verification_image', $pv . "." . $extension);
            $name = array("photo" => $pv. "." . $extension,);
            $verificationIdPic = asset('public/storage/rider/verification_image/'. $pv . "." . $extension);

            $pp=Carbon::now()->toDateString() . "-" . uniqid();
            $rider_profile_pic = $request->profile_pic;
            $imageName = $rider_profile_pic->getClientOriginalName();
            $extension = pathinfo($imageName, PATHINFO_EXTENSION);
            $imagemove = $rider_profile_pic->move('public/storage/rider/profile_image',  $pp. "." . $extension);
            $name = array("photo" => $pp . "." . $extension,);
            $profilePic = asset('public/storage/rider/profile_image/'. $pp . "." . $extension);


            $rider = new Rider;
            $rider->name =$request->name;
            $rider->nick_name =$request->nick_name;
            $rider->email =$request->email;
            $rider->phone =$request->phone;
            $rider->father_name =$request->father_name;
            $rider->mother_name =$request->mother_name;
            $rider->present_address =$request->present_address;
            $rider->permanent_address =$request->permanent_address;
            $rider->verification_id_type =$request->verification_id_type;
            $rider->verification_id_number =$request->verification_id_number;
            $rider->verification_id_pic =$verificationIdPic;
            $rider->profile_pic =$profilePic;
            $rider->referrer_name =$request->referrer_name;
            $rider->referrer_phone =$request->referrer_phone;
            $rider->referrer_address =$request->referrer_address;
            $rider->save();
            toastr()->success('Rider registered successfully');
            return redirect()->back();
        }
        toastr()->error('Rider already registered.');
        return redirect()->back();
    }

    public function edit($id)
    {
        $rider = Rider::findOrFail(decrypt($id));
        return view('admins.riders.edit',compact('rider'));
    }

    public function update(Request $request)
    {
        $rider = Rider::where('id',$request->id)->first();
        $rider->name =$request->name;
        $rider->nick_name =$request->nick_name;
        $rider->email =$request->email;
        $rider->phone =$request->phone;
        $rider->father_name =$request->father_name;
        $rider->mother_name =$request->mother_name;
        $rider->present_address =$request->present_address;
        $rider->permanent_address =$request->permanent_address;
        $rider->verification_id_type =$request->verification_id_type;
        $rider->verification_id_number =$request->verification_id_number;
        $rider->referrer_name =$request->referrer_name;
        $rider->referrer_phone =$request->referrer_phone;
        $rider->referrer_address =$request->referrer_address;
        $rider->save();

        toastr()->success("Rider updated successfully.");
        return redirect()->route('admin.rider.view');
    }


}
