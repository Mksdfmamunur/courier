<?php

namespace App\Http\Controllers\Admin;

use App\Area;
use App\City;
use App\CoverageArea;
use App\Http\Controllers\Controller;
use App\Plan;
use Illuminate\Http\Request;

class BusinessSettingController extends Controller
{
    public function plan()
    {
        return view('admins.business-settings.plan');
    }
    public function storePlan(Request $request)
    {

        if (empty(Plan::where('plan_name',$request->plan_name)->first()) ) {
            $plan = new Plan;
            $plan->plan_name =$request->plan_name;
            $plan->idr_charge =$request->idr_charge;
            if ($request->idr_cod=="on")
            {
                $plan->idr_cod =1;
            }
            else
            {
                $plan->idr_cod =0;
            }
            $plan->idu_charge =$request->idu_charge;
            if ($request->idr_cod=="on")
            {
                $plan->idu_cod =1;
            }
            else
            {
                $plan->idu_cod =0;
            }
            $plan->ds_charge =$request->ds_charge;
            if ($request->ds_cod=="on")
            {
                $plan->ds_cod =1;
            }
            else
            {
                $plan->ds_cod =0;
            }
            $plan->od_charge =$request->od_charge;
            if ($request->od_cod=="on")
            {
                $plan->od_cod =1;
            }
            else
            {
                $plan->od_cod =0;
            }
            $plan->cpw =$request->cpw;
            $plan->save();
            toastr()->success('Plan created successfully');
            return redirect()->back();
        }
        toastr()->error('Plan name already exist.');
        return redirect()->back();
    }
    public function updatePlan(Request $request)
    {
        $plan = Plan::where('id',$request->id)->first();
        if ($plan->plan_name!=$request->plan_name)
        {
            $plan->plan_name =$request->plan_name;
        }

            $plan->idr_charge =$request->idr_charge;
            if ($request->idr_cod=="on")
            {
                $plan->idr_cod =1;
            }
            else
            {
                $plan->idr_cod =0;
            }
            $plan->idu_charge =$request->idu_charge;
            if ($request->idr_cod=="on")
            {
                $plan->idu_cod =1;
            }
            else
            {
                $plan->idu_cod =0;
            }
            $plan->ds_charge =$request->ds_charge;
            if ($request->ds_cod=="on")
            {
                $plan->ds_cod =1;
            }
            else
            {
                $plan->ds_cod =0;
            }
            $plan->od_charge =$request->od_charge;
            if ($request->od_cod=="on")
            {
                $plan->od_cod =1;
            }
            else
            {
                $plan->od_cod =0;
            }
            $plan->cpw =$request->cpw;
            $plan->save();
            toastr()->success("Plan updated successfully.");
            return redirect()->back();
        }
    public function coverageArea()
    {
        return view('admins.business-settings.coverage-area');
    }
    public function StoreCoverageArea(Request $request)
    {
        $id = $request->area;
        $area = Area::where('id',$id)->first();
        $city = City::where('id',$area->city_id)->first();
        $coverageArea1 = CoverageArea::where('area',$area->area_name)->where('city',$city->city_name)->first();
        $coverageArea = new CoverageArea;
        if (!empty($coverageArea1))
        {
            toastr()->error("Coverage area already Exists.");
            return redirect()->back();
        }
        $coverageArea->city = $city->city_name;
        $coverageArea->area = $area->area_name;
        $coverageArea->division = $request->division;
        $coverageArea->save();
        toastr()->success("Coverage area create successfully.");
        return redirect()->back();
    }
    public function status(Request $request){
        if($request->ajax())
        {
            $coverageArea = CoverageArea::where('id',$request->id)->first();
            $coverageArea->status = $request->status;
            $coverageArea->save();

        }
    }
}
