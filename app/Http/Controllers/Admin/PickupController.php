<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Order;
use App\Status;
use App\TrackOrder;
use Illuminate\Http\Request;

class PickupController extends Controller
{
    public function approve()
    {
        return view('admins.pickups.approve');
    }
    public function list()
    {
        return view('admins.pickups.list');
    }
    public function status(Request $request)
    {
        $id1 = $request->id;
        if ($id1=="")
        {
            toastr()->error("Order not added.");
            return redirect()->back();
        }
        $id = array_unique($id1);
        for ($i=0;$i<count($id);$i++)
        {
            for ($i=0;$i<count($id);$i++)
            {
                $order = Order::where('id',$id[$i])->first();
                $order->delivery_status = $request->delivery_status;
                $order->save();
                $tracking = new TrackOrder;
                $tracking->tracking_id = $order->tracking_id;
                $tracking->status_id=$request->delivery_status;
                $tracking->save();
            }
            toastr()->success("Order status change successfully.");
            return redirect()->back();
        }
    }
}
