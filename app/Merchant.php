<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class Merchant extends Authenticatable
{
    use Notifiable;
    protected $guard = 'merchant';
    protected $fillable = [
        'first_name', 'email',
    ];
    protected $hidden = [
        'password','password_reset_token' ,'remember_token',
    ];

    public function paymentInformation()
    {
        return $this->hasOne('App\MerchantPaymentInformation');
    }
    public function shops()
    {
        return $this->hasMany('App\MerchantShop');
    }
}
