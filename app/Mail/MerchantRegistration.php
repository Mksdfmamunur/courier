<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class MerchantRegistration extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $address = 'info@metroexpress.com.bd';
//        $address = 'mksdf008@gmail.com';
        $subject = 'Welcome to MetroExpress!';
        $name = 'MetroExpress | No-reply';
        return $this->from($address,$name)->subject($subject)->view('mail.merchant-registration');
    }
}
