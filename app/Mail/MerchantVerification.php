<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class MerchantVerification extends Mailable
{
    use Queueable, SerializesModels;
    public $data;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $address = 'info@metroexpress.com.bd';
        $subject = 'Account verified successfully';
        $name = 'MetroExpress | No-reply';
        return $this->from($address,$name)->subject($subject)->view('mail.merchant-verification')->with('data', $this->data);
    }
}
