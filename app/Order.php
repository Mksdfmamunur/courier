<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    public function merchantShop()
    {
        return $this->belongsTo('App\MerchantShop','shop_id','id');
    }

    public function status()
    {
        return $this->hasOne('App\Status','id','delivery_status');
    }
    public function coverageArea()
    {
        return $this->hasOne('App\CoverageArea','id','recipient_area');
    }
}
