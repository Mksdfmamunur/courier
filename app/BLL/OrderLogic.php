<?php


namespace App\BLL;



use App\Order;
use App\Status;
use App\TrackOrder;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;

class OrderLogic
{
    public function createOrder($request)
    {
        $characters = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < 8; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        $date= Carbon::now();
        $date1 = $date->isoFormat('YYMMD');
        $tracking_id = $date1.$randomString;

        $order = new Order;
        $order->merchant_id = Auth::id();
        $order->shop_id = $request->shop_id;
        $order->order_type = $request->order_type;
        $order->tracking_id = $tracking_id;
        $order->merchant_order_id = $request->merchant_order_id;
        $order->recipient_name = $request->recipient_name;
        $order->recipient_phone = $request->recipient_phone;
        $order->recipient_address = $request->recipient_address;
        $order->recipient_area = $request->recipient_area;
        $order->delivery_time = $request->delivery_time;
        $order->order_weight = $request->order_weight;
        $order->amount_to_collect = $request->amount_to_collect;
        $order->delivery_charge = $request->delivery_charge;
        $order->cod_charge = $request->cod_charge;
        $order->total_charge = $request->delivery_charge+$request->cod_charge;
        $order->paid_out = $request->paid_out;
        $order->merchant_instruction = $request->merchant_instruction;
        $order->delivery_status = 1;
        $order->delivery_status_date = Carbon::now();
        $order->payment_status = "Unpaid";
        $order->payment_date = Carbon::now();
        $order->save();
        $tracking = new TrackOrder;
        $tracking->tracking_id = Order::pluck('tracking_id')->last();
        $tracking->status_id =1;
        $tracking->save();
    }

    public function updateOrder($request)
    {
        $order = Order::where('id',$request->id)->first();
        $order->shop_id = $request->shop_id;
        $order->order_type = $request->order_type;
        $order->merchant_order_id = $request->merchant_order_id;
        $order->recipient_name = $request->recipient_name;
        $order->recipient_phone = $request->recipient_phone;
        $order->recipient_address = $request->recipient_address;
        $order->recipient_area = $request->recipient_area;
        $order->delivery_time = $request->delivery_time;
        $order->order_weight = $request->order_weight;
        $order->amount_to_collect = $request->amount_to_collect;
        $order->delivery_charge = $request->delivery_charge;
        $order->cod_charge = $request->cod_charge;
        $order->total_charge = $request->delivery_charge+$request->cod_charge;
        $order->paid_out = $request->paid_out;
        $order->merchant_instruction = $request->merchant_instruction;
        $order->save();
    }

    public function searchOrder($request)
    {
        $page = $request->page;
        $date = $request->date;
        $tracking_id = $request->tracking_id;
        $receiver = $request->receiver;
        $shop = $request->shop;
        $delivery_status = $request->delivery_status;
        $parts = explode(' - ' , $date);
        $date_from = Carbon::createFromFormat('d/m/Y', $parts[0])->format('Y-m-d');
        $date_to = Carbon::createFromFormat('d/m/Y', $parts[1])->format('Y-m-d');
        if ($tracking_id!=null && $receiver!=null && $shop!="0" && $delivery_status!="0")
        {
            $orders = Order::where('merchant_id',Auth::id())
                ->whereDate('created_at','>=',$date_from)
                ->whereDate('created_at','<=',$date_to)
                ->where('tracking_id','like','%' .$tracking_id.'%')
                ->where('shop_id',$shop)
                ->where('delivery_status',$delivery_status)
                ->where(function ($query) use ($receiver) {
                    $query->where('recipient_name', 'like', '%' . $receiver . '%')
                        ->orWhere('recipient_phone', 'like', '%' . $receiver . '%');
                })
                ->orderBy('id','desc')->paginate(50);
        }
        else if ($tracking_id!=null && $receiver!=null && $shop!="0" && $delivery_status=="0")
        {
            $orders = Order::where('merchant_id',Auth::id())
                ->whereDate('created_at','>=',$date_from)
                ->whereDate('created_at','<=',$date_to)
                ->where('tracking_id','like','%' .$tracking_id.'%')
                ->where('shop_id',$shop)
                ->where(function ($query) use ($receiver) {
                    $query->where('recipient_name', 'like', '%' . $receiver . '%')
                        ->orWhere('recipient_phone', 'like', '%' . $receiver . '%');
                })
                ->orderBy('id','desc')->paginate(50);
        }
        else if ($tracking_id!=null && $receiver!=null && $shop=="0" && $delivery_status!="0")
        {
            $orders = Order::where('merchant_id',Auth::id())
                ->whereDate('created_at','>=',$date_from)
                ->whereDate('created_at','<=',$date_to)
                ->where('tracking_id','like','%' .$tracking_id.'%')
                ->where('delivery_status',$delivery_status)
                ->where(function ($query) use ($receiver) {
                    $query->where('recipient_name', 'like', '%' . $receiver . '%')
                        ->orWhere('recipient_phone', 'like', '%' . $receiver . '%');
                })
                ->orderBy('id','desc')->paginate(50);
        }
        else if ($tracking_id!=null && $receiver==null && $shop!="0" && $delivery_status!="0")
        {
            $orders = Order::where('merchant_id',Auth::id())
                ->whereDate('created_at','>=',$date_from)
                ->whereDate('created_at','<=',$date_to)
                ->where('tracking_id','like','%' .$tracking_id.'%')
                ->where('shop_id',$shop)
                ->where('delivery_status',$delivery_status)
                ->orderBy('id','desc')->paginate(50);
        }
        else if ($tracking_id==null && $receiver!=null && $shop!="0" && $delivery_status!="0")
        {
            $orders = Order::where('merchant_id',Auth::id())
                ->whereDate('created_at','>=',$date_from)
                ->whereDate('created_at','<=',$date_to)
                ->where('shop_id',$shop)
                ->where('delivery_status',$delivery_status)
                ->where(function ($query) use ($receiver) {
                    $query->where('recipient_name', 'like', '%' . $receiver . '%')
                        ->orWhere('recipient_phone', 'like', '%' . $receiver . '%');
                })
                ->orderBy('id','desc')->paginate(50);
        }
        else if ($tracking_id==null && $receiver==null && $shop!="0" && $delivery_status!="0")
        {
            $orders = Order::where('merchant_id',Auth::id())
                ->whereDate('created_at','>=',$date_from)
                ->whereDate('created_at','<=',$date_to)
                ->where('shop_id',$shop)
                ->where('delivery_status',$delivery_status)
                ->orderBy('id','desc')->paginate(50);
        }
        else if ($tracking_id!=null && $receiver!=null && $shop=="0" && $delivery_status=="0")
        {
            $orders = Order::where('merchant_id',Auth::id())
                ->whereDate('created_at','>=',$date_from)
                ->whereDate('created_at','<=',$date_to)
                ->where('tracking_id','like','%' .$tracking_id.'%')
                ->where(function ($query) use ($receiver) {
                    $query->where('recipient_name', 'like', '%' . $receiver . '%')
                        ->orWhere('recipient_phone', 'like', '%' . $receiver . '%');
                })
                ->orderBy('id','desc')->paginate(50);
        }
        else if ($tracking_id==null && $receiver!=null && $shop=="0" && $delivery_status!="0")
        {
            $orders = Order::where('merchant_id',Auth::id())
                ->whereDate('created_at','>=',$date_from)
                ->whereDate('created_at','<=',$date_to)
                ->where('delivery_status',$delivery_status)
                ->where(function ($query) use ($receiver) {
                    $query->where('recipient_name', 'like', '%' . $receiver . '%')
                        ->orWhere('recipient_phone', 'like', '%' . $receiver . '%');
                })
                ->orderBy('id','desc')->paginate(50);
        }
        else if ($tracking_id==null && $receiver!=null && $shop!="0" && $delivery_status=="0")
        {
            $orders = Order::where('merchant_id',Auth::id())
                ->whereDate('created_at','>=',$date_from)
                ->whereDate('created_at','<=',$date_to)
                ->where('shop_id',$shop)
                ->where(function ($query) use ($receiver) {
                    $query->where('recipient_name', 'like', '%' . $receiver . '%')
                        ->orWhere('recipient_phone', 'like', '%' . $receiver . '%');
                })
                ->orderBy('id','desc')->paginate(50);
        }
        else if ($tracking_id!=null && $receiver==null && $shop=="0" && $delivery_status!="0")
        {
            $orders = Order::where('merchant_id',Auth::id())
                ->whereDate('created_at','>=',$date_from)
                ->whereDate('created_at','<=',$date_to)
                ->where('tracking_id','like','%' .$tracking_id.'%')
                ->where('delivery_status',$delivery_status)
                ->orderBy('id','desc')->paginate(50);
        }
        else if ($tracking_id!=null && $receiver==null && $shop!="0" && $delivery_status=="0")
        {
            $orders = Order::where('merchant_id',Auth::id())
                ->whereDate('created_at','>=',$date_from)
                ->whereDate('created_at','<=',$date_to)
                ->where('tracking_id','like','%' .$tracking_id.'%')
                ->where('shop_id',$shop)
                ->orderBy('id','desc')->paginate(50);
        }
        else if ($tracking_id!=null && $receiver==null && $shop=="0" && $delivery_status=="0")
        {
            $orders = Order::where('merchant_id',Auth::id())
                ->whereDate('created_at','>=',$date_from)
                ->whereDate('created_at','<=',$date_to)
                ->where('tracking_id','like','%' .$tracking_id.'%')
                ->orderBy('id','desc')->paginate(50);
        }
        else if ($tracking_id==null && $receiver!=null && $shop=="0" && $delivery_status=="0" )
        {
            $orders = Order::where('merchant_id',Auth::id())
                ->whereDate('created_at','>=',$date_from)
                ->whereDate('created_at','<=',$date_to)
                ->where('recipient_name', 'like', '%' . $receiver . '%')
                ->orWhere('recipient_phone', 'like', '%' . $receiver . '%')
                ->orderBy('id','desc')->paginate(50);
        }
        else if ($tracking_id==null && $receiver==null && $shop!="0" && $delivery_status=="0")
        {
            $orders = Order::where('merchant_id',Auth::id())
                ->whereDate('created_at','>=',$date_from)
                ->whereDate('created_at','<=',$date_to)
                ->where('shop_id',$shop)
                ->orderBy('id','desc')->paginate(50);
        }
        else if ($tracking_id==null && $receiver==null && $shop=="0" &&$delivery_status!="0")
        {
            $orders = Order::where('merchant_id',Auth::id())
                ->whereDate('created_at','>=',$date_from)
                ->whereDate('created_at','<=',$date_to)
                ->where('delivery_status',$delivery_status)
                ->orderBy('id','desc')->paginate(50);
        }
        else
        {
            $orders = Order::where('merchant_id',Auth::id())
                ->whereDate('created_at','>=',$date_from)
                ->whereDate('created_at','<=',$date_to)
                ->orderBy('id','desc')->paginate(50);
        }
        return $orders;
    }
}