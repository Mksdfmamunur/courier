<?php

namespace App\BLL;

use App\Merchant;
use App\MerchantPaymentInformation;
use App\MerchantShop;
use Carbon\Carbon;
use Illuminate\Support\Facades\Hash;

class MerchantLogic
{
    public function register($request,$password)
    {
        if (empty(Merchant::where('email',$request->email)->first()) ){
            $mp = Carbon::now()->toDateString() . "-" . uniqid();
            $receipt = $request->verification_id_pic;
            $imageName =  $receipt->getClientOriginalName();
            $extension = pathinfo($imageName, PATHINFO_EXTENSION);
            $imagemove= $receipt->move('public/storage/merchant_pic',$mp .".".$extension);
            $name = array (   "photo"=> $mp .".".$extension,  );
            $verificationIdPic = asset('public/storage/merchant_pic/'.$mp .".".$extension);
            $merchantLastData = Merchant::orderBy('id','desc')->first();
            if (empty($merchantLastData))
            {
                $merchantId = "merchant_1000";
            }
            else{
                $str = $merchantLastData->merchant_id;
                $str1 = explode("_",$str);
                $merchantId = $str1[0]."_".((int)$str1[1]+1);
            }

            $merchant = new Merchant;
            $merchant->merchant_id = $merchantId;
            $merchant->first_name = $request->first_name;
            $merchant->last_name = $request->last_name;
            $merchant->email = $request->email;
            $merchant->mobile_number = $request->mobile_number;
            $merchant->password = Hash::make($password);
            $merchant->verification_id_type = $request->verification_id_type;
            $merchant->verification_id_number = $request->verification_id_number;
            $merchant->verification_id_pic = $verificationIdPic;
            $merchant->save();
            $shopLastData = MerchantShop::orderBy('id','desc')->first();
            if (empty($shopLastData))
            {
                $shopId = "1000";
            }
            else{
                $str = $shopLastData->shop_id;
                $shopId = $str+1;
            }

            $merchantShop = new MerchantShop;

            $merchantShop->shop_id = $shopId;
            $merchantShop->merchant_id = Merchant::pluck('id')->last();
            $merchantShop->shop_name = $request->shop_name;
            $merchantShop->shop_link = $request->shop_link;
            $merchantShop->pickup_address = $request->pickup_address;
            $merchantShop->pickup_number = $request->pickup_number;
            $merchantShop->pickup_area = $request->pickup_area;
            $merchantShop->status = "Active";
            $merchantShop->save();
            $merchantPaymentInformation = new MerchantPaymentInformation;
            $merchantPaymentInformation->merchant_id = Merchant::pluck('id')->last();
            $merchantPaymentInformation->payment_type = $request->payment_type;
            if ($request->payment_type=="Mobile")
            {
                $merchantPaymentInformation->mobile_wallet = $request->mobile_wallet;
                $merchantPaymentInformation->wallet_number = $request->wallet_number;
            }
            if ($request->payment_type=="Bank")
            {
                $merchantPaymentInformation->bank_name = $request->bank_name;
                $merchantPaymentInformation->branch_name = $request->branch_name;
                $merchantPaymentInformation->account_type = $request->account_type;
                $merchantPaymentInformation->routing_number = $request->routing_number;
                $merchantPaymentInformation->account_holder_name = $request->account_holder_name;
                $merchantPaymentInformation->account_number = $request->account_number;
            }
            $merchantPaymentInformation->save();
            return true;
        }
        return false;
    }
}
