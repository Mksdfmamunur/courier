<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class staff extends Model
{
    public function admin(){
        return $this->belongsTo(Admin::class);
    }

    public function role(){
        return $this->belongsTo(Role::class);
    }
}
