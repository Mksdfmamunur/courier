<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MerchantPaymentInformation extends Model
{
    protected $table = 'merchant_payment_informations';
}
