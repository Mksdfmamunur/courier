<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SubStatus extends Model
{
    protected $table = 'sub_status';
}
