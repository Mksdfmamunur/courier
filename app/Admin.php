<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class Admin extends Authenticatable
{
    use Notifiable;
    protected $guard = 'admin';
    protected $hidden = [
        'password','password_reset_token' ,'remember_token',
    ];

    public function staff()
    {
        return $this->hasOne(staff::class);
    }
}
