<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CoverageArea extends Model
{
    public function shop()
    {
        return $this->belongsTo('App\Models\MerchantShop','id','pickup_area');
    }
    public function city()
    {
        return $this->belongsTo('App\Models\CoverageCity', 'id','city_id');
    }
}
