<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MerchantPayment extends Model
{
    protected $table = 'merchant_payments';
    public function merchant()
    {
        return $this->hasOne('App\Merchant','id');
    }

}
