<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RiderPayment extends Model
{
    public function rider()
    {
        return $this->hasOne('App\Rider','id','rider_id');
    }
}
