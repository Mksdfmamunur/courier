<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MerchantShop extends Model
{
    //
    public function pickUpArea()
    {
        return $this->hasOne('App\CoverageArea','id');
    }
    public function orders()
    {
        return $this->hasMany('App\Order','shop_id','id');
    }
}
